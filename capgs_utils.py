import xml.etree.ElementTree as ET
from schedcat.model.serialize import write_xml, task, set_attribute, subtag_for_attribute, maybe_int, parse_resmodel, get_attribute, write
from schedcat.util.time import ms2us
import schedcat.model.resources as resources
from schedcat.model.tasks import TaskSystem
from schedcat.model.tasks import SporadicTask
from schedcat.mapping.binpack import ignore
import copy
from fractions import Fraction
from schedcat.util.math    import lcm

def capgs_task(t):
	tag = ET.Element('capgs_task')
	if not t.id is None:
		set_attribute(tag, 'id', t)
	set_attribute(tag, 'period', t)
	set_attribute(tag, 'wcet', t, 'cost')
	if not t.implicit_deadline():
		set_attribute(tag, 'deadline', t)

	set_attribute(tag, 'partition', t)
	set_attribute(tag, 'response_time', t)
	set_attribute(tag, 'wss', t)
	set_attribute(tag, 'group', t)
	set_attribute(tag, 'task_type', t)

	rmodel = subtag_for_attribute(tag, t, 'resmodel', 'resources')
	if not rmodel is None:
		for res_id in t.resmodel:
			res_requirement(t.resmodel[res_id], rmodel)

	tag.task = t
	task.xml = tag
	return tag

def capgs_write(ts, fname):
	tag = ET.Element('capgs_taskset')
	prop = ET.SubElement(tag, 'properties')
	prop.set('utilization', str(ts.utilization()))
	prop.set('utilization_q', str(ts.utilization_q()))    
	prop.set('density_q', str(ts.density_q()))
	prop.set('density', str(ts.density()))
	prop.set('count', str(len(ts)))
	prop.set('number_of_groups', str(ts.number_of_groups()))

	hp = ts.hyperperiod()
	if hp:
		prop.set('hyperperiod', str(hp))

	for t in ts:
		tag.append(capgs_task(t))
	
	write_xml(tag, fname)

def parse_capgs_task(node):
	cost      = maybe_int(node.get('wcet'))
	period    = maybe_int(node.get('period'))
	group	  = maybe_int(node.get('group'))
	task_type = maybe_int(node.get('task_type'))
	partition = maybe_int(node.get('partition'))
	t_id 		  = maybe_int(node.get('id'))    

	#group, task_type, exec_cost, period, deadline=None, id=None, cpu=-1):
	t = CAPGS_Task(group, task_type, cost, period, None, t_id , partition)

	get_attribute(node, 'deadline', t,  convert=maybe_int)
	get_attribute(node, 'wss', t, convert=maybe_int)

	resmodel = parse_resmodel(node)
	if not resmodel is None:
		t.resmodel = resmodel

	t.xml = node
	node.task = t
	return t

def parse_capgs_taskset(node):
	tasks = [parse_capgs_task(n) for n in node.findall('capgs_task')]
	return CAPGS_TaskSystem(tasks)

def read_capgs_ts_from_file(file):
	tree = ET.ElementTree()
	tree.parse(file)
	root = tree.getroot()
	if root.tag == 'capgs_taskset':
		ts = parse_capgs_taskset(root)
		ts.xml = tree.getroot()
		return ts
	elif root.tag == 'capgs_task':
		t = parse_capgs_task(root)
		return t
	else:
		return None	


class CAPGS_Task(SporadicTask):
	def __init__(self, group, task_type, exec_cost, period, deadline=None, id=None, cpu=-1):
		SporadicTask.__init__(self, exec_cost, period, deadline, id)
		self.group = group
		self.task_type = task_type #task_type 0 = HRT, 1 = SRT, 2 = Best-effort
		self.partition = cpu

	def __repr__(self):
		idstr = ", id=%s" % self.id if self.id is not None else ""
		dstr  = ", deadline=%s" % self.deadline if self.deadline != self.period else ""
		typestr = ", %s" % "HRT task" if self.task_type == 0 else ", SRT task" if self.task_type == 1 else ", Best-effort task"
		groupstr = ", group=%s" % self.group
		cpustr = ", cpu=%s" % self.partition
		return "CAPGS_Task(wcet=%s, period=%s, util=%.4f%s%s%s%s%s)" % (self.cost, self.period, self.utilization(), dstr, idstr, typestr, groupstr, cpustr)

	def utilization(self):
		if self.task_type == 2: #if it is a BE task
			return 0
		return self.cost / float(self.period)

	def utilization_q(self):
		if self.task_type == 2: #if it is a BE task
			return 0
		return Fraction(self.cost, self.period)

	def density(self):
		if self.task_type == 2: #if it is a BE task
			return 0
		return self.cost / float(min(self.period, self.deadline))

	def density_q(self):
		if self.task_type == 2: #if it is a BE task
			return 0
		return Fraction(self.cost, min(self.period, self.deadline))

class CAPGS_TaskSystem(TaskSystem):
	def __init__(self, tasks=[]):
		TaskSystem.__init__(self, tasks)

	def __str__(self):
		return "\n".join([str(t) for t in self])

	def __repr__(self):
		return "CAPGS_TaskSystem([" + ", ".join([repr(t) for t in self]) + "])"

	def copy(self):
		ts = CAPGS_TaskSystem((copy.deepcopy(t) for t in self))
		return ts

	def print_ts(self):
		for t in self:
			t_type = "HRT" if t.task_type == 0 else "SRT" if t.task_type == 1 else "Best-effort"
			c = int(t.partition)
			print "Task[%d], p=%d, wcet=%d, util=%.4f, type=%s, group=%d, cpu=%d" % (t.id, t.period, t.cost, t.utilization(), t_type, t.group, c)
		return

	# the number of SRT groups must start in 1
	def number_of_groups(self):
 		return max([t.group for t in self])

	def hyperperiod(self):
		periods = []
		for t in self:
			if t.task_type != 2:
				periods.append(t.period)
		return lcm(*periods)

	def task_from_id(self, t_id):
		for t in self:
			if t.id == t_id:
				return t				


class CAPGS_Groups():
	def __init__(self, number):
		self.number_of_groups = number
		#self.groups = [[list for i in range(number*5)] for CAPGS_Task in range(20)]
		self.groups = [[] for _ in xrange(number*5)]
		self.group_utils = [0.0 for i in range(number*5)]

	def add_util_to_group(self, util, grp):
		self.group_utils[grp] =  self.group_utils[grp] + util

	def add_task_to_group(self, task, grp):
		self.groups[grp].append(task)

	def print_group(self, grp):
		for i in range(len(self.groups[grp])):
			print self.groups[grp][i]

	def sort_by_util(self):
		for i in range(self.number_of_groups):
			for j in range(self.number_of_groups):
				if self.group_utils[i] > self.group_utils[j]:
					tmp = self.group_utils[j]
					self.group_utils[j] = self.group_utils[i]
					self.group_utils[i] = tmp

					tmp2 = self.groups[i]
					self.groups[i] = self.groups[j]
					self.groups[i] = tmp2

	def remove_task_from_group(self, grp):
		min_util = 1.1
		for t in self.groups[grp]:
			if t.utilization() < min_util and t.task_type == 1: #remove only SRT task and not the Best-effort
				min_util = t.utilization()
				task = t
		self.groups[grp].remove(task)
		self.group_utils[grp] = self.group_utils[grp] - task.utilization()
		self.groups[self.number_of_groups].append(task)
		self.group_utils[self.number_of_groups] = task.utilization()
		self.number_of_groups = self.number_of_groups + 1


def capgs_first_fit_and_best_fit_decreasing(number_of_groups, items, bins, capacity=1.0, weight=id, misfit=ignore, empty_bin=list):
	sets = [empty_bin() for _ in xrange(0, bins)]
	sums = [0.0 for _ in xrange(0, bins)]

	srt_tasks = []

	#first partitions HRT tasks using the first-fit heuristic
	for x in items:
		if x.task_type == 0: #only considers HRT tasks task_type == 0
			found = False
			c = weight(x)
			for i in xrange(0, bins):
				if sums[i] + c <= capacity:
					sets[i] += [x]
					sums[i] += c
					x.partition = i
					found = True
					break
			if found == False:
				misfit(x)
		else:
			srt_tasks.append(x)

	#mount SRT task groups and order them by decreasing utilizations
	groups = CAPGS_Groups(number_of_groups)

	for t in srt_tasks:
		if t.task_type == 1:
			groups.add_util_to_group(t.utilization(), t.group-1)
		groups.add_task_to_group(t, t.group-1) 		

	groups.sort_by_util()		

	#srt_tasks.sort(key=group)
	#print "@@@SRT task group start"

	#for g in range(1, number_of_groups+1):
	#for g in range(number_of_groups):
	#	print "Grp[%d], util = %.4f" % (g, groups.group_utils[g])
	#	groups.print_group(g)
	#print groups.groups[0][0]

	#print "@@@SRT task group end"

	#partition SRT task groups according to the BFD

	x = 0
	while x < groups.number_of_groups:
		c = groups.group_utils[x] 
		# find the first bin that is sufficiently large
		idxs = range(0, bins)
		idxs.sort(key=lambda i: sums[i], reverse=True)
		found = False
		for i in idxs:
			#print "Grp[%d] = %.4f, Bin[%d]=%.4f, cap=%.4f" % (x, c, i, sums[i], capacity)
			if sums[i] + c <= capacity:
				for t in groups.groups[x]:
					if t.task_type == 1: #do not change the CPU for best-effort tasks
						t.partition = i
					#print "inserting %s of grp[%d] into bin[%d]" % (t, x, i)					
					sets[i] += [t]
				sums[i] += c
				found = True
				break
		if found == False:
			if len(groups.groups[x]) > 1:
				#print "Removing task with smallest util from group %d" % x
				groups.remove_task_from_group(x)
				x = x - 1
			else:
				print "Group %d not partitioned - Could not partition the task set" % x
				return 0
		x = x + 1

	return sets

def cap_gs_task_partitioning(taskset, util_cap, number_of_processors):
	taskset.sort(key=utilization, reverse=True)
	sets = capgs_first_fit_and_best_fit_decreasing(taskset.number_of_groups(), taskset, number_of_processors, util_cap, utilization, misfit)
	return sets

def group(task):
	return task.group

def utilization(task):
	#print "ID = %d, period = %d, wcet = %d, util = %.4f" % (task.id, task.period, task.cost, task.utilization())
	return task.utilization()

def misfit(task):
	print "Not able to partition the task set - Task utilization %.4f, type %d" % (task.utilization(), task.task_type)

