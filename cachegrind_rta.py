#!/usr/bin/env python
from __future__ import division
from math import ceil, floor
from schedcat.model.tasks import SporadicTask
from schedcat.model.tasks import TaskSystem
from collections import defaultdict
from copy import deepcopy
from generator_emstada import gen_taskset, gen_periods, StaffordRandFixedSum
from datetime import datetime
import sys
import os
import optparse
import csv
import numpy
import math
from collections import defaultdict
import itertools
import schedcat.sched.fp.rta as rta
from multiprocessing import Pool




#sizes    = [ 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608]
#ways     = [ 1, 2, 4, 8, 16, 32]

#our_benchmarks = [ "pca-small", "pca-medium", "pca-large", "lda-small", "lda-large", "lda-medium"  ]
our_benchmarks = [ "lda-large", "pca-large" ]


global results
global max_processors



def read_cachegrind_logs():

    results  = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))

    files = []
    path = "./summary_all_benchmarks/"
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if '.txt' in file:
                files.append(os.path.join(r, file))

    for f in files:
        filename = os.path.split(f)[1]
        size     = int(filename.split("_")[0])
        ways     = int(filename.split("_")[1][:-4])

        with open(f) as fp:
            for line in fp:
                benchmark_name    = line.split()[0]
                benchmark_speedup = (1 - float(line.split()[2][:-1])/100)
                if results[benchmark_name][size][ways] > 0 and results[benchmark_name][size][ways] > benchmark_speedup:
                    continue
                else:
                    results[benchmark_name][size][ways] = benchmark_speedup
        fp.close()
    
    return results


def rta_non_preemptive(taskset, task):
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task)  ]

    task.schedulable = False

    u = sum([t.utilization() for t in higher_equal_prio])
    if u >= 1:
        task.response_time = False
        return False

    # compute low priority blocking
    B = max([t.cost for t in lower_prio] or [0])

    # compute busy period length
    demand = B + task.cost; time = 0
    while demand > time:
        time = demand
        demand = B + sum([t.cost * int(ceil(time/t.period)) for t in higher_equal_prio])
    busy_period = demand

    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        time   = max(B+sum([t.cost for t in higher_prio]),(k-1)*task.period)
        demand = B + sum([t.cost * (int(floor(time/t.period))+1) for t in higher_prio]) + (k-1)*task.cost
        while demand > time:
            time   = demand
            demand = B + sum([t.cost * (int(floor(time/t.period))+1) for t in higher_prio]) + (k-1)*task.cost

        response_times.append(demand + task.cost - (k-1)*task.period)

    task.response_time = max(response_times)
    if task.response_time <= task.deadline:
        task.schedulable = True

    return task.schedulable


def rta_np_test(taskset):
    for task in taskset:
        if rta_non_preemptive(taskset, task) == False:
            return False
    return True


def rta_p_test(taskset):
    return rta.bound_response_times(1, taskset)
                

def sched_test_wrapper(size, ways, util, profiles, period_min, period_max, max_iters, test_type="NP"):
    global max_processors
    pool = Pool(processes=max_processors)
  
    if test_type   == "NP":
        test_results = pool.map(multi_run_wrapper, [ (size, ways, util, profiles, period_min, period_max, "NP") ] * max_iters )
    elif test_type == "P":
        test_results = pool.map(multi_run_wrapper,  [ (size, ways, util, profiles, period_min, period_max, "P")  ] * max_iters )

    sum_test_success_legacy = sum_test_success_ours = 0
    for (test_success_legacy,test_success_ours) in test_results:
        sum_test_success_legacy = sum_test_success_legacy + test_success_legacy
        sum_test_success_ours   = sum_test_success_ours   + test_success_ours

    out_test_success_legacy = float(sum_test_success_legacy)/max_iters
    out_test_success_ours   = float(sum_test_success_ours)/max_iters

    pool.close()

    return (out_test_success_legacy, out_test_success_ours)


def multi_run_wrapper(args):
    return sched_test(*args)


def sched_test(size, ways, util, profiles, period_min, period_max, test_type="NP"):

    global results
    n = len(profiles)
    test_success_legacy = 0
    test_success_ours   = 0

    taskset_legacy = gen_taskset((period_min,period_max), 'logunif', n, util)
    taskset_legacy.assign_ids_by_deadline()
    taskset_legacy.sort_by_deadline()

    taskset_ours   = deepcopy(taskset_legacy)
    
    i = 0
    for task in taskset_ours:
        if test_type == "NP":
            speedup = results[profiles[i]][size][ways]
        elif test_type == "P":
            speedup = results[profiles[i]][size/n][ways]
        if speedup == 0: 
            speedup=1
        task.cost = task.cost * speedup
        i = i + 1

    if test_type == "NP":
        if rta_np_test(taskset_legacy) == True:
            test_success_legacy = 1
        if rta_np_test(taskset_ours)   == True:
            test_success_ours = 1
    elif test_type == "P":
        if rta_p_test(taskset_legacy)  == True:
            test_success_legacy = 1
        if rta_p_test(taskset_ours)    == True:
            test_success_ours = 1

    return test_success_legacy, test_success_ours


def main():
    period_min = 10
    period_max = 100
    
    n = 8
    
    global max_processors 
    global results

    max_processors = 4
    max_iterations = 100

    utilization_step = 0.1

    test_type = "P"
    #test_type = "P"

    dirname = "cachegrind_rta_results"


    sizes    = [ 1024 ]
    ways     = [ 8 ]
    
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    results       = read_cachegrind_logs()  # these are cache-related speed-ups, results[benchmark_name][size][ways]

    test_results  = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    delta_avg     = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
    delta_max     = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
    final_ours    = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
    final_lru     = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))

    test_utilizations = [ util*utilization_step for util in range(1,int(1/utilization_step) + 1) ]
    task_profiles     =  list(itertools.combinations_with_replacement(our_benchmarks,n))

    for (s,w,u,p) in itertools.product(*[sizes, ways, test_utilizations, task_profiles]):
        test_results[s][w][u].append(sched_test_wrapper(s,w,u,p,period_min,period_max,max_iterations,test_type))

    for s in test_results:
        for w in test_results[s]:
            for u in test_results[s][w]:
                for (test_success_legacy, test_success_ours) in test_results[s][w][u]:
                    final_ours[s][w][u] = final_ours[s][w][u] + test_success_ours
                    final_lru[s][w][u]  = final_lru[s][w][u]  + test_success_legacy
                    delta_avg[s][w][u]  = delta_avg[s][w][u]  + test_success_ours - test_success_legacy
                    delta_max[s][w][u]  = max(delta_max[s][w][u],  test_success_ours - test_success_legacy)
                delta_avg[s][w][u]  = delta_avg[s][w][u]  / len(test_results[s][w][u])
                final_ours[s][w][u] = final_ours[s][w][u] / len(test_results[s][w][u])
                final_lru[s][w][u]  = final_lru[s][w][u]  / len(test_results[s][w][u])


    for s in delta_avg:
        for w in delta_avg[s]:
            if   test_type == "NP":
                filename = dirname + "/sched_experiment_non_preemptive_tasks_"
            elif test_type == "P":
                filename = dirname + "/sched_experiment_preemptive_tasks_"
            filename = filename + str(n) +"_size_" + str(s) + "_ways_" + str(w) + ".log"
            f = open(filename,"w")
            f.write("util\tlru\tours\tdt avg\tdt max\n")
            for u in sorted(delta_avg[s][w]):
                f.write("%.2f\t%.3f\t%.3f\t%.3f\t%.3f\n" % (u, final_lru[s][w][u], final_ours[s][w][u], delta_avg[s][w][u], delta_max[s][w][u]))
            f.close()


if __name__ == "__main__":
    sys.exit(main())
