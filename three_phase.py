#!/usr/bin/env python
from __future__ import division
from math import ceil, floor
from schedcat.model.tasks import SporadicTask
from schedcat.model.tasks import TaskSystem
from collections import defaultdict
from copy import deepcopy
from datetime import datetime
import sys
import os
import optparse


class SegmentedTask(SporadicTask):
    
    def __init__(self, load_cost, unload_cost, exec_cost, period, deadline=None, id=None, name=None):
        if deadline is None:
            deadline=period
        self.period      = period
        self.exec_cost   = exec_cost
        self.load_cost   = load_cost
        self.unload_cost = unload_cost
        self.cost        = load_cost + exec_cost + unload_cost
        self.deadline    = deadline
        self.id          = id
        self.name        = name

    def __repr__(self):
        idstr = ", id=%s" % self.id if self.id is not None else ""
        ustr = "%.2f" % self.utilization()
        return "Segmented_Task(%s, %s, %s, %s, %s%s (%s) %s)" % (self.load_cost, self.exec_cost, self.unload_cost, self.period, self.deadline, idstr, self.name, ustr)

    def mem_utilization(self):
        return (self.load_cost + self.unload_cost) / self.period



class SegmentedTaskSystem(TaskSystem):
    
    def max_exec_cost(self):
        return max([t.exec_cost for t in self])

    def max_load_cost(self):
        return max([t.load_cost for t in self])

    def max_unload_cost(self):
        return max([t.unload_cost for t in self])

    def mem_utilization(self):
        u = [t.mem_utilization() for t in self]


def rta_non_preemptive(taskset, task):
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task)  ]

    task.schedulable = False

    u = sum([t.utilization() for t in higher_equal_prio])
    if u >= 1:
        task.response_time = False
        return False

    # compute low priority blocking
    B = max([t.exec_cost for t in lower_prio] or [0])

    # compute busy period length
    demand = B + task.exec_cost; time = 0
    while demand > time:
        time = demand
        demand = B + sum([t.exec_cost * int(ceil(time/t.period)) for t in higher_equal_prio])
    busy_period = demand

    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        time   = max(B+sum([t.exec_cost for t in higher_prio]),(k-1)*task.period)
        demand = B + sum([t.exec_cost * (int(floor(time/t.period))+1) for t in higher_prio]) + (k-1)*task.exec_cost
        while demand > time:
            time   = demand
            demand = B + sum([t.exec_cost * (int(floor(time/t.period))+1) for t in higher_prio]) + (k-1)*task.exec_cost

        response_times.append(demand + task.exec_cost - (k-1)*task.period)

    task.response_time = max(response_times)
    if task.response_time <= task.deadline:
        task.schedulable = True

    return task.schedulable


def interference(taskset, extra_jobs, time, slot, cycle):

    mem_times = []
    cmp_times = []
    for task in taskset:
        num_jobs = int(ceil(time/task.period))
        if task.exec_cost > 2*cycle:
            mem_times.extend([2*cycle + slot] * num_jobs)
        else:
            mem_times.extend([2*cycle] * num_jobs)
        cmp_times.extend([task.exec_cost] * num_jobs)

    num_jobs = len(mem_times)
    if len(extra_jobs) > 0:
        num_jobs = num_jobs + len(filter(None,extra_jobs))
        for extra_job_cost in filter(None,extra_jobs):
            if extra_job_cost > 2*cycle:
                mem_times.extend([2*cycle + slot])
            else:
                mem_times.extend([2*cycle])
            cmp_times.extend([extra_job_cost])

    sum_list = sorted(mem_times + cmp_times, reverse=True)
    return sum(sum_list[0:num_jobs])


def rta_fixed_slot(taskset, task, slot, cycle):
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task)  ]

    task.schedulable = False

    u = sum([max(t.exec_cost,2*cycle+slot)/t.period for t in higher_equal_prio])
    if u >= 1:
        task.response_time = False
        return False

    lower_prio = sorted(lower_prio, key=lambda task: task.exec_cost, reverse=True)
    if len(lower_prio) == 0:
        B = slot + cycle
        blocking_job = None 
    elif len(lower_prio) == 1:
        B = slot
        blocking_job = lower_prio[0].exec_cost
    else:
        B = max(lower_prio[0].exec_cost - cycle, 0) + slot
        blocking_job = lower_prio[1].exec_cost

    F = max(task.exec_cost, cycle) + 2*cycle + slot

    # compute busy period length
    demand = B + task.cost; time = 0
    while demand > time:
        time   = demand
        demand = B + interference(higher_equal_prio, [blocking_job], time, slot, cycle)
    busy_period = demand

    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        # maybe you should add here k*C_i
        time   = max(B+sum([t.cost for t in higher_prio]),(k-1)*task.period)
        demand = B + interference(higher_prio, [blocking_job] + (k-1)*[task.exec_cost], time, slot, cycle)
        while demand > time:
            time   = demand
            demand = B + interference(higher_prio, [blocking_job] + (k-1)*[task.exec_cost], time, slot, cycle)

        response_times.append(demand + F - (k-1)*task.period)

    task.response_time = max(response_times)
    if task.response_time <= task.deadline:
        task.schedulable = True

    return task.schedulable


def rta_variable_slot(taskset, task, slot, cycle):
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task)  ]

    task.schedulable = False

    max_load_cost   = SegmentedTaskSystem(higher_equal_prio).max_load_cost()
    max_unload_cost = SegmentedTaskSystem(higher_equal_prio).max_unload_cost()
    max_reload_time = cycle - slot + floor((max_load_cost + max_unload_cost)/slot) * cycle + (max_load_cost + max_unload_cost) % slot
    print max_load_cost, max_unload_cost, max_reload_time, slot, cycle

    u = sum([max(t.exec_cost,max_reload_time)/t.period for t in higher_equal_prio])
    if u >= 1:
        task.response_time = False
        return False

    return 0

    lower_prio = sorted(lower_prio, key=lambda task: task.exec_cost, reverse=True)
    if len(lower_prio) == 0:
        B = slot + cycle
        blocking_job = None 
    elif len(lower_prio) == 1:
        B = slot
        blocking_job = lower_prio[0].exec_cost
    else:
        B = max(lower_prio[0].exec_cost - cycle, 0) + slot
        blocking_job = lower_prio[1].exec_cost

    F = max(task.exec_cost, cycle) + 2*cycle + slot

    # compute busy period length
    demand = B + task.cost; time = 0
    while demand > time:
        time   = demand
        demand = B + interference(higher_equal_prio, [blocking_job], time, slot, cycle)
    busy_period = demand

    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        time   = max(B+sum([t.cost for t in higher_prio]),(k-1)*task.period)
        demand = B + interference(higher_prio, [blocking_job] + (k-1)*[task.exec_cost], time, slot, cycle)
        while demand > time:
            time   = demand
            demand = B + interference(higher_prio, [blocking_job] + (k-1)*[task.exec_cost], time, slot, cycle)

        response_times.append(demand + F - (k-1)*task.period)

    task.response_time = max(response_times)
    if task.response_time <= task.deadline:
        task.schedulable = True

    return task.schedulable




def create_log_file(test_name, tasks_per_core, period_min, period_max, N, runs):
    now = datetime.now()
    if not os.path.exists('dir_results'):
        os.makedirs('dir_results')
    dirname  = "dir_results/" + str(test_name)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    filename = str(N) + "_cores_" + str(tasks_per_core) \
                      + "_task_per_core_with_periods_" + str(period_min) + "_" +str(period_max) + "_runs_" + str(runs) + "_" \
                      +  now.strftime("%d_%m_%H_%M_%S")
    filename = dirname + "/" + filename + ".log"
    return open(filename,"w")




def main():
    #don't add help option as we will handle it ourselves
    parser = optparse.OptionParser()
    parser.add_option("-c", "--chanells",
                       type="int", dest="chanells",
                      default="1")

    (options, args) = parser.parse_args()


    frequency_load_cost   = 7.87
    frequency_unload_cost = 7.87
    frequency_exec_cost   = 4169.6
    frequency_period      = 20000
    frequency_deadline    = 20000

    spike_load_cost       = 10.40
    spike_unload_cost     = 10.40
    spike_exec_cost       = 1784
    spike_deadline        = 20000
    spike_period          = 20000

    clip_load_cost        = 16.80
    clip_unload_cost      = 16.80
    clip_exec_cost        = 1008
    clip_deadline         = 20000
    clip_period           = 20000

    level_load_cost       = 12.43
    level_unload_cost     = 12.43
    level_exec_cost       = 1160.4
    level_period          = 20000
    level_deadline        = 20000

    nfer_load_cost        = 356.44
    nfer_unload_cost      = 356.44
    nfer_exec_cost        = 3185.6
    nfer_period           = 20000
    nfer_deadline         = 20000

    voter_load_cost       = 13.37
    voter_unload_cost     = 13.37
    voter_exec_cost       = 300
    voter_period          = 15000
    voter_deadline        = 15000

    cores = defaultdict(SegmentedTaskSystem)
    slot  = defaultdict(int)

    # chanell = 1

    freq_1   = SegmentedTask(frequency_load_cost, frequency_unload_cost, frequency_exec_cost, frequency_period, frequency_deadline, 0, "frequency")
    clip_1   = SegmentedTask(clip_load_cost, clip_unload_cost, clip_exec_cost, clip_period, clip_deadline, 1, "clip")
    cores[1] = SegmentedTaskSystem([freq_1, clip_1])

    voter_1  = SegmentedTask(voter_load_cost, voter_unload_cost, voter_exec_cost, voter_period, voter_deadline, 0, "voter")
    spike_1  = SegmentedTask(spike_load_cost, spike_unload_cost, spike_exec_cost, spike_period, spike_deadline, 1, "spike")
    level_1  = SegmentedTask(level_load_cost, level_unload_cost, level_exec_cost, level_period, level_deadline, 2, "level")
    cores[2] = SegmentedTaskSystem([voter_1, spike_1, level_1])

    nfer_1   = SegmentedTask(nfer_load_cost, nfer_unload_cost, nfer_exec_cost, nfer_period, nfer_deadline, 0, "nfer")
    cores[3] = SegmentedTaskSystem([nfer_1])






    # compute TDM slots and cycle
    for core_id in cores:
        slot[core_id] = cores[core_id].max_load_cost()
    cycle = sum(slot.values())

    print "cycle: " + str(cycle)
    for core_id in cores:
        print "slot " + str(core_id) + ": " + str(slot[core_id])


    for core_id in cores:
        for task in cores[core_id]:
            rta_variable_slot(cores[core_id], task, slot[core_id], cycle)

    return 0

    #rta_fixed_slot(cores[2], spike_1, slot[2], cycle)

    for core_id in cores:
        print "core " + str(core_id)
        for task in cores[core_id]:
            print task.name
            print "non-preemptive"
            rta_non_preemptive(cores[core_id], task)
            print task.response_time
            print "fixed-slot"
            rta_fixed_slot(cores[core_id], task, slot[core_id], cycle)
            print task.response_time



    #else:
    #    parser.print_help()

if __name__ == "__main__":
    sys.exit(main())
