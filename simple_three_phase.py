#!/usr/bin/env python
from __future__ import division
from math import ceil, floor
from schedcat.model.tasks import SporadicTask
from schedcat.model.tasks import TaskSystem
from collections import defaultdict
from copy import deepcopy
from datetime import datetime
import sys
import os
import optparse
import pdb


max_channels         = 4

spectrum_load_cost   = 787
spectrum_unload_cost = 787
spectrum_exec_cost   = 416960
spectrum_period      = 2000000
spectrum_deadline    = 2000000

spike_load_cost       = 1040
spike_unload_cost     = 1040
spike_exec_cost       = 178400
spike_deadline        = 2000000
spike_period          = 2000000

level_load_cost       = 1243
level_unload_cost     = 1243
level_exec_cost       = 116040
level_period          = 2000000
level_deadline        = 2000000

clip_load_cost        = 1680
clip_unload_cost      = 1680
clip_exec_cost        = 100800
clip_deadline         = 2000000
clip_period           = 2000000

voter_load_cost       = 1337
voter_unload_cost     = 1337
voter_exec_cost       = 30000
voter_period          = 1500000
voter_deadline        = 1500000

nfer_load_cost        = 35644
nfer_unload_cost      = 35644
nfer_exec_cost        = 318560
nfer_period           = 2000000
nfer_deadline         = 2000000



class SegmentedTask(SporadicTask):
    
    def __init__(self, load_cost, unload_cost, exec_cost, period, deadline=None, id=None, name=None):
        if deadline is None:
            deadline=period
        self.period      = period
        self.exec_cost   = exec_cost
        self.load_cost   = load_cost
        self.unload_cost = unload_cost
        self.cost        = load_cost + exec_cost + unload_cost
        self.deadline    = deadline
        self.id          = id
        self.name        = name

    def __repr__(self):
        idstr = ", id=%s" % self.id if self.id is not None else ""
        ustr = "%.2f" % self.utilization()
        return "Segmented_Task(%s, %s, %s, %s, %s%s (%s) %s)" % (self.load_cost, self.exec_cost, self.unload_cost, self.period, self.deadline, idstr, self.name, ustr)

    def mem_utilization(self):
        return (self.load_cost + self.unload_cost) / self.period

    def load_time(self,slot,cycle,ovh):
        assert  cycle >= slot > ovh
        payload   = self.load_cost - ovh
        assert payload > 0
        n_slots   = ceil(payload/(slot-ovh)) - 1
        load_time = (cycle + ovh) + n_slots * (cycle - (slot-ovh)) + payload
        return load_time

    def unload_time(self,slot,cycle,ovh):
        assert cycle >= slot > ovh
        payload   = self.unload_cost - ovh
        assert payload > 0
        n_slots   = ceil(payload/(slot-ovh)) - 1
        unload_time = (cycle + ovh) + n_slots * (cycle - (slot-ovh)) + payload
        return unload_time





class SegmentedTaskSystem(TaskSystem):
    
    def max_exec_cost(self):
        return max([t.exec_cost for t in self])

    def max_load_cost(self):
        return max([t.load_cost for t in self])

    def max_unload_cost(self):
        return max([t.unload_cost for t in self])

    def mem_utilization(self):
        u = [t.mem_utilization() for t in self]

    def memtime(self,slot,cycle,ovh):
        assert cycle >= slot > ovh
        payload     = self.max_load_cost() + self.max_unload_cost() - 2 * ovh
        assert payload > 0
        n_slots     = ceil(payload/(slot-ovh)) - 1
        reload_time = (cycle + ovh) + n_slots * (cycle - (slot-ovh)) + payload 
        return reload_time

    def load_time(self,slot,cycle,ovh):
        assert cycle >= slot > ovh
        payload   = self.max_load_cost() -  ovh
        assert payload > 0
        n_slots     = ceil(payload/(slot-ovh)) - 1
        load_time = (cycle + ovh) + n_slots * (cycle - (slot-ovh)) + payload
        return load_time

    def unload_time(self,slot,cycle,ovh):
        assert cycle >= slot > ovh
        payload     = self.max_unload_cost() -  ovh
        assert payload > 0
        n_slots     = ceil(payload/(slot-ovh)) - 1
        unload_time = (cycle + ovh) + n_slots * (cycle - (slot-ovh)) + payload
        return unload_time

    def best_load_time(self,slot,cycle,ovh):
        assert cycle >= slot > ovh
        payload   = self.max_load_cost() -  ovh
        assert payload > 0
        n_slots   = ceil(payload/(slot-ovh)) - 1
        load_time = n_slots * (cycle - (slot-ovh)) + payload
        return load_time

    def LUL_time(self,slot,cycle,ovh):
        assert cycle >= slot > ovh
        payload     = 2 * self.max_load_cost() + self.max_unload_cost() - 3 * ovh
        assert payload > 0
        n_slots     = ceil(payload/(slot-ovh)) - 1
        lul_time    = (cycle + ovh) + n_slots * (cycle - (slot-ovh)) + payload
        #pdb.set_trace()
        return lul_time



def interference(taskset, time, memtime):
    demand = 0
    for task in taskset:
        num_jobs = int(ceil(time/task.period))
        demand   = demand + num_jobs*max(memtime, task.exec_cost)

    return demand


def rta_non_preemptive(taskset, task):
    hp  = [ t for t in taskset.with_higher_priority_than(task) ]
    hep = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lp  = [ t for t in taskset.with_lower_priority_than(task)  ]

    task.schedulable = False

    u = sum([t.exec_cost/t.period for t in hep])
    if u >= 1:
        task.response_time = False
        return False

    # compute low priority blocking
    B = max([t.exec_cost for t in lp] or [0])

    # compute busy period length
    time   = 0
    demand = B + sum([t.exec_cost for t in hep])
    while demand > time:
        time = demand
        demand = B + sum([t.exec_cost * int(ceil(time/t.period)) for t in hep])
    busy_period = demand

    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        time   = max(B+sum([t.exec_cost for t in hp]),(k-1)*task.period)
        demand = B + sum([t.exec_cost * int(ceil(time/t.period)) for t in hp]) + (k-1)*task.exec_cost
        while demand > time:
            time   = demand
            # ceil / floor + 1  => depends on the scheduler
            # demand = B + sum([t.exec_cost * (int(floor(time/t.period))+1) for t in hp]) + (k-1)*task.exec_cost
            demand = B + sum([t.exec_cost * int(ceil(time/t.period)) for t in hp]) + (k-1)*task.exec_cost

        response_times.append(demand + task.exec_cost - (k-1)*task.period)

    task.response_time = max(response_times)
    if task.response_time <= task.deadline:
        task.schedulable = True

    return task.schedulable


# Giovani Gracioli, Tomasz Kloda, Rohan Tabish, Reza Mirosanlou, Renato Mancuso, Rodolfo Pellizzoni, and Marco Caccamo
# Mixed Criticality Applications on Modern Heterogeneous MPSoCs: Analysis and Implementation
def late_loading_prem(taskset, task, cycle, slot, ovh=400):
    hp  = [ t for t in taskset.with_higher_priority_than(task) ]
    hep = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lp  = [ t for t in taskset.with_lower_priority_than(task)  ]

    task.schedulable = False

    memtime =  taskset.memtime(slot,cycle,ovh)

    L       =  taskset.load_time(slot,cycle,ovh)
    Lmin    =  taskset.best_load_time(slot,cycle,ovh)
    U       =  task.unload_time(slot,cycle,ovh)


    if(len(taskset)==1):
        task.response_time = task.load_time(slot,cycle,ovh) + task.exec_cost + task.unload_time(slot,cycle,ovh)
        if task.response_time <= task.deadline:
            task.schedulable = True
        return task.schedulable


    u = sum([max(t.exec_cost,memtime)/t.period for t in hep])
    if u >= 1:
        task.response_time = False
        return False

    lp = sorted(lp, key=lambda task: task.exec_cost, reverse=True)
    if len(lp) == 0:
        B = memtime
        #B = taskset.LUL_time(slot,cycle,ovh)
        #L = 0
    else:
        B = max(lp[0].exec_cost, memtime)

    demand = B + L + sum([max(t.exec_cost, memtime) for t in hep])
    while True:
        time   = demand - Lmin
        demand = B + L + interference(hep, time, memtime)
        if time + Lmin == demand:
            busy_period = time
            break


    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        time   = max(L+B+sum([max(t.exec_cost, memtime) for t in hp]), (k-1)*task.period)
        demand = L + B + interference(hp, time - Lmin, memtime) + (k-1)*max(task.exec_cost, memtime) 
        while demand > time:
            time   = demand
            demand = L + B + interference(hp, time - Lmin, memtime) + (k-1)*max(task.exec_cost, memtime)

        f_time = time + max(task.exec_cost, memtime) + U 
        response_times.append(f_time - (k-1)*task.period)

    task.response_time = max(response_times)
    if task.response_time <= task.deadline:
        task.schedulable = True

    return task.schedulable

    

# Muhammad R. Soliman and Rodolfo Pellizzoni
# PREM-Based Optimal Task Segmentation Under Fixed Priority Scheduling

def rta_classic_prem(taskset, task, cycle, slot, ovh=400):
    hp  = [ t for t in taskset.with_higher_priority_than(task) ]
    hep = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lp  = [ t for t in taskset.with_lower_priority_than(task)  ]

    task.schedulable = False


    if(len(taskset)==1):
        task.response_time = task.load_time(slot,cycle,ovh) + task.exec_cost + task.unload_time(slot,cycle,ovh)
        if task.response_time <= task.deadline:
            task.schedulable = True
        return task.schedulable

    memtime = taskset.memtime(slot,cycle,ovh)
    U       = task.unload_time(slot,cycle,ovh)

    # they do not check this condition but the convergence condtion is needed
    u = sum([max(t.exec_cost,memtime)/t.period for t in hep])
    if u >= 1:
        task.response_time = False
        return False

    lp = sorted(lp, key=lambda task: task.exec_cost, reverse=True)
    if len(lp) == 0:
        B = memtime
    elif len(lp) == 1:
        B = memtime + max(lp[0].exec_cost, memtime)
    else:
        B = 2*max(lp[0].exec_cost, memtime)

    # they do not compute busy period
    # there should be additional blocking for each \tau_i
    time   = 0
    demand = B + sum([max(t.exec_cost, memtime) for t in hep])
    while demand > time:
        time   = demand
        demand = B + interference(hep, time, memtime)
    busy_period = demand


    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        time   = max(B+sum([max(t.exec_cost, memtime) for t in hp]), (k-1)*task.period)
        # actually the blocking should be k*B
        demand = B + interference(hp, time, memtime) + (k-1)*max(task.exec_cost, memtime)
        while demand > time:
            time   = demand
            demand = B + interference(hp, time, memtime) + (k-1)*max(task.exec_cost, memtime)

        f_time = time + max(task.exec_cost, memtime) + U
        response_times.append(f_time - (k-1)*task.period)

        # to do everything like in the previous papers we stop at the first task instance
        break

    task.response_time = max(response_times)
    if task.response_time <= task.deadline:
        task.schedulable = True

    return task.schedulable


def create_log_file(test_type, test_name):
    dirname  = "anomaly_detection_results/"
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    dirname = dirname + "/" + test_type
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    filename = str(test_name) 
    filename = dirname + "/" + filename + ".log"

    return open(filename,"w")


def cpu0(channels=1):

    spikes = [ SegmentedTask(spike_load_cost, spike_unload_cost, spike_exec_cost, spike_period, spike_deadline, priority, "spike") for priority in range(0,channels) ]
    clips  = [ SegmentedTask(clip_load_cost, clip_unload_cost, clip_exec_cost, clip_period, clip_deadline, priority, "clip") for priority in range(channels, 2*channels) ]
    levels = [ SegmentedTask(level_load_cost, level_unload_cost, level_exec_cost, level_period, level_deadline, priority, "level") for priority in range(2*channels,3*channels) ]

    return SegmentedTaskSystem(spikes + clips + levels)


def cpu1(channels=1):

    voters = [ SegmentedTask(voter_load_cost, voter_unload_cost, voter_exec_cost, voter_period, voter_deadline, priority, "voter") for priority in range(0,channels) ]
    spectrums = [ SegmentedTask(spectrum_load_cost, spectrum_unload_cost, spectrum_exec_cost, spectrum_period, spectrum_deadline, priority, "spectrum") for priority in range(channels,2*channels) ]

    return SegmentedTaskSystem(voters + spectrums)


def cpu2(channels=1):

    nfers = [ SegmentedTask(nfer_load_cost, nfer_unload_cost, nfer_exec_cost, nfer_period, nfer_deadline, priority, "nfer") for priority in range(0,channels) ]

    return SegmentedTaskSystem(nfers) 


def cpu(core_id, channels=1):
    if core_id == 0:
        return cpu0(channels)
    elif core_id == 1:
        return cpu1(channels)
    elif core_id == 2:
        return cpu2(channels)


def tdma_assignment(cores, max_slot=40000):

    slots = []

    for core in cores:
        max_load = core.max_load_cost()
        slots.append(min(max_load,max_slot))
    cycle = sum(slots)

    return (slots,cycle)


def run_test_cpu(test_type="prem_classic", max_slot=40000):

    scale = 1000*100 # convert results to miliseconds

    cores    = defaultdict(lambda: defaultdict(SegmentedTaskSystem))
    results  = defaultdict(lambda: defaultdict(int))
    log_file = defaultdict(str)

    if test_type == "non_preemp":
        for core_id in range(0,3):
            log_file[core_id] = create_log_file(test_type, str(core_id))
    else:
        for core_id in range(0,3):
            log_file[core_id] = create_log_file(test_type, str(core_id) + "_" + str(max_slot))


    (slot,cycle) = tdma_assignment([cpu(0), cpu(1), cpu(2)], max_slot)
    print slot,cycle


    for core_id in range(0,3):
        for channels in range(1,max_channels+1):
            cores[core_id][channels] = cpu(core_id,channels)

            results[core_id][channels] = 0

            for task in cores[core_id][channels]:
                if test_type == "prem_classic":
                    rta_classic_prem(cores[core_id][channels], task, cycle, slot[core_id])
                elif test_type == "prem_late_load":
                    late_loading_prem(cores[core_id][channels], task, cycle, slot[core_id])
                elif test_type == "non_preemp":
                    rta_non_preemptive(cores[core_id][channels], task)

                if task.schedulable is False:
                    results[core_id][channels] = results[core_id][channels] + task.deadline / len(cores[core_id][channels])
                    print test_type, core_id, channels, "deadline miss"
                else:
                    results[core_id][channels] = results[core_id][channels] + task.response_time / len(cores[core_id][channels])


            log_file[core_id].write("%10.2f\t"%(results[core_id][channels]/scale))





def run_test(test_type="prem_classic", max_slot=40000):

    scale = 1000*100 # convert results to miliseconds

    cores    = defaultdict(lambda: defaultdict(SegmentedTaskSystem))
    results  = defaultdict(lambda: defaultdict(set))
    log_file = defaultdict(str)

    if test_type == "non_preemp":
        for test_name in ["spectrum", "spike", "level", "clip", "voter", "nfer"]:
            log_file[test_name] = create_log_file(test_type,  test_name)
    else:
        for test_name in ["spectrum", "spike", "level", "clip", "voter", "nfer"]:
            log_file[test_name] = create_log_file(test_type,  test_name + "_" + str(max_slot))



    (slot,cycle) = tdma_assignment([cpu(0), cpu(1), cpu(2)], max_slot)


    for core_id in range(0,3):
        for channels in range(1,max_channels+1):
            cores[core_id][channels] = cpu(core_id,channels)

            for task in cores[core_id][channels]:
                if test_type == "prem_classic":
                    rta_classic_prem(cores[core_id][channels], task, cycle, slot[core_id])
                elif test_type == "prem_late_load":
                    late_loading_prem(cores[core_id][channels], task, cycle, slot[core_id])
                elif test_type == "non_preemp":
                    rta_non_preemptive(cores[core_id][channels], task)



    for core_id in range(0,3):
        for channels in range(1,max_channels+1):
            cnt = 0
            for task in cores[core_id][channels]:
                if task.response_time is False:
                    cnt = cnt + 1
                    #task.response_time = task.deadline
                results[task.name][channels].add(task.response_time)
            print "deadline misses: " +  str(test_type) + ", " + str(channels) + ", " + str(cnt)


    rt_min = defaultdict(lambda: defaultdict(int))
    rt_max = defaultdict(lambda: defaultdict(int))
    rt_avg = defaultdict(lambda: defaultdict(int))

    for task in results.keys():
        for channels in range(1,max_channels+1):
            if len(filter(None,results[task][channels])) == 0:
                rt_avg[task][channels] = rt_min[task][channels] = False
            else:
                rt_avg[task][channels] = sum(filter(None,results[task][channels]))/len(filter(None,results[task][channels]))
            if False in results[task][channels]:
                rt_max[task][channels] = False
            else:
                rt_max[task][channels] = max(results[task][channels])
            if len(filter(None,results[task][channels])) == 0:
                rt_min[task][channels] = False
            else:
                rt_min[task][channels] = min(filter(None,results[task][channels]))

    for task in results.keys():
        log_file[task].write("min ")
        for channels in range(1,max_channels+1):
            log_file[task].write("%10.2f\t"%(rt_min[task][channels]/scale))
        log_file[task].write("\n")
        log_file[task].write("avg ")
        for channels in range(1,max_channels+1):
            log_file[task].write("%10.2f\t"%(rt_avg[task][channels]/scale))
        log_file[task].write("\n")
        log_file[task].write("max ")
        for channels in range(1,max_channels+1):
            log_file[task].write("%10.2f\t"%(rt_max[task][channels]/scale))
        log_file[task].write("\n")


    return 0


def main():
    #don't add help option as we will handle it ourselves
    parser = optparse.OptionParser()
    parser.add_option("-c", "--chanells",
                       type="int", dest="chanells",
                      default="1")

    (options, args) = parser.parse_args()


    for max_slot in [4000, 40000]:
        run_test_cpu("prem_classic", max_slot)
        run_test_cpu("prem_late_load", max_slot)
    run_test_cpu("non_preemp")

    return

if __name__ == "__main__":
    sys.exit(main())
