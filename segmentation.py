import os
import random
import schedcat.generator.tasks as gen
import schedcat.mapping.binpack as bp
from schedcat.mapping.rollback import Bin

from schedcat.model.serialize import write
from schedcat.generator.tasksets import mkgen #, \
                                    #    NAMED_UTILIZATIONS, \
                                    #    NAMED_PERIODS
from schedcat.util.time import ms2us
import schedcat.model.resources as resources

from segmented_task import *

import pandas as pd
import pylab as pl
import argparse

def test_segmented_ts():
    tasks = []
    #load_time, unload_time, dma_stream_time, is_segment_task, exec_cost, period, priority, deadline, id, cpu
    
    #task set is schedulable
    tasks.append(Segmented_Task(1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(Segmented_Task(1, 1, 1, True, 100, 1000, 2,  1000, 1, 0))
    tasks.append(Segmented_Task(1, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(Segmented_Task(1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))

    ts = Segmented_TaskSystem(tasks)

    tdma_slot_size = 2
    tdma_slot_size_wo_overhead = 1 
    tdma_round_length = 6
    
    #tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length
    #ts.non_streaming_sched_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)

    #tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length
    ts.streaming_sched_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)

    #for task in sd_ts:
    #    print(task)

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--test", help="Call a test function that creates and prints Segmented_Tasks", action="store_true")

args = parser.parse_args()

def main():
    if args.test:
        test_segmented_ts()

if __name__ == "__main__":
    main()
