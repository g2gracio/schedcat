from capgs_utils import *

def generate_array_from_ts(capgs_ts_file, packing, task_id):
	original_ts = read_capgs_ts_from_file(capgs_ts_file)

	hrt_tasks = 0
	srt_tasks = 0
	be_tasks = 0
	
	for task in original_ts:
		if task.task_type == 0:
			hrt_tasks += 1
		elif task.task_type == 1:
			srt_tasks += 1
		else:
			be_tasks += 1
	
	print "\n#define TASKS %d" % len(original_ts)
	print "#define HRT_TASKS %d" % hrt_tasks
	print "#define SRT_TASKS %d" % srt_tasks
	print "#define BE_TASKS %d" % be_tasks
	
	print "\n#define PARAMETER_TABLE_ID %d" % task_id
	print "#define PACKING_ALGORITHM \"%s\"" % packing
	
	# func, group, id, partition, period, task_type, wcet
	print "\nstruct Task_Set {"
	print "\tint (*func)(int,int,int);"
	print "\tint group;"
	print "\tint id;"
	print "\tint cpu;"
	print "\tint period;"
	print "\tint type;"
	print "\tint wcet;"
	print "} set[TASKS] = {" 

	for task in original_ts:
		print "{",
		if task.task_type == 0:
			print "&hrt_func, ",
		elif task.task_type == 1:
			print "&srt_func, ",
		else:
			print "&be_func, ",
		print "%d, %d, %d, %d, %d, %d }," % (task.group, task.id, task.partition, task.period, task.task_type, task.cost)		
	print "};"


def main(files):
	filename_cap = "capgs_test_"
	filename_bfd = "bfd_test_"

	table_index = 0
	for i in range(files):
		print "\n\n#elif(PARAMETER_TABLE == %d)" % table_index
		filename = filename_cap + str(i)
		#print "\n\nGenerating CAPGS Task set %d" % i
		packing_algorithm = "CAPGS"
		generate_array_from_ts(filename, packing_algorithm, i)
		table_index += 1
		
		print "\n\n#elif(PARAMETER_TABLE == %d)" % table_index
		filename = filename_bfd + str(i)
		#print "\n\nGenerating BFD Task set %d" % i
		packing_algorithm = "BFD"
		generate_array_from_ts(filename, packing_algorithm, i)
		table_index += 1

main(10)
