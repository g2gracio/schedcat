#!/usr/bin/env python
from __future__ import division
from math import ceil, floor
from schedcat.model.tasks import SporadicTask
from schedcat.model.tasks import TaskSystem
import schedcat.model.resources as resources
import schedcat.locking.bounds as bounds
from collections import defaultdict
from copy import deepcopy
from generator_emstada import gen_taskset
from datetime import datetime
import sys
import os
import optparse
import random
import schedcat.sched.fp.rta as rta
from schedcat.generator.tasksets import mkgen
import schedcat.generator.tasks as gen
from schedcat.util.time import ms2us
import schedcat.mapping.binpack as bp
from schedcat.overheads.locking import charge_spinlock_overheads
from schedcat.util.math import const, monotonic_pwlin


class Overheads(object):
    def __init__(self):
        self.__dict__['lock']        = const(0)
        self.__dict__['unlock']      = const(0)
        self.__dict__['read_lock']   = const(0)
        self.__dict__['read_unlock'] = const(0)
        self.__dict__['syscall_in']  = const(0)
        self.__dict__['syscall_out'] = const(0)


NAMED_PERIODS = {
# Named period distributions used in several UNC papers, in milliseconds.
    'uni-short'         : gen.uniform_int( 3,  33),
    'uni-moderate'      : gen.uniform_int(10, 100),
    'uni-long'          : gen.uniform_int(50, 250),

    'log-uni-short'     : gen.log_uniform_int( 3,  33),
    'log-uni-moderate'  : gen.log_uniform_int(10, 100),
    'log-uni-long'      : gen.log_uniform_int(50, 250),

    'homogeneous'       : gen.log_uniform_int(10,  100),
    'heterogeneous'     : gen.log_uniform_int(1, 1000),
}

NAMED_UTILIZATIONS = {
# Named utilization distributions used in several UNC papers, in milliseconds.
    'uni-light'     : gen.uniform(0.001, 0.1),
    'uni-medium'    : gen.uniform(0.1  , 0.4),
    'uni-heavy'     : gen.uniform(0.5  , 0.9),

    'exp-light'     : gen.exponential(0, 1, 0.10),
    'exp-medium'    : gen.exponential(0, 1, 0.25),
    'exp-heavy'     : gen.exponential(0, 1, 0.50),

    'bimo-light'    : gen.multimodal([(gen.uniform(0.001, 0.5), 8),
                                      (gen.uniform(0.5  , 0.9), 1)]),
    'bimo-medium'   : gen.multimodal([(gen.uniform(0.001, 0.5), 6),
                                      (gen.uniform(0.5  , 0.9), 3)]),
    'bimo-heavy'    : gen.multimodal([(gen.uniform(0.001, 0.5), 4),
                                      (gen.uniform(0.5  , 0.9), 5)]),
    'light'         : gen.uniform(0.05, 0.1),
    'medium'        : gen.uniform(0.1  , 0.25),
}


# Critical section lenght in microseconds
CSLENGTH = { 'short'  : lambda: random.randint(1,   25),
             'medium' : lambda: random.randint(25,  100),
             'long'   : lambda: random.randint(100, 500),
#            'long'   : lambda: random.randint(5, 1280),
}


def find_prio_ceilings(taskset, nres):
    
    per_resource_tasks = find_per_resource_tasks(taskset)
    prio_ceilings = {}
    for res_id in range(nres):
        prio_ceilings[res_id] = len(taskset) + 1
    for res_id in range(nres):
        for t in per_resource_tasks[res_id]:
            if t.id < prio_ceilings[res_id]:
                prio_ceilings[res_id] = t.id
    return prio_ceilings


def find_prio_ceilings_srp(taskset, nres):
    
    per_resource_tasks = find_per_resource_tasks(taskset)
    prio_ceilings = {}
    for res_id in range(nres):
        prio_ceilings[res_id] = len(taskset) + 1
    for res_id in range(nres):
        for t in per_resource_tasks[res_id]:
            if t.deadline < prio_ceilings[res_id]:
                prio_ceilings[res_id] = t.deadline
    return prio_ceilings


def find_per_resource_tasks(taskset):
    
    per_resource_tasks = defaultdict(set)
    for t in taskset:
        for res_id in t.resmodel:
            if t.resmodel[res_id].max_length > 0: 
                per_resource_tasks[res_id].add(t)
    return per_resource_tasks


# Stack Resource Sharing Policy (Baruah's version)
def blocking_SRP(taskset, nres, L):
    
    delay_by_lower = 0
    if L > taskset.max_deadline():
        return delay_by_lower
    per_resource_tasks = find_per_resource_tasks(taskset)
    lp_tasks = [task for task in taskset if task.deadline >  L]
    hp_tasks = [task for task in taskset if task.deadline <= L]
    delay_by_lower = 0
    for res_id in range(nres):
        for hp_task in hp_tasks:
            if hp_task in per_resource_tasks[res_id]:
                for lp_task in lp_tasks:
                    if lp_task in per_resource_tasks[res_id]:
                        length = lp_task.resmodel[res_id].max_length
                        delay_by_lower = max(delay_by_lower, length)

    return delay_by_lower


# Priority Ceiling Protocol
def blocking_PCP(nres, prio_ceilings, task, taskset):
    
    delay_by_lower  = 0
    for t in taskset.with_lower_priority_than(task):
        for res_id in range(nres):
            ceiling = prio_ceilings[res_id]
            if ceiling <= task.id:
                length = t.resmodel[res_id].max_length
                delay_by_lower = max(delay_by_lower, length)

    return delay_by_lower


# Priority Inheritance Protocol
def blocking_PIP(nres, prio_ceilings, task, taskset):

    sum_delay_by_lower = 0
    for t in taskset.with_lower_priority_than(task):
        delay_by_lower = 0
        for res_id in range(nres):
            ceiling = prio_ceilings[res_id]
            if ceiling <= task.id:
                length = t.resmodel[res_id].max_length
                delay_by_lower = max(delay_by_lower, length)
        sum_delay_by_lower = sum_delay_by_lower + delay_by_lower

    sum_delay_by_res = 0
    for res_id in range(nres):
        delay_by_res = 0
        for t in taskset.with_lower_priority_than(task):
            ceiling = prio_ceilings[res_id]
            if ceiling <= task.id:
                length = t.resmodel[res_id].max_length
                delay_by_res = max(delay_by_res, length)
        sum_delay_by_res = sum_delay_by_res + delay_by_res

    return min(sum_delay_by_lower,sum_delay_by_res)


# PCP analysis
def blocking(taskset, nres, protocol_name):
    
    prio_ceilings = find_prio_ceilings(taskset, nres)
    for task in taskset:
        if  protocol_name  == "PCP":
            task.blocked    = blocking_PCP(nres, prio_ceilings, task, taskset)
        elif protocol_name == "PIP":
            task.blocked    = blocking_PIP(nres, prio_ceilings, task, taskset)
        task.jitter     = 0
        task.suspended  = 0
    return taskset


#  EDF schedulability test utlization based
def edf_test_util(taskset, nres):
    
    u = 0
    for task in taskset:
        u = u + task.utilization()
        B = blocking_SRP(taskset, nres, task.deadline)
        if u + B/task.deadline > 1:
            return False
    return True


# EDF schedulability test with processor demand criterion
def edf_test_pcrit(taskset, nres):
    
    if taskset.utilization() > 1:
        return False

    if len(taskset) == 0:
        return True

    D = taskset.max_deadline()
    deadlines = [ task.deadline for task in taskset ]
    deadlines.sort()
    deadlines_iterator = iter(deadlines)
    next_deadline = next(deadlines_iterator)
    B = blocking_SRP(taskset, nres, next_deadline)

    for d in taskset.dbf_points_of_change(D):
        if d > next_deadline:
            next_deadline = next(deadlines_iterator)
            B = blocking_SRP(taskset, nres, next_deadline)
        if taskset.dbf(d) + B > d:
            return False

    return True


# Rate Monotnic test utlization based
def rm_test_util(taskset_in):

    def sched_util(n): return n*(2**(1/n)-1)

    taskset = deepcopy(taskset_in)

    # Rate Monotonic priority assignment
    taskset.assign_ids_by_deadline()
    taskset.sort_by_deadline()

    n = 0
    u = 0
    for task in taskset:
        n = n + 1
        u = u + task.utilization()

        if not 'blocked' in task.__dict__:
            task.blocked = 0

        if u + task.blocked/task.period > sched_util(n):
            return False

    return True


def erase_blocking(taskset):
    
    for task in taskset:
        task.blocked         = 0
        task.jitter          = 0
        task.suspended       = 0
        task.remote_blocking = 0
        task.response_time   = task.deadline


def generate_lock_taskset(util_name, period_name, cap,
                                cslength, nres, pacc, number):
    
    generator = mkgen(NAMED_UTILIZATIONS[util_name],
                      NAMED_PERIODS[period_name])
    generated_sets = []
    for i in range(number):
        taskset = generator(max_util=cap, time_conversion=ms2us)
        resources.initialize_resource_model(taskset)
        for task in taskset:
            for res_id in range(nres):
                if random.random() < pacc:
                    length = CSLENGTH[cslength]
                    task.resmodel[res_id].add_request(length())
        generated_sets.append(taskset)
    return generated_sets


def test_single_sched_with_locks(util_name, period_name, util_cap, cslength, nres, pacc, num_tasksets, test_type = 'util', oheads = None):
    
    # test fail counters
    non_schedulable_lockfree_tasksets_edf = 0
    non_schedulable_lockfree_tasksets_rm  = 0

    non_schedulable_tasksets_srp          = 0
    non_schedulable_tasksets_pip          = 0
    non_schedulable_tasksets_pcp          = 0

    non_schedulable_tasksets_srp_ov       = 0
    non_schedulable_tasksets_pip_ov       = 0
    non_schedulable_tasksets_pcp_ov       = 0
    non_schedulable_tasksets_ipcp_ov      = 0



    # generate num_tasksets tasksets with locks
    tasksets = generate_lock_taskset(util_name, period_name, util_cap, cslength, nres, pacc, num_tasksets)

    # EDF test
    for taskset in tasksets:
        if taskset.utilization() > 1:
            non_schedulable_tasksets_srp          = non_schedulable_tasksets_srp + 1
            non_schedulable_lockfree_tasksets_edf = non_schedulable_lockfree_tasksets_edf + 1
            non_schedulable_tasksets_srp_ov       = non_schedulable_tasksets_srp_ov + 1
        else:
            if   test_type == "util" and not edf_test_util(taskset, nres):
                non_schedulable_tasksets_srp    = non_schedulable_tasksets_srp + 1
                non_schedulable_tasksets_srp_ov = non_schedulable_tasksets_srp_ov + 1
                continue
            elif test_type == "exact" and not edf_test_pcrit(taskset, nres):
                non_schedulable_tasksets_srp = non_schedulable_tasksets_srp + 1
                non_schedulable_tasksets_srp_ov = non_schedulable_tasksets_srp_ov + 1
                continue

            if oheads:
                overhead_taskset = deepcopy(taskset)
                erase_blocking(overhead_taskset)
                charge_semaphore_overheads(oheads['SRP'], overhead_taskset, 'SRP')
                overhead_taskset = blocking(overhead_taskset, nres, "SRP")

                if test_type == "util" and not edf_test_util(overhead_taskset, nres):
                    non_schedulable_tasksets_srp_ov = non_schedulable_tasksets_srp_ov + 1
                elif test_type == "exact" and not edf_test_pcrit(overhead_taskset, nres):
                    non_schedulable_tasksets_srp_ov = non_schedulable_tasksets_srp_ov + 1


    # RM test
    for taskset in tasksets:
        # Rate Monotonic priority assignment
        taskset.assign_ids_by_deadline()
        taskset.sort_by_deadline()

        # lockfree system schedulability
        if (taskset.utilization() > 1) or \
           (test_type == "util"  and rm_test_util(taskset) == False) or \
           (test_type == "exact" and rta.bound_response_times(1, taskset) == False):
           
            non_schedulable_tasksets_pcp         = non_schedulable_tasksets_pcp + 1
            non_schedulable_tasksets_pip         = non_schedulable_tasksets_pip + 1
            non_schedulable_lockfree_tasksets_rm = non_schedulable_lockfree_tasksets_rm + 1
            non_schedulable_tasksets_pcp_ov      = non_schedulable_tasksets_pcp_ov + 1
            non_schedulable_tasksets_pip_ov      = non_schedulable_tasksets_pip_ov + 1
            non_schedulable_tasksets_ipcp_ov     = non_schedulable_tasksets_ipcp_ov + 1
            continue

        # PCP schedulability
        erase_blocking(taskset)
        taskset = blocking(taskset, nres, "PCP")
        if (test_type == "util"  and rm_test_util(taskset) == False) or \
           (test_type == "exact" and rta.bound_response_times(1, taskset) == False):

            non_schedulable_tasksets_pcp     = non_schedulable_tasksets_pcp + 1
            non_schedulable_tasksets_pcp_ov  = non_schedulable_tasksets_pcp_ov + 1
            non_schedulable_tasksets_ipcp_ov = non_schedulable_tasksets_ipcp_ov + 1

        elif oheads:
            erase_blocking(taskset)
            overhead_taskset = deepcopy(taskset)
            charge_semaphore_overheads(oheads['PCP'], overhead_taskset, 'PCP')
            overhead_taskset = blocking(overhead_taskset, nres, "PCP")

            if (test_type == "util"  and rm_test_util(overhead_taskset) == False) or \
               (test_type == "exact" and rta.bound_response_times(1, overhead_taskset) == False):

                    non_schedulable_tasksets_pcp_ov = non_schedulable_tasksets_pcp_ov + 1

            # IPCP (test only when overheads, otherwise is the same as PCP)
            overhead_taskset = deepcopy(taskset)
            charge_semaphore_overheads(oheads['IPCP'], overhead_taskset, 'IPCP')
            overhead_taskset = blocking(overhead_taskset, nres, "IPCP")

            if (test_type == "util"  and rm_test_util(overhead_taskset) == False) or \
               (test_type == "exact" and rta.bound_response_times(1, overhead_taskset) == False):

                    non_schedulable_tasksets_ipcp_ov = non_schedulable_tasksets_ipcp_ov + 1


        # PIP schedulability
        erase_blocking(taskset)
        taskset = blocking(taskset, nres, "PIP")
        if (test_type == "util"  and rm_test_util(taskset) == False) or \
           (test_type == "exact" and rta.bound_response_times(1, taskset) == False):

            non_schedulable_tasksets_pip    = non_schedulable_tasksets_pip + 1
            non_schedulable_tasksets_pip_ov = non_schedulable_tasksets_pip_ov + 1
            continue

        elif oheads:
            erase_blocking(taskset)
            overhead_taskset = deepcopy(taskset)
            charge_semaphore_overheads(oheads['PIP'], overhead_taskset, 'PIP')
            overhead_taskset = blocking(overhead_taskset, nres, "PIP")

            if (test_type == "util"  and rm_test_util(overhead_taskset) == False) or \
               (test_type == "exact" and rta.bound_response_times(1, overhead_taskset) == False):

                    non_schedulable_tasksets_pip_ov = non_schedulable_tasksets_pip_ov + 1



    success_ratio_lock_pip     = (num_tasksets - non_schedulable_tasksets_pip)          / num_tasksets
    success_ratio_lock_pcp     = (num_tasksets - non_schedulable_tasksets_pcp)          / num_tasksets
    success_ratio_lock_srp     = (num_tasksets - non_schedulable_tasksets_srp)          / num_tasksets
    success_ratio_lockfree_rm  = (num_tasksets - non_schedulable_lockfree_tasksets_rm)  / num_tasksets
    success_ratio_lockfree_edf = (num_tasksets - non_schedulable_lockfree_tasksets_edf) / num_tasksets

    if oheads:
        success_ratio_lock_pip_ov     = (num_tasksets - non_schedulable_tasksets_pip_ov)  / num_tasksets
        success_ratio_lock_pcp_ov     = (num_tasksets - non_schedulable_tasksets_pcp_ov)  / num_tasksets
        success_ratio_lock_ipcp_ov    = (num_tasksets - non_schedulable_tasksets_ipcp_ov) / num_tasksets
        success_ratio_lock_srp_ov     = (num_tasksets - non_schedulable_tasksets_srp_ov)  / num_tasksets

        return success_ratio_lock_pip, success_ratio_lock_pip_ov, success_ratio_lock_pcp, success_ratio_lock_pcp_ov, \
               success_ratio_lock_pcp, success_ratio_lock_ipcp_ov, \
               success_ratio_lock_srp, success_ratio_lock_srp_ov, success_ratio_lockfree_rm, success_ratio_lockfree_edf


    # IPCP wo overheads = PCP wo overheads
    return success_ratio_lock_pip, success_ratio_lock_pcp, success_ratio_lock_pcp, success_ratio_lock_srp, success_ratio_lockfree_rm, success_ratio_lockfree_edf 



def test_multi_sched_with_locks(util_name, period_name, util_cap, cslength, nres, pacc, num_tasksets, N, test_type="util", oheads = None):
  
    # test fail counter
    non_schedulable_tasksets_mpcp         = 0
    non_schedulable_tasksets_msrp         = 0
    non_schedulable_lockfree_tasksets_rm  = 0
    non_schedulable_lockfree_tasksets_edf = 0

    non_schedulable_tasksets_mpcp_ov      = 0
    non_schedulable_tasksets_msrp_ov      = 0

    if oheads == None:
        print "Not implemented"
        return False

    global bin_packing_error
    def misfit(task):
        global bin_packing_error
        bin_packing_error = True
 
    
    # generate num_tasksets tasksets with locks
    tasksets = generate_lock_taskset(util_name, period_name, util_cap, cslength, nres, pacc, num_tasksets)

    # assign tasks to the processors with the worst-fit
    for taskset in tasksets:

        bin_packing_error = False
        subsets = bp.worst_fit(taskset, N, 1.0, lambda t: t.utilization(),misfit)

        if bin_packing_error:
            non_schedulable_tasksets_mpcp         = non_schedulable_tasksets_mpcp + 1
            non_schedulable_tasksets_msrp         = non_schedulable_tasksets_msrp + 1
            non_schedulable_lockfree_tasksets_rm  = non_schedulable_lockfree_tasksets_rm  + 1
            non_schedulable_lockfree_tasksets_edf = non_schedulable_lockfree_tasksets_edf + 1
            non_schedulable_tasksets_mpcp_ov      = non_schedulable_tasksets_mpcp_ov + 1
            non_schedulable_tasksets_msrp_ov      = non_schedulable_tasksets_msrp_ov + 1
            continue

        for partition_num, subset in enumerate(subsets):
            for task in subset:
                task.partition     = partition_num
                task.response_time = task.period  # this is needed for RTA

        # Rate Monotonic priority assignment
        taskset.assign_ids_by_deadline()
        taskset.sort_by_deadline()

        # test RM schedulability without locks
        for subset in subsets:
            partition_taskset = TaskSystem(subset)
            partition_taskset.sort_by_deadline()
            if (test_type == "exact" and rta.bound_response_times(1, partition_taskset) == False) or \
               (test_type == "util"  and rm_test_util(partition_taskset) == False):

                non_schedulable_lockfree_tasksets_rm  = non_schedulable_lockfree_tasksets_rm + 1
                non_schedulable_tasksets_mpcp_ov      = non_schedulable_tasksets_mpcp_ov + 1
                non_schedulable_tasksets_mpcp         = non_schedulable_tasksets_mpcp + 1
                break

        # test RM schedulability with locks
        else:
            erase_blocking(taskset)
            
            # assign preemption levels
            bounds.assign_fp_preemption_levels(taskset)

            # compute MPCP suspension-based bounds on blocking factors
            bounds.apply_mpcp_bounds(taskset, use_virtual_spin=False)

            # check schedulability of each partition
            for subset in subsets:
                partition_taskset = TaskSystem(subset)
                partition_taskset.sort_by_deadline()
                # if not schedulable: increment the fail counter
                if (test_type == "exact" and rta.bound_response_times(1, partition_taskset) == False) or \
                   (test_type == "util"  and rm_test_util(partition_taskset) == False):

                    non_schedulable_tasksets_mpcp    = non_schedulable_tasksets_mpcp    + 1
                    non_schedulable_tasksets_mpcp_ov = non_schedulable_tasksets_mpcp_ov + 1
                    break
            else:
                # test RM schedulability with locks including the overheads
                erase_blocking(taskset)
                ov_taskset = deepcopy(taskset)
                charge_semaphore_overheads(oheads['MPCP'], ov_taskset, 'MPCP')

                # rerun task partitioning
                bin_packing_error = False
                ov_subsets = bp.worst_fit(ov_taskset, N, 1.0, lambda t: t.utilization(), misfit)
                if bin_packing_error:
                    non_schedulable_tasksets_mpcp_ov = non_schedulable_tasksets_mpcp_ov + 1
                else:

                    # assign preemption levels
                    bounds.assign_fp_preemption_levels(ov_taskset)

                    # compute MPCP suspension-based bounds on blocking factors
                    bounds.apply_mpcp_bounds(ov_taskset, use_virtual_spin=False)

                    # check schedulability of each partition
                    for subset in ov_subsets:
                        partition_taskset = TaskSystem(subset)
                        partition_taskset.sort_by_deadline()
                        # if not schedulable: increment the fail counter
                        if (test_type == "exact" and rta.bound_response_times(1, partition_taskset) == False) or \
                           (test_type == "util"  and rm_test_util(partition_taskset) == False):

                               non_schedulable_tasksets_mpcp_ov = non_schedulable_tasksets_mpcp_ov + 1
                               break
        


        erase_blocking(taskset)
        # test EDF schedulability without the locks
        for subset in subsets:
            partition_taskset = TaskSystem(subset)
            if partition_taskset.utilization() > 1.0:
                
                non_schedulable_lockfree_tasksets_edf = non_schedulable_lockfree_tasksets_edf + 1
                non_schedulable_tasksets_msrp         = non_schedulable_tasksets_msrp + 1
                non_schedulable_tasksets_msrp_ov      = non_schedulable_tasksets_msrp_ov + 1
                break
        else:
            erase_blocking(taskset)

            # assign preemption levels
            bounds.assign_edf_preemption_levels(taskset)

            # conpute MSRP spinlock-based bound on blocking factors
            bounds.apply_msrp_bounds(taskset,N)

            # test EDF schedulability with locks
            for subset in subsets:
                partition_taskset = TaskSystem(subset)
                if (test_type == "exact" and edf_test_pcrit(partition_taskset, nres) == False) or\
                   (test_type == "util"  and edf_test_util(partition_taskset, nres) == False):

                    non_schedulable_tasksets_msrp    = non_schedulable_tasksets_msrp + 1
                    non_schedulable_tasksets_msrp_ov = non_schedulable_tasksets_msrp_ov + 1 
                    break
            else:
                # test RM schedulability with locks including the overheads
                erase_blocking(taskset)
                ov_taskset = deepcopy(taskset)
                charge_semaphore_overheads(oheads['MSRP'], ov_taskset, 'MSRP')

                # rerun task partitioning
                bin_packing_error = False
                ov_subsets = bp.worst_fit(ov_taskset, N, 1.0, lambda t: t.utilization(), misfit)
                if bin_packing_error:
                    non_schedulable_tasksets_msrp_ov = non_schedulable_tasksets_msrp_ov + 1
                else:
                    # assign preemption levels
                    bounds.assign_edf_preemption_levels(ov_taskset)

                    # conpute MSRP spinlock-based bound on blocking factors
                    bounds.apply_msrp_bounds(ov_taskset,N)

                    # test EDF schedulability with locks including the overheads
                    for subset in ov_subsets:
                        partition_taskset = TaskSystem(subset)
                        if (test_type == "exact" and edf_test_pcrit(partition_taskset, nres) == False) or\
                           (test_type == "util"  and edf_test_util(partition_taskset, nres) == False):

                            non_schedulable_tasksets_msrp_ov = non_schedulable_tasksets_msrp_ov + 1 
                            break
  



    success_ratio_lockfree_rm  = (num_tasksets - non_schedulable_lockfree_tasksets_rm)  / num_tasksets
    success_ratio_lockfree_edf = (num_tasksets - non_schedulable_lockfree_tasksets_edf) / num_tasksets
    success_ratio_lock_mpcp    = (num_tasksets - non_schedulable_tasksets_mpcp)         / num_tasksets
    success_ratio_lock_msrp    = (num_tasksets - non_schedulable_tasksets_msrp)         / num_tasksets
    success_ratio_lock_mpcp_ov = (num_tasksets - non_schedulable_tasksets_mpcp_ov)      / num_tasksets
    success_ratio_lock_msrp_ov = (num_tasksets - non_schedulable_tasksets_msrp_ov)      / num_tasksets



    return success_ratio_lock_mpcp, success_ratio_lock_mpcp_ov,  \
           success_ratio_lock_msrp, success_ratio_lock_msrp_ov,  \
           success_ratio_lockfree_rm, success_ratio_lockfree_edf


def create_log_file(util_name, period_name, cslength, nres, pacc, num_tasksets, N, test_type):
    
    now = datetime.now()
    if not os.path.exists('dir_results'):
        os.makedirs('dir_results')
    if N == 1:
        dirname  = "dir_results/lock/single" 
    else:
        dirname = "dir_results/lock/multi"
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    filename = str(N) + "_cores"   \
                      + "_util_" + str(util_name) + "_periods_" +str(period_name)  \
                      + "_pacc_" + str(pacc) + "_nres_" + str(nres) + "_cs_" + str(cslength) \
                      + "_numts_" + str(num_tasksets) + "_testtype_" + str(test_type) + "_" \
                      +  now.strftime("%d_%m_%H_%M_%S")
    filename = dirname + "/" + filename + ".log"
    return open(filename,"w")


# Load overheads from file
def read_overheads_from_file(filename):
    
    f = open(filename,"r")

    # get the names of the protocols, get rid of white spaces
    lines   = f.readlines()
    headers = lines[0].replace("\t", "")
    headers = headers.replace("\n","")
    headers = headers.replace("\r","")
    headers = headers.split(';')

    overheads = defaultdict(list)

    # load the data into overheads dictionary
    for line in lines[1:]:
        line = line.replace("\t", "")
        line = line.replace("\r", "")
        line = line.replace("\n", "")
        l=0
        for col in line.split(';'):
            try:
                # nanosecond to microsecond
                overheads[headers[l]].append(float(col)/1000)
            except ValueError:
                pass
            l=l+1
    f.close()

    # copy the raw data from overheads into the Overheads objects
    oheads = defaultdict(lambda:Overheads())
    for protocol_name in ['PIP', 'PCP', 'IPCP', 'SRP', 'MPCP', 'MSRP']:
        for sig in [' p', ' v']:
            protocol_name_sig = protocol_name + sig
            ntasks = len(overheads[protocol_name_sig])
            points = zip([x for x in range(1,ntasks)], overheads[protocol_name_sig])
            if sig == ' p':
                oheads[protocol_name].__dict__['lock']        = monotonic_pwlin(points)
                oheads[protocol_name].__dict__['read_lock']   = monotonic_pwlin(points)
            elif sig == ' v':
                oheads[protocol_name].__dict__['unlock']      = monotonic_pwlin(points)
                oheads[protocol_name].__dict__['read_unlock'] = monotonic_pwlin(points)
        oheads[protocol_name].__dict__['syscall_in']   = const(0)
        oheads[protocol_name].__dict__['syscall_out']  = const(0)
        oheads[protocol_name].__dict__['ctx_switch']   = const(0.353)
        oheads[protocol_name].__dict__['syscall_in']   = monotonic_pwlin([ (5,0.869),  (15,  1.102), (25,  1.579), \
                                                                           (50,2.596), (75,3.318),   (100, 4.415), (125, 6.430) ])

    return oheads


def charge_semaphore_overheads(oheads, tasks, protocol):
    if oheads is None or not tasks:
        return tasks

    ntasks = len(tasks)
    lock   = oheads.lock(ntasks)
    unlock = oheads.unlock(ntasks)
    sysin  = oheads.syscall_in(ntasks)
    sysout = oheads.syscall_out(ntasks)
    sched  = oheads.ctx_switch(ntasks)

    if sysin == 0:
        sysin = oheads.ctx_switch(ntasks)

    if sysout == 0:
        sysout = oheads.ctx_switch(ntasks)

    # for every protocol add release and deschedule overheads
    extra_wcet = sysin + sysout
    for t in tasks:
        t.cost += int(ceil(extra_wcet))

    wcost = lock + unlock
    for t in tasks:
        for res_id in t.resmodel:
                req = t.resmodel[res_id]
                if req.max_writes:
                    req.max_write_length += wcost
                    req.max_write_length = int(ceil(req.max_write_length))
                    extra_wcet += req.max_writes * wcost
        t.cost += int(ceil(extra_wcet))


    # no context switches due to the blocking
    if protocol == 'SRP' or protocol == 'IPCP' or protocol == 'MSRP':
        return tasks

    per_resource_tasks = find_per_resource_tasks(tasks)
    cs_increase = 2 * sched

    # task can be blocked at most once by a lower priority task
    # add one context switch when blocking starts and one context switch when it ends if task shares at least one resource with a lower priority task
    if protocol == 'PCP' or protocol == 'MPCP':
        for t in tasks:
            for res_id in t.resmodel:
                req = t.resmodel[res_id]
                if req.max_writes:
                    for ti in per_resource_tasks[res_id]:
                        if ti.id > t.id:
                            t.cost += int(ceil(cs_increase))
                            break
                    else:
                        continue
                    break
        return tasks

    # task can be blocked at each resource access by a lower priority task
    # add one context switch when blocking starts and one context switch when it ends for each resource shared with a lower priority task
    if protocol == 'PIP':
        for t in tasks:
            for res_id in t.resmodel:
                req = t.resmodel[res_id]
                if req.max_writes:
                    for ti in per_resource_tasks[res_id]:
                        if ti.id > t.id:
                            t.cost += int(ceil(cs_increase))
                            break

        return tasks



def main():

    parser = optparse.OptionParser()

    parser.add_option("-N", "--core-number",
                      type="int", dest="core_number",
                      default="1")

    parser.add_option("-r", "--runs",
                      type="int", dest="runs",
                      default="100")

    parser.add_option("-p", "--pacc",
                      type="float", dest="pacc",
                      default="0.25")

    parser.add_option("-n", "--nres",
                      type="int", dest="nres",
                      default="4")

    parser.add_option("-t", "--test",
                      type="string", dest="test_type",
                      help="util, exact",
                      default="util")

    parser.add_option("-c", "--cs",
                      type="string", dest="cslength",
                      help="short, medium, long",
                      default="short")

    parser.add_option("-P", "--periods",
                      type="string", dest="period_name",
                      help="uni-short, uni-moderate, uni-long, log-uni-short, log-uni-moderate, log-uni-long, homogeneous, heterogeneous",
                      default="homogeneous")

    parser.add_option("-u", "--utilization",
                      type="string", dest="util_name",
                      help="uni-light, uni-medium, uni-heavy, exp-light, exp-medium, exp-heavy, bimo-light, bimo-medium, bimo-heavy, light, medium",
                      default="exp-light")



    (options, args) = parser.parse_args()

    N            = options.core_number
    num_tasksets = options.runs
    util_name    = options.util_name
    period_name  = options.period_name
    cslength     = options.cslength
    test_type    = options.test_type
    pacc         = options.pacc
    nres         = options.nres

    oheads = read_overheads_from_file("run_time_overhead_with_sleep_wakeup.csv")

    if N == 1:
        f = create_log_file(util_name, period_name, cslength, nres, pacc, num_tasksets, N, test_type)
        f.write("U\tPIP\tPIP-ov\tPCP\tPCP-ov\tIPCP\tIPCP-ov\tSRP\tSRP-ov\tRM\tEDF\n")
        for u_cap in range(0,105,5):
            u_cap /= 100
            print u_cap
            (result_pip, result_pip_ov,result_pcp,result_pcp_ov, result_ipcp, result_ipcp_ov, result_srp,result_srp_ov,result_rm,result_edf) = \
            test_single_sched_with_locks(util_name, period_name, u_cap, cslength, nres, pacc, num_tasksets, test_type, oheads)
            f.write ("%.2f\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % \
                (u_cap,result_pip,result_pip_ov,result_pcp,result_pcp_ov,result_ipcp,result_ipcp_ov,result_srp,result_srp_ov,result_rm,result_edf))
        f.close()

    else:
        f = create_log_file(util_name, period_name, cslength, nres, pacc, num_tasksets, N, test_type )
        f.write("U\tMPCP\tMPCP-ov\tMSRP\tMSRP-ov\tRM\tEDF\n")
        for u_cap in range(0,N*100+5,5):
            u_cap /= 100
            print u_cap
            (result_mpcp, result_mpcp_ov,result_msrp, result_msrp_ov, result_rm, result_edf) = \
            test_multi_sched_with_locks(util_name, period_name, u_cap, cslength, nres, pacc, num_tasksets, N, test_type, oheads)
            f.write ("%.2f\t%s\t%s\t%s\t%s\t%s\t%s\n" % \
                (u_cap,result_mpcp, result_mpcp_ov,result_msrp, result_msrp_ov, result_rm, result_edf))

        f.close()
   
    
if __name__ == "__main__":
    sys.exit(main())
