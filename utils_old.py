import xml.etree.ElementTree as ET
from schedcat.model.serialize import write_xml, task, set_attribute, subtag_for_attribute, maybe_int, parse_resmodel, get_attribute, write
from schedcat.util.time import ms2us
import schedcat.model.resources as resources
from schedcat.model.tasks import TaskSystem
from schedcat.model.tasks import SporadicTask
from schedcat.mapping.binpack import ignore
import copy
from fractions import Fraction
from schedcat.util.math    import lcm

from math import ceil
from math import floor

def sd_task(t):
    tag = ET.Element('sd_task')
    set_attribute(tag, 'id', t)
    set_attribute(tag, 'period', t)
    set_attribute(tag, 'priority', t)
    set_attribute(tag, 'cost', t)
    set_attribute(tag, 'deadline', t)
    set_attribute(tag, 'cpu', t)
    set_attribute(tag, 'arrival_curve_jitter', t)
    set_attribute(tag, 'input_load_time', t)
    set_attribute(tag, 'output_unload_time', t)
    set_attribute(tag, 'load_time', t)
    set_attribute(tag, 'unload_time', t)
    set_attribute(tag, 'is_sd_task', t)
    set_attribute(tag, 'unload_time', t)

    rmodel = subtag_for_attribute(tag, t, 'resmodel', 'resources')
    if not rmodel is None:
        for res_id in t.resmodel:
            res_requirement(t.resmodel[res_id], rmodel)

    tag.task = t
    task.xml = tag
    return tag

def parse_sd_task(node):
    # arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, 
    # exec_cost, period, priority, deadline, id, cpu)

    arrival_curve_jitter = maybe_int(node.get('arrival_curve_jitter'))
    input_load_time    = maybe_int(node.get('input_load_time'))
    output_unload_time = maybe_int(node.get('output_unload_time'))
    load_time 		  = maybe_int(node.get('load_time'))
    unload_time 		  = maybe_int(node.get('unload_time'))
    is_sd_task = maybe_int(node.get('is_sd_task'))
    cost      = maybe_int(node.get('cost'))
    period    = maybe_int(node.get('period'))
    priority    = maybe_int(node.get('priority'))
    deadline = maybe_int(node.get('deadline'))
    id 		  = maybe_int(node.get('id')) 
    cpu = maybe_int(node.get('cpu')) 

    t = SD_Task(arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, cost, period, priority, deadline, id, cpu)

    resmodel = parse_resmodel(node)
    if not resmodel is None:
        t.resmodel = resmodel

    t.xml = node
    node.task = t
    return t

def parse_sd_taskset(node):
	tasks = [parse_sd_task(n) for n in node.findall('sd_task')]
	return SD_TaskSystem(tasks)

def read_sd_ts_from_file(file):
	tree = ET.ElementTree()
	tree.parse(file)
	root = tree.getroot()
	if root.tag == 'sd_taskset':
		ts = parse_sd_taskset(root)
		ts.xml = tree.getroot()
		return ts
	elif root.tag == 'sd_task':
		t = parse_sd_task(root)
		return t
	else:
		return None	

def sd_write(ts, fname):
	tag = ET.Element('sd_taskset')
	prop = ET.SubElement(tag, 'properties')
	prop.set('utilization', str(ts.utilization()))
	prop.set('utilization_q', str(ts.utilization_q()))    
	prop.set('density_q', str(ts.density_q()))
	prop.set('density', str(ts.density()))
	prop.set('count', str(len(ts)))

	hp = ts.hyperperiod()
	if hp:
		prop.set('hyperperiod', str(hp))

	for t in ts:
		tag.append(sd_task(t))
	
	write_xml(tag, fname)

'''
Synchronous Dataflow Task
Parameters are:
- Input Rate
- Input buffer size
- Output Rate
- Output buffer size
- Boolean if the task is a SD task (True is, False is not, is SporadicTask)
- CPU in which the task is assigned
'''
class SD_Task(SporadicTask):
    def __init__(self, arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task,
                exec_cost, period, priority, deadline=None, id=None, cpu=-1):
        SporadicTask.__init__(self, exec_cost, period, deadline, id)
        self.arrival_curve_jitter = float(arrival_curve_jitter)
        self.input_load_time = float(input_load_time)
        self.output_unload_time = float(output_unload_time)
        self.load_time = float(load_time)
        self.unload_time = float(unload_time)
        self.is_sd_task = is_sd_task # True if SD task, False if SporadicTask
        self.priority = priority
        self.cpu = cpu

        self.exec_final_i = 0
        self.exec_cons_i = 0

    def __repr__(self):
        idstr = ", id=%s" % self.id if self.id is not None else ""
        dstr  = ", deadline=%s" % self.deadline
        typestr = ", %s" % ('SD Task' if bool(self.is_sd_task) == True else 'SporadicTask')
        cpustr = ", cpu=%s" % self.cpu
        jitterst = ", jitter=%s" % self.arrival_curve_jitter
        inpstr = ", input_load_time=%s" % self.input_load_time
        outstr = ", output_unload_time=%s" % self.output_unload_time
        ldstr = ", load_time=%s" % self.load_time
        ulstr = ", unload_time=%s" % self.unload_time
        return "SD_Task(wcet=%s, period=%s, prio=%s, util=%.2f%s%s%s%s%s%s%s%s%s)" % \
        (self.cost, self.period, self.priority, self.utilization(), dstr, idstr, typestr, cpustr, jitterst, inpstr, outstr, ldstr, ulstr)

    def utilization(self):
        if self.id < 0:
            return 0.0
        return self.cost / float(self.period)

    def utilization_q(self):
        if self.id < 0:
            return 0.0
        return Fraction(self.cost, self.period)

    def density(self):
        if self.id < 0:
            return 0.0
        return self.cost / float(min(self.period, self.deadline))

    def density_q(self):
        if self.id < 0:
            return 0.0
        return Fraction(self.cost, min(self.period, self.deadline))

class SD_TaskSystem(TaskSystem):
    def __init__(self, tasks=[]):
        TaskSystem.__init__(self, tasks)
        self.dummy_task_id = 0

    def __str__(self):
        return "\n".join([str(t) for t in self])

    def __repr__(self):
        return "SD_TaskSystem([" + ", ".join([repr(t) for t in self]) + "])"

    def copy(self):
        ts = SD_TaskSystem((copy.deepcopy(t) for t in self))
        return ts

    # arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, 
    # exec_cost, period, priority, deadline, id, cpu)
    def print_ts(self):
        for t in self:
            print "Task[%d], p=%d, cost=%d, prio=%d, util=%.2f, arrival_curve_jitter=%d, input_load_time=%d, output_unload_time=%d, load_time=%d, unload_time=%d" % (t.id, t.period, t.cost, t.priority, t.utilization(), float(t.arrival_curve_jitter), float(t.input_load_time), float(t.output_unload_time), float(t.load_time), float(t.unload_time))
        return

    def hyperperiod(self):
        periods = []
        for t in self:
            if t.is_sd_task == 0:
                periods.append(t.period)
        return lcm(*periods)

    def task_from_id(self, t_id):
        for t in self:
            if t.id == int(t_id):
                return t
        return None

    #returns all tasks with highest priority than t_id
    def highest_prio_tasks(self, t_id):
        hp = []

        for t in self:
            if t.id != t_id and t.priority < self[t_id].priority:
                hp.append(t)
        return hp

    #returns all tasks with lowest priority than t_id
    def lowest_prio_tasks(self, t_id):
        lp = []

        for t in self:
            if t.id != t_id and t.priority > self[t_id].priority:
                lp.append(t)
        return lp

    def task_with_longest_unload_time(self):
        task = 0
        max = -1
        for t in self:
            if t.unload_time > max:
                max = t.unload_time
                task = t
        return task

    def task_with_longest_load_time(self):
        task = 0
        max = -1
        for t in self:
            if t.load_time > max:
                max = t.load_time
                task = t
        return task

    def task_with_longest_cost(self):
        task = 0
        max = -1
        for t in self:
            if t.cost > max:
                max = t.cost
                task = t
        return task

    def longest_two_cost_in_tasks(self, tasks):
        cost = []
        for t in tasks:
            cost.append(t.cost)
        cost.sort(reverse=True)
        return (cost[0], cost[1])

    def longest_two_output_in_tasks(self, tasks):
        out = []
        for t in tasks:
            out.append(t.output_unload_time)
        out.sort(reverse=True)
        return (out[0], out[1])

    def longest_cost_in_tasks(self, tasks):
        cost = -1
        for t in tasks:
            if t.cost > cost:
                cost = t.cost
        return cost

    def longest_output_in_tasks(self, tasks):
        out = -1
        for t in tasks:
            if t.output_unload_time > out:
                out = t.output_unload_time
        return out

    def longest_unload_in_tasks(self, tasks):
        ul = -1
        for t in tasks:
            if t.unload_time > ul:
                ul = t.unload_time
        return ul

    def longest_load_in_tasks(self, tasks):
        ld = -1
        for t in tasks:
            if t.load_time > ld:
                ld = t.load_time
        return ld

    def longest_input_in_tasks(self, tasks):
        input = -1
        for t in tasks:
            if t.input_load_time > input:
                input = t.input_load_time
        return input
    
    def Saud_schedulability_test(self):

        for task in self:
            #print "******************************"
            #print "\nSaud's Schedulability test for task", self.task_from_id(task.id)
            lp = self.lowest_prio_tasks(task.id)

            if lp == []: #there are no lowest priority tasks than the current under analysis task 'task'
                #print "There are not enough lowest priority tasks\n"
                t_l1 = self.create_dummy_task()
                t_l2 = self.create_dummy_task()
                lp.append(t_l1)
                lp.append(t_l2)
            elif len(lp) == 1:
                t_l1 = self.task_with_longest_load_time() # should be the task with longest load time
                t_l2 = self.create_dummy_task()
                lp.append(t_l2)
            else:
                #line 1 of the algorithm
                t_l1 = self.task_with_longest_load_time() # should be the task with longest load time
                t_l2 = self.task_with_longest_cost() # should be the task with longest cost

            t_u = self.task_with_longest_unload_time()

            #line 2 of the algorithm
            B = max(t_l2.cost, t_u.unload_time + t_l1.load_time)

            #line 3 of the algorithm
            r_ti = task.cost + B

            #print "Lp =", lp
            #print "Task", t_u.id, "has the longest unload time =",t_u.unload_time
            #print "B =", B
            #print "r_ti", r_ti
            #print

            #line 4 of the algorithm
            prev_r_ti = 0

            #staring loop - line 5 of the algorithm
            while r_ti != prev_r_ti:
                #line 6 of the algorithm
                prev_r_ti = r_ti

                # creates J_hp - line 7 of the algorithm
                hp = self.highest_prio_tasks(task.id)
                #print "Hp =", hp
                J_hp = []
                for j in hp:
                    #print "r_ti",r_ti,"task.cost",task.cost,"j.period",j.period
                    r = (float(r_ti) - task.cost) / j.period
                    #print "r =", r
                    k = int(ceil(r))  #t_jk is missing here
                    #print "k =",k
                    J_hp.append([j, k])

                #print "J_hp =", J_hp
                
                #creates the list of execution times - line 8 of the algorithm
                #creates the list of DMA load times - line 9 of the algorithm
                #creates the list of DMA unload times - line 10 of the algorithm
                E = []
                t = self.task_with_longest_cost()
                E.append(t.cost) # append the highest cost of any lower priority task
                LD = []
                LD.append(task.load_time)
                UD = []
                (max1, max2) = self.longest_two_cost_in_tasks(lp)
                UD.append(max1) # two highest unload cost of any two lowest priority task
                UD.append(max2)

                #ADDED list of input loading time and output unloading time
                I_LD = []
                I_LD.append(task.input_load_time) 
                O_UL = []
                (max1, max2) = self.longest_two_output_in_tasks(lp)
                O_UL.append(max1) # two highest output unload time of any two lowest priority task
                O_UL.append(max2)

                for tuple in J_hp: #tuple[0] has the task, tuple[1] has the number of jobs of the task
                    for j in xrange(tuple[1]): #iterates over the number of jobs and sum the task values
                        E.append(tuple[0].cost)
                        LD.append(tuple[0].load_time)
                        UD.append(tuple[0].unload_time)
                        I_LD.append(tuple[0].input_load_time) #ADDED
                        O_UL.append(tuple[0].output_unload_time) #ADDED

                #sort by decreasing order - lines 11 and 12
                LD.sort(reverse=True)
                UD.sort(reverse=True)

                #ADDED
                I_LD.sort(reverse=True)
                O_UL.sort(reverse=True)

                #creates DMA list - line 13
                DMA = []
                for j in xrange(len(LD)):
                    DMA.append(LD[j] + UD[j] + I_LD[j] + O_UL[j]) #I_LD and O_UD were added here

                #lines 14 and 15
                M = E + DMA
                M.sort(reverse=True)
                #print "M =", M

                #line 16
                H = 0
                for j in xrange(len(E)):
                    H = H + M[j]
                
                r_ti = task.cost + B + H
                #print "r_ti in the end of iteration =", r_ti

                if r_ti > task.deadline:
                    return False

                #I = len(hp) + 1
                #print "I =", I
        #print "******************************\n\n"
        return True

    def compute_rmin(self, task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):
        input = (ceil(task.input_load_time / float(tdma_slot_size_wo_overhead)) - 1) * tdma_round_length + tdma_slot_size
        #cost^min = cost here
        next_tdma = tdma_round_length - tdma_slot_size + ceil((task.cost - tdma_round_length + tdma_slot_size) / float(tdma_round_length)) * tdma_round_length
        output = (ceil(task.output_unload_time / float(tdma_slot_size_wo_overhead)) - 1) * tdma_round_length + tdma_slot_size
        #print "Task", task.id, "Rmin: Input part =", input, "next tdma part =", next_tdma, "output part =", output, "Rmin total =", (input + next_tdma + output)
        return input + next_tdma + output

    def create_dummy_task(self):
        self.dummy_task_id = self.dummy_task_id - 1
        #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
        return SD_Task(0, 0, 0, 0, 0, True, 0, 0, 0, 0, self.dummy_task_id, 0)

    def longest_output_time(self):
        max_out = -1
        for t in self:
            if t.output_unload_time > max_out:
                max_out = t.output_unload_time

        return max_out

    def longest_unload_time(self):
        max_ul = -1
        for t in self:
            if t.unload_time > max_ul:
                max_ul = t.output_unload_time

        return max_ul

    def longest_load_time(self, tasks):
        max_ld = -1
        for t in tasks:
            if t.load_time > max_ld:
                max_ld = t.load_time

        return max_ld

    def longest_input_time(self, tasks):
        max_in = -1
        for t in tasks:
            if t.input_load_time > max_in:
                max_in = t.input_load_time
                
        return max_in

    def compute_b(self, task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):
        lp = self.lowest_prio_tasks(task.id)

        t_l1_cmax = 0.0
        t_l1_out = 0.0
        t_l1_ul = 0.0

        t_l2_cmax = 0.0
        t_l2_ld = 0.0
        t_l2_in = 0.0

        #l1 does include "task" - t_l1 is got from lower priority tasks
        #l2 does not include "task" - t_l1 is got from lower priority tasks

        if len(lp) >= 1: # there is only one lower priority task
            t_l1_cmax = self.longest_cost_in_tasks(lp + [task])
            t_l1_out = self.longest_output_in_tasks(lp + [task])
            t_l1_ul = self.longest_unload_in_tasks(lp + [task])
        else: # #there are no lower priority task
            t_l1_cmax = task.cost
            t_l1_out = task.output_unload_time
            t_l1_ul = task.unload_time

        if len(lp) == 0:
            t_l2_cmax = 0.0
            t_l2_ld = 0.0
            t_l2_in = 0.0
        else: # at least one higher priority task plus the task under analysis
            t_l2_cmax = self.longest_cost_in_tasks(lp) 
            t_l2_ld = self.longest_load_in_tasks(lp)
            t_l2_in = self.longest_input_in_tasks(lp)

        #calculates the second part of the equation (7)
        out = self.longest_output_time() # highest output of any task in the system
        ul = self.longest_unload_time()  # highest unload of any task in the system

        hp = self.highest_prio_tasks(task.id)
        ld = self.longest_load_in_tasks(hp + [task])
        input = self.longest_input_in_tasks(hp + [task])

        part_1 = max(t_l1_cmax, tdma_slot_size + ceil((out + ul + t_l2_ld + t_l2_in) / tdma_slot_size_wo_overhead) * tdma_round_length)

        #calculates the third part of the equation (7)
        part_2 = max(t_l2_cmax, tdma_slot_size + ceil((t_l1_out + t_l1_ul + ld + input) / tdma_slot_size_wo_overhead) * tdma_round_length)
        
        #print "Task", "part_1 =", part_1, "part_2 =",part_2,task.id, "B =", (part_1 + part_2)
        return part_1 + part_2

    def find_max_dma_times(self):
        max_in = -1.0
        max_out = -1.0
        max_ld = -1.0
        max_ul = -1.0

        for t in self:
            if t.input_load_time > max_in:
                max_in = t.input_load_time

            if t.output_unload_time > max_out:
                max_out = t.output_unload_time

            if t.load_time > max_ld:
                max_ld = t.load_time

            if t.unload_time > max_ul:
                max_ul = t.unload_time
        
        return (max_in, max_out, max_ld, max_ul)

    def compute_f(self, task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):
        first_part = task.cost + tdma_slot_size + ceil(task.output_unload_time / float(tdma_slot_size_wo_overhead)) * tdma_round_length
        
        (max_in, max_out, max_ld, max_ul) = self.find_max_dma_times()
        second_part_a = tdma_slot_size + ceil((float(max_in) + max_out + max_ld + max_ul) / float(tdma_slot_size_wo_overhead)) * tdma_round_length
        second_part_b = ceil(task.output_unload_time / float(tdma_slot_size_wo_overhead)) * float(tdma_round_length)
        
        second_part = second_part_a + second_part_b
        
        #print "Task", task.id, "F =", max(first_part, second_part)
        
        return max(first_part, second_part)

    def compute_arrival_curve(self, task, t):
        return int(ceil((t + task.arrival_curve_jitter) / float(task.period)))

    def compute_final_i(self, task, k, B, R_i_k):
        hp = self.highest_prio_tasks(task.id)
        sum = 0.0
        for j in hp:
            t_j_alpha = self.compute_arrival_curve(j, R_i_k)
            sum = sum + t_j_alpha

        #print "Task", task.id, "FINAL_i =", min(k - 1, sum)
        return min(k - 1, sum)

    def compute_cons_i(self, task, k, final):
        #print "Task", task.id, "CONS_i =", max(0, k - 1 - final)
        return max(0, k - 1 - final)

    def compute_final_j(self, task_under_analysis, task, k, R_i_k):
        hp = self.highest_prio_tasks(task_under_analysis.id)

        sum = 0.0
        for l in hp:
            if l.id != task.id:
                l_alpha = self.compute_arrival_curve(l, R_i_k)
                sum = sum + l_alpha

        #print "Final j for task", task.id, " =", (min(self.compute_arrival_curve(task, R_i_k), sum + k))
        return min(self.compute_arrival_curve(task, R_i_k), sum + k)

    def compute_final_j_no_streaming(self, task_under_analysis, task, k, R_i_k):
        hp = self.highest_prio_tasks(task_under_analysis.id)

        sum = 0.0
        for l in hp:
            if l.id != task.id:
                l_alpha = self.compute_arrival_curve(l, R_i_k)
                sum = sum + l_alpha

        #print "Final j for task", task.id, " =", (min(self.compute_arrival_curve(task, R_i_k), sum + 1))
        return min(self.compute_arrival_curve(task, R_i_k), sum + 1)

    def compute_cons_j(self, task, R_i_k, final):
        #print "Cons j for task", task.id, " =",  max(0, self.compute_arrival_curve(task, R_i_k) - final)
        return max(0, self.compute_arrival_curve(task, R_i_k) - final)

    def find_longest_output_and_unload_times(self, tasks):
        max_out = -1
        max_ul = -1
        for t in tasks:
            if t.output_unload_time > max_out:
                max_out = t.output_unload_time
            if t.unload_time > max_ul:
                max_ul = t.unload_time
        return (max_out, max_ul)

    def find_cons(self, task, cons):
        for tuple in cons:
            if tuple[0].id == task.id:
                return tuple[1]
        print "ERROR in find_cons"
        return -1
    
    def find_final(self, task, final):
        for tuple in final:
            if tuple[0].id == task.id:
                return tuple[1]
        print "ERROR in find_final"
        return -1

    def compute_DMA_operations(self, task, cons_i, final_i, k, cons_j, final_j, tdma_slot_size_wo_overhead, tdma_round_length):
        OUT = []
        UL = []
        IN = []
        LD = []

        hp = self.highest_prio_tasks(task.id)
        lp = self.lowest_prio_tasks(task.id)

        if lp == []:
            lp.append(self.create_dummy_task())
        
        if hp == []:
            hp.append(self.create_dummy_task())

        (max_out, max_ul) = self.find_longest_output_and_unload_times(lp) 

        # Compute OUT
        OUT.append(max_out) #longest output of any task l2 > i (lower priority)

        for l in xrange(k - 1):
            OUT.append(task.output_unload_time)

        for j in hp:
            if j.id < 0: #dummy task
                c_j = 0
                f_j = 0
            else:
                c_j = self.find_cons(j, cons_j)
                f_j = self.find_final(j, final_j)
           
            for l in xrange(int(c_j + f_j)):
                OUT.append(j.output_unload_time)
            
            for l in xrange(int(c_j + f_j)):
                IN.append(j.input_load_time)

        # Compute UL
        UL.append(max_ul)

        for l in xrange(int(final_i)):
            UL.append(task.unload_time)

        for j in hp:
            if j.id < 0: #dummy task
                f_j = 0
            else:
                f_j = self.find_final(j, final_j)

            for l in xrange(int(f_j)):
                UL.append(j.unload_time)
                LD.append(j.load_time)

        # Compute IN
        for l in xrange(k):
            IN.append(task.input_load_time)

        # Compute LD
        for l in xrange(int((final_i + 1))):
            LD.append(task.load_time)


        for l in xrange(len(OUT)):
            OUT[l] = OUT[l] * (tdma_round_length / tdma_slot_size_wo_overhead)

        for l in xrange(len(IN)):
            IN[l] = IN[l] * (tdma_round_length / tdma_slot_size_wo_overhead)

        for l in xrange(len(LD)):
            LD[l] = LD[l] * (tdma_round_length / tdma_slot_size_wo_overhead)

        for l in xrange(len(UL)):
            UL[l] = UL[l] * (tdma_round_length / tdma_slot_size_wo_overhead)

        # Sort OUT, UL, IN, LD in decreasing order - Line 7
        OUT.sort(reverse=True)
        UL.sort(reverse=True)
        IN.sort(reverse=True)
        LD.sort(reverse=True)

        return (OUT, UL, IN, LD)

    def compute_DMA_operations_no_streaming(self, task, final_i, k, ignore_j, final_j, tdma_slot_size_wo_overhead, tdma_round_length):
        OUT = []
        UL = []
        IN = []
        LD = []   

        hp = self.highest_prio_tasks(task.id)
        lp = self.lowest_prio_tasks(task.id)

        if lp == []:
            lp.append(self.create_dummy_task())
        
        if hp == []:
            hp.append(self.create_dummy_task())

        (max_out, max_ul) = self.find_longest_output_and_unload_times(lp) 

        # Compute OUT
        OUT.append(max_out) #longest output of any task l2 > i (lower priority)

        for l in xrange(k - 1):
            OUT.append(task.output_unload_time)

        for j in hp:
            if j.id < 0: #dummy task                
                f_j = 0
            else:
                f_j = self.find_final(j, final_j)
           
            for l in xrange(int(f_j)):
                OUT.append(j.output_unload_time)
            
            for l in xrange(int(f_j)):
                IN.append(j.input_load_time)

        # Compute UL
        UL.append(max_ul)

        for l in xrange(int(final_i)):
            UL.append(task.unload_time)

        for j in hp:
            if j.id < 0: #dummy task
                f_j = 0
            else:
                f_j = self.find_final(j, final_j)

            for l in xrange(int(f_j)):
                UL.append(j.unload_time)
                LD.append(j.load_time)

        # Compute IN
        for l in xrange(k):
            IN.append(task.input_load_time)

        # Compute LD
        for l in xrange(int((final_i + 1))):
            LD.append(task.load_time)

        found = 0
        task_ignore_j_value = 0
        for t in ignore_j:
            if t[1] > 0.0:
                found = 1
                task_ignore_j = t[0]
                task_ignore_j_value = t[1]

        longest_input = self.longest_input_in_tasks(lp)
        longest_load = self.longest_load_in_tasks(lp)
        longest_output = self.longest_output_in_tasks(lp)
        longest_unload = self.longest_unload_in_tasks(lp)

        if found == 0:          
            for l in xrange(k - 1):
                IN.append(longest_input)
                LD.append(longest_load)
            
            for l in xrange(k - 2):
                OUT.append(longest_output)
                UL.append(longest_unload)
        else:
            if task_ignore_j.input_load_time <= longest_input:
                for l in xrange(k - 1):
                    IN.append(longest_input)
            else:
                for l in xrange(int(min(k - 1, task_ignore_j_value))):
                    IN.append(task_ignore_j.input_load_time)

                for l in xrange(int(max(0, k - 1 - task_ignore_j_value))):
                    IN.append(longest_input)

            if task_ignore_j.output_unload_time <= longest_output:
                for l in xrange(k - 1):
                    OUT.append(longest_output)
            else:
                for l in xrange(int(min(k - 1, task_ignore_j_value))):
                    OUT.append(task_ignore_j.output_unload_time)

                for l in xrange(int(max(0, k - 1 - task_ignore_j_value))):
                    OUT.append(longest_output)

            if task_ignore_j.load_time <= longest_load:
                for l in xrange(k - 2):
                    LD.append(longest_load)
            else:
                for l in xrange(int(min(k - 2, task_ignore_j_value))):
                    LD.append(task_ignore_j.load_time)

                for l in xrange(int(max(0, k - 2 - task_ignore_j_value))):
                    LD.append(longest_load)

            if task_ignore_j.unload_time <= longest_unload:
                for l in xrange(k - 2):
                    UL.append(longest_unload)
            else:
                for l in xrange(int(min(k - 2, task_ignore_j_value))):
                    UL.append(task_ignore_j.unload_time)

                for l in xrange(int(max(0, k - 2 - task_ignore_j_value))):
                    UL.append(longest_unload)

        for l in xrange(len(OUT)):
            OUT[l] = OUT[l] * (tdma_round_length / tdma_slot_size_wo_overhead)

        for l in xrange(len(IN)):
            IN[l] = IN[l] * (tdma_round_length / tdma_slot_size_wo_overhead)

        for l in xrange(len(LD)):
            LD[l] = LD[l] * (tdma_round_length / tdma_slot_size_wo_overhead)

        for l in xrange(len(UL)):
            UL[l] = UL[l] * (tdma_round_length / tdma_slot_size_wo_overhead)        


        # Sort OUT, UL, IN, LD in decreasing order - Line 7
        OUT.sort(reverse=True)
        UL.sort(reverse=True)
        IN.sort(reverse=True)
        LD.sort(reverse=True)

        return (OUT, UL, IN, LD)

    def compute_hfinal(self, task, final_i, final_j, tdma_slot_size, tdma_round_length, OUT, IN, LD, UL):
        hp = self.highest_prio_tasks(task.id)

        final = []
        for j in hp:
            f_j = self.find_final(j, final_j)
            for l in xrange(int(f_j)):
                final.append(j.cost - tdma_slot_size - tdma_round_length)

        for l in xrange(int(final_i)):
            final.append(task.cost - tdma_slot_size - tdma_round_length)

        NFINAL = len(final)
        DMA = []
        for l in xrange(int(NFINAL)):
            DMA.append(OUT[l] + UL[l] + LD[l] + IN[l])

        M = DMA + final
        M.sort(reverse=True)

        sum = 0.0
        for l in xrange(int(NFINAL)):
            sum = sum + M[l]

        '''
        for j in hp:
            f_j = self.find_final(j, final_j)
            sum = sum + (tdma_slot_size + tdma_round_length) * f_j

        sum = sum + (tdma_slot_size + tdma_round_length) * final_i
        '''
        sum = sum + (tdma_slot_size + tdma_round_length) * NFINAL
        return sum

    def compute_hfinal_no_streaming(self, task, final_i, final_j, ignore_j, k, tdma_slot_size, tdma_round_length, OUT, IN, LD, UL):
        hp = self.highest_prio_tasks(task.id)
        lp = self.lowest_prio_tasks(task.id)

        if lp == []:
            lp.append(self.create_dummy_task())

        final = []

        found = 0
        task_ignore_j_value = 0
        for t in ignore_j:
            if t[1] > 0.0:
                found = 1
                task_ignore_j = t[0]
                task_ignore_j_value = t[1]

        for j in hp:
            f_j = self.find_final(j, final_j)
            for l in xrange(int(f_j)):
                final.append(j.cost - tdma_slot_size - tdma_round_length)

        for l in xrange(int(final_i)):
            final.append(task.cost - tdma_slot_size - tdma_round_length)
        
        longest_cost = self.longest_cost_in_tasks(lp)
        if found == 0:
            for l in xrange(k-1):
                final.append(longest_cost - tdma_slot_size - tdma_round_length)
        else:
            if task_ignore_j.cost > longest_cost:
                for l in xrange(int(min(task_ignore_j_value, k-1))):
                    final.append(task_ignore_j.cost - tdma_slot_size - tdma_round_length)

                for l in xrange(int(max(0, k-1-task_ignore_j_value))):
                    final.append(longest_cost - tdma_slot_size - tdma_round_length)
            else:
                for l in xrange(k-1):
                    final.append(longest_cost - tdma_slot_size - tdma_round_length)
        
        NFINAL = len(final)
        DMA = []
        for l in xrange(int(NFINAL)):
            DMA.append(OUT[l] + UL[l] + LD[l] + IN[l])

        M = DMA + final
        M.sort(reverse=True)

        sum = 0.0
        for l in xrange(int(NFINAL)):
            sum = sum + M[l]

        sum = sum + (tdma_slot_size + tdma_round_length) * NFINAL

        return sum

    def compute_hcons(self, task, cons_i, cons_j, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length, OUT):
        hp = self.highest_prio_tasks(task.id)

        cons = []
        for j in hp:
            c_j = self.find_cons(j, cons_j)
            for l in xrange(int(c_j)):
                cons.append(j.cost - tdma_slot_size - tdma_round_length - j.input_load_time * tdma_round_length / tdma_slot_size_wo_overhead)

        for l in xrange(int(cons_i)):
            cons.append(task.cost - tdma_slot_size - tdma_round_length - task.input_load_time * tdma_round_length / tdma_slot_size_wo_overhead)

        NCONS = len(cons)

        M = cons + OUT
        M.sort(reverse=True)

        sum = 0.0
        for l in xrange(int(NCONS)):
            sum = sum + M[l]

        for j in hp:
            c_j = self.find_cons(j, cons_j)
            sum = sum + (tdma_slot_size + tdma_round_length + j.input_load_time * tdma_round_length / tdma_slot_size_wo_overhead) * c_j

        sum = sum + (tdma_slot_size + tdma_round_length + task.input_load_time * tdma_round_length / tdma_slot_size_wo_overhead) * cons_i

        return sum

    def compute_tal_k(self, task, k_plus_one):
        Q = int(floor(task.arrival_curve_jitter / float(task.period)))

        if k_plus_one <= Q + 1:
            return 0
        else:
            return (k_plus_one - 1) * task.period - task.arrival_curve_jitter

    # tdma_slot_size is the size of the TDMA in the core under analysis
    # tdma_slot_size_wo_overhead is the size of the TDMA in the core under analysis without overhead
    # tdma_round_lenght is the lenght of the complete TDMA round
    def SPM_Stream_Schedulability_test(self, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):

        for task in self: #task is the task under analysis
            #print "**************************************"
            #print "No Contention and Streaming Schedulability test for task", task.id

            # Compute Rmin_i, B_i and F_i - Line 1
            Rmin_i = self.compute_rmin(task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)
            B_i = self.compute_b(task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)
            F_i = self.compute_f(task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)

            # Sek k = 1, Rmax_i = 0, and t_i,1 = 0 - Line 2
            k = 1
            Rmax_i = 0
            t_i_k = 0

            end_loop = False

            while end_loop == False:

                #print "-----Starting iteration k =", k
                # Set R_i,k = B_i - Line 3
                R_i_k = B_i

                end_inner_loop = False

                while end_inner_loop == False:

                    # Compute FINAL_i and CONS_i for task under analysis - Line 4
                    FINAL_i = self.compute_final_i(task, k, B_i, R_i_k)
                    CONS_i = self.compute_cons_i(task, k, FINAL_i)

                    # Compute FINAL_j,CONS_j for  each  task T_j with j < i (higher priority) - Line 5
                    hp = self.highest_prio_tasks(task.id)
                    FINAL_j = []
                    CONS_j = []

                    for j in hp:
                        final_j = self.compute_final_j(task, j, k, R_i_k)
                        FINAL_j.append([j, final_j])
                        cons_j = self.compute_cons_j(j, R_i_k, final_j)
                        CONS_j.append([j, cons_j])

                    # Compute set of DMA operations OUT, UL, IN, LD (sort by decreasing order) - Lines 6 and 7
                    (OUT, UL, IN, LD) = self.compute_DMA_operations(task, CONS_i, FINAL_i, k, CONS_j, FINAL_j, tdma_slot_size_wo_overhead, tdma_round_length)

                    # Compute Hcons_i_k and Hfinal_i_k - Line 8
                    # task, cons_i, cons_j, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length, OUT):
                    Hcons_i_k = self.compute_hcons(task, CONS_i, CONS_j, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length, OUT)
                    
                    #task, final_i, final_j, tdma_slot_size, tdma_round_length, OUT, IN, LD, UL):
                    Hfinal_i_k = self.compute_hfinal(task, FINAL_i, FINAL_j, tdma_slot_size, tdma_round_length, OUT, IN, LD, UL)

                    # Test of Line 9
                    #print "Task",task.id,"B_i =",B_i,"Hcons_i_k =", Hcons_i_k, "Hfinal_i_k =", Hfinal_i_k, "F_i =",F_i, "t_i_k =",t_i_k, "Sum total =", (B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k)
                    if B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k > task.deadline:
                        #print "Task", task.id,"has deadline =", task.deadline,"and (B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k) =", (B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k)
                        return False # Return NON SCHEDULABLE

                    # Test of Line 10
                    if B_i + Hcons_i_k + Hfinal_i_k > R_i_k:
                        R_i_k =  B_i + Hcons_i_k + Hfinal_i_k
                    else:
                        end_inner_loop = True
                    
                    del FINAL_j
                    del CONS_j
                    del OUT
                    del UL
                    del IN
                    del LD

                # Set Rmax_i_k - Line 11
                Rmax_i_k = R_i_k + F_i - t_i_k

                # Set Rmax_i - Line 12
                Rmax_i = max(Rmax_i, Rmax_i_k)

                # Compute t_i_k+1 - Line 13
                t_i_k = self.compute_tal_k(task, k + 1)

                # Test of Line 14
                if R_i_k > t_i_k:
                    k = k + 1
                else:
                    end_loop = True

        # Return SCHEDULABLE - Line 15
        return True

    # factor M - under contention, each memory request takes M times to complete
    def compute_exec_final_and_exec_cons_for_all(self, M):
        for task in self:
            task.exec_final_i = task.cost + (task.load_time + task.unload_time + task.input_load_time + task.output_unload_time) * M
            task.exec_cons_i = task.cost + (task.input_load_time + task.output_unload_time) * M

    def longest_exec_final(self, tasks):
        max = -1
        for t in tasks:
            if t.exec_final_i > max:
                max = t.exec_final_i

        return max

    # tdma_slot_size is the size of the TDMA in the core under analysis
    # tdma_slot_size_wo_overhead is the size of the TDMA in the core under analysis without overhead
    # tdma_round_lenght is the lenght of the complete TDMA round
    # factor M - under contention, each memory request takes M times to complete
    def Contention_schedulability_test(self, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length, M):
        
        self.compute_exec_final_and_exec_cons_for_all(M)

        for task in self:

            #print "**************************************"
            #print "Contention and Streaming test for task", task.id

            lp = self.lowest_prio_tasks(task.id)

            F_i = task.exec_final_i

            if lp == []:
                B_i = 0
            else:
                B_i = self.longest_exec_final(lp)

            #print "Task ", task.id, "B =", B_i, "F =", F_i

            # Sek k = 1, Rmax_i = 0, and t_i,1 = 0 - Line 2
            k = 1
            Rmax_i = 0
            t_i_k = 0

            end_loop = False

            while end_loop == False:

                #print "-----Starting iteration k =", k
                # Set R_i,k = B_i - Line 3
                R_i_k = B_i

                end_inner_loop = False

                while end_inner_loop == False:

                    # Compute FINAL_i and CONS_i for task under analysis - Line 4
                    FINAL_i = self.compute_final_i(task, k, B_i, R_i_k)
                    CONS_i = self.compute_cons_i(task, k, FINAL_i)

                    # Compute FINAL_j,CONS_j for  each  task T_j with j < i (higher priority) - Line 5
                    hp = self.highest_prio_tasks(task.id)
                    FINAL_j = []
                    CONS_j = []

                    for j in hp:
                        final_j = self.compute_final_j(task, j, k, R_i_k)
                        FINAL_j.append([j, final_j])
                        cons_j = self.compute_cons_j(j, R_i_k, final_j)
                        CONS_j.append([j, cons_j])

                    # Compute Hcons_i_k and Hfinal_i_k - Line 8
                    # sum([t.exec_final_i for t in lp])
                    Hfinal_i_k = FINAL_i * task.exec_final_i + sum([j[0].exec_final_i * j[1]  for j in FINAL_j])

                    Hcons_i_k = CONS_i * task.exec_cons_i + sum([j[0].exec_cons_i * j[1]  for j in CONS_j])                

                    # Test of Line 9
                    #print "Task",task.id,"B_i =",B_i,"Hcons_i_k =", Hcons_i_k, "Hfinal_i_k =", Hfinal_i_k, "F_i =",F_i, "t_i_k =",t_i_k, "Sum total =", (B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k)
                    if B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k > task.deadline:
                        #print "Task", task.id,"has deadline =", task.deadline,"and (B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k) =", (B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k)
                        return False # Return NON SCHEDULABLE

                    # Test of Line 10
                    if B_i + Hcons_i_k + Hfinal_i_k > R_i_k:
                        R_i_k =  B_i + Hcons_i_k + Hfinal_i_k
                    else:
                        end_inner_loop = True
                    
                    del FINAL_j
                    del CONS_j

                # Set Rmax_i_k - Line 11
                Rmax_i_k = R_i_k + F_i - t_i_k

                # Set Rmax_i - Line 12
                Rmax_i = max(Rmax_i, Rmax_i_k)

                # Compute t_i_k+1 - Line 13
                t_i_k = self.compute_tal_k(task, k + 1)

                # Test of Line 14
                if R_i_k > t_i_k:
                    k = k + 1
                else:
                    end_loop = True

        return True                #print "-----Starting iteration k =", k


    def No_streaming_schedulability_test(self, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):
        for task in self: #task is the task under analysis
            #print "\n\n**************************************"
            #print "No Contention and No Streaming Schedulability test for task", task.id

            # Compute Rmin_i, B_i and F_i - Line 1
            Rmin_i = self.compute_rmin(task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)
            B_i = self.compute_b(task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)
            F_i = self.compute_f(task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)

            # Sek k = 1, Rmax_i = 0, and t_i,1 = 0 - Line 2
            k = 1
            Rmax_i = 0
            t_i_k = 0

            end_loop = False

            while end_loop == False:

                # Set R_i,k = B_i - Line 3
                R_i_k = B_i

                end_inner_loop = False

                while end_inner_loop == False:

                    # Compute FINAL_i for task under analysis - Line 4
                    FINAL_i = k - 1

                    # Compute FINAL_j,CONS_j for  each  task T_j with j < i (higher priority) - Line 5
                    hp = self.highest_prio_tasks(task.id)
                    FINAL_j = []
                    IGNORE_j = []

                    for j in hp:
                        final_j = self.compute_final_j_no_streaming(task, j, k, R_i_k)
                        FINAL_j.append([j, final_j])
                        ignore_j = self.compute_cons_j(j, R_i_k, final_j)
                        IGNORE_j.append([j, ignore_j])

                    # Compute set of DMA operations OUT, UL, IN, LD (sort by decreasing order) - Lines 6 and 7
                    #task, final_i, k, ignore_j, final_j, tdma_slot_size_wo_overhead, tdma_round_length):
                    (OUT, UL, IN, LD) = self.compute_DMA_operations_no_streaming(task, FINAL_i, k, IGNORE_j, FINAL_j, tdma_slot_size_wo_overhead, tdma_round_length)
                    
                    #task, final_i, final_j, ignore_j, k, tdma_slot_size, tdma_round_length, OUT, IN, LD, UL):
                    Hfinal_i_k = self.compute_hfinal_no_streaming(task, FINAL_i, FINAL_j, IGNORE_j, k, tdma_slot_size, tdma_round_length, OUT, IN, LD, UL)

                    # Test of Line 9
                    #print "Task",task.id,"B_i =",B_i, "Hfinal_i_k =", Hfinal_i_k, "F_i =",F_i, "t_i_k =",t_i_k, "Sum total =", (B_i + Hfinal_i_k + F_i - t_i_k)
                    if B_i + Hfinal_i_k + F_i - t_i_k > task.deadline:
                        #print "Task", task.id,"has deadline =", task.deadline,"and (B_i + Hcons_i_k + Hfinal_i_k + F_i - t_i_k) =", (B_i + Hfinal_i_k + F_i - t_i_k)
                        return False # Return NON SCHEDULABLE

                    # Test of Line 10
                    if B_i + Hfinal_i_k > R_i_k:
                        R_i_k =  B_i + Hfinal_i_k
                    else:
                        end_inner_loop = True
                    
                    del FINAL_j
                    del IGNORE_j
                    del OUT
                    del UL
                    del IN
                    del LD

                # Set Rmax_i_k - Line 11
                Rmax_i_k = R_i_k + F_i - t_i_k

                # Set Rmax_i - Line 12
                Rmax_i = max(Rmax_i, Rmax_i_k)

                # Compute t_i_k+1 - Line 13
                t_i_k = self.compute_tal_k(task, k + 1)

                # Test of Line 14
                if R_i_k > t_i_k:
                    k = k + 1
                else:
                    end_loop = True

        # Return SCHEDULABLE - Line 15
        return True