from schedcat.model.serialize import load
from schedcat.locking.native import PCP_Analysis
from schedcat.locking import bounds

x = PCP_Analysis()

file = "light_homogeneous_0.5_short_4_0.05_1"

taskset = load(file)

print taskset
print "TaskSet Utilization = %.4f" % (taskset.utilization())

taskset.assign_ids()

i = 0

for task in taskset:
	task.partition = 0
	task.preemption_level = 0
	i = i + 1

print "Number of tasks = %d" % i # taskset.??? TODO how to get the right number of tasks from the TaskSystem
for task in taskset:
	print "Task[%d] period %d wcet %d utilization %.4f" % (task.id, task.period, task.cost, task.utilization())
	for res_id in task.resmodel:
            print "Resource %d write length = %d" % (res_id, task.resmodel[res_id].max_write_length)

x.test(bounds.get_cpp_model(taskset, True))

