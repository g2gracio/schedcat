import os
import random
import schedcat.generator.tasks as gen
import schedcat.mapping.binpack as bp
from schedcat.mapping.rollback import Bin, WorstFit
from schedcat.model.tasks import SporadicTask, TaskSystem
import schedcat.sched.fp.rta as rta
import schedcat.model.tasks as tasks

from schedcat.model.serialize import write
from schedcat.generator.tasksets import mkgen #, \
                                    #    NAMED_UTILIZATIONS, \
                                    #    NAMED_PERIODS
from schedcat.util.time import ms2us

from two_phase import SegmentedTask, SegmentedTaskSystem

def partition_tasks(cluster_size, clusters, taskset):
    first_cap = cluster_size
    first_bin = Bin(size=SporadicTask.utilization, capacity=first_cap)
    other_bins = [Bin(size=SporadicTask.utilization,capacity=cluster_size)
    for _ in xrange(1, clusters)]
    heuristic = WorstFit(initial_bins=[first_bin] + other_bins)
    heuristic.binpack(taskset)
    if not (heuristic.misfits):
        clusts = [TaskSystem(b.items) for b in heuristic.bins]
        for i, c in enumerate(clusts):
            c.cpus = cluster_size
            for task in c:
                task.partition = i
        return [c for c in clusts if len(c) > 0]
    else:
        return False

NAMED_PERIODS = {
# Named period distributions used in several UNC papers, in milliseconds.
    'uni-short'     : gen.uniform_int( 3,  33),
    'uni-moderate'  : gen.uniform_int(10, 100),
    'uni-long'      : gen.uniform_int(50, 250),

    'log-uni-short'     : gen.log_uniform_int( 3,  33),
    'log-uni-moderate'  : gen.log_uniform_int(10, 100),
    'log-uni-long'      : gen.log_uniform_int(50, 250),
}

NAMED_UTILIZATIONS = {
# Named utilization distributions used in several UNC papers, in milliseconds.
    'uni-light'     : gen.uniform(0.01, 0.2), #(0.001, 0.1)
    'uni-light-2'     : gen.uniform(0.01, 0.1),
    'uni-medium'    : gen.uniform(0.1  , 0.4), 
    'uni-heavy'     : gen.uniform(0.5  , 0.9),

    'exp-light'     : gen.exponential(0, 1, 0.10),'bimo-light'    : gen.multimodal([(gen.uniform(0.001, 0.5), 8),
                                      (gen.uniform(0.5  , 0.9), 1)]),
    'bimo-medium'   : gen.multimodal([(gen.uniform(0.001, 0.5), 6),
                                      (gen.uniform(0.5  , 0.9), 3)]),
    'bimo-heavy'    : gen.multimodal([(gen.uniform(0.001, 0.5), 4),
                                      (gen.uniform(0.5  , 0.9), 5)]),
    'exp-medium'    : gen.exponential(0, 1, 0.25),
    'exp-heavy'     : gen.exponential(0, 1, 0.50),
}

def generate_taskset_files(util_name, period_name, cap, number):
    generator = mkgen(NAMED_UTILIZATIONS[util_name],
                      NAMED_PERIODS[period_name])
    generated_sets = []
    for i in range(number):
        taskset = generator(max_util=cap, time_conversion=ms2us)
        filename = "{0}_{1}_{2}_{3}".format(util_name,
                                            period_name, cap, i)
        #write(taskset, filename)
        #generated_sets.append(filename)
        generated_sets.append(taskset)
    return generated_sets

def utilization(task):
	#print "UTILIZATION() period = %d, wcet = %d, util = %.4f" % (task.period, task.cost, task.utilization())
	return task.utilization()

def period(task):
    return task.period

bin_packing_error = False

def misfit(task):
    global bin_packing_error
    bin_packing_error = True
    print "Not able to partition the task set - Task utilization %.4f" % (task.utilization())

def task_set_partitioning(ts):
    #for x in ts[0]:
    #    print("Printing task")
    #    print(x)

    bin_packing_error = False

    print "Task set utilization = %.2f" % ts[0].utilization()
    sets = bp.best_fit_decreasing(ts[0], 4, 1, utilization, misfit)

    if bin_packing_error == False:
        for i in sets:
            print(i)
    else:
        print "BDF was not able to partition the TS"

def apply_bfd(ts):
    bin_packing_error = False

    print "****Applying BFD - Task set utilization = %.2f" % ts.utilization()
    sets = bp.best_fit_decreasing(ts, 4, 1, utilization, misfit)

    if bin_packing_error == False:
        for i in sets:
            print(i)
    else:
        print "BDF was not able to partition the TS"

    print "****done\n\n"

def apply_wfd(ts):
    bin_packing_error = False

    print "****Applying WFD - Task set utilization = %.2f" % ts.utilization()
    sets = bp.worst_fit_decreasing(ts, 4, 1, utilization, misfit)

    if bin_packing_error == False:
        for i in sets:
            print(i)
    else:
        print "WFD was not able to partition the TS"

    print "****done\n\n"

def apply_ffd(ts):
    bin_packing_error = False

    print "****Applying FFD - Task set utilization = %.2f" % ts.utilization()
    sets = bp.first_fit_decreasing(ts, 4, 1, utilization, misfit)

    if bin_packing_error == False:
        for i in sets:
            print(i)
    else:
        print "FFD was not able to partition the TS"

    print "****done\n\n"

def bin_packing_test():
    tasks = []
    tasks.append(SporadicTask(9060, 58000))
    tasks.append(SporadicTask(782, 46000))
    tasks.append(SporadicTask(67791, 99000))
    tasks.append(SporadicTask(4448, 24000))
    tasks.append(SporadicTask(23372, 61000))
    tasks.append(SporadicTask(23729, 93000))
    tasks.append(SporadicTask(21545, 25000))
    tasks.append(SporadicTask(23729, 70000))
    
    '''
    SporadicTask(9060, 58000, util=0.15)
    SporadicTask(782, 46000, util=0.01)
    SporadicTask(67791, 99000, util=0.68)
    SporadicTask(4448, 24000, util=0.18)
    SporadicTask(23372, 61000, util=0.38)
    SporadicTask(23729, 93000, util=0.25)
    SporadicTask(21545, 25000, util=0.86)
    SporadicTask(23729, 70000, util=0.33)
    '''

    ts = TaskSystem(tasks)
    
    #ts = generate_taskset_files('bimo-medium', 'uni-moderate', 3, 1)
    #ts = ts[0]
    
    print(ts)

    apply_bfd(ts)
    apply_wfd(ts)
    apply_ffd(ts)

def apply_RM_fit(ts):
    bin_packing_error = False

    print "****Applying RM fit - Task set utilization = %.2f" % ts.utilization()
    sets = bp.first_fit(ts, 4, 0.69, utilization, misfit)

    if bin_packing_error == False:
        for i in sets:
            print(i)
    else:
        print "FFD was not able to partition the TS"

    print "****done\n\n"

'''
def first_fit(items, bins, capacity=1.0, weight=id, misfit=ignore,
              empty_bin=list):
    sets = [empty_bin() for _ in xrange(0, bins)]
    sums = [0.0 for _ in xrange(0, bins)]
    for x in items:
        c = weight(x)
        for i in xrange(0, bins):
            if sums[i] + c <= capacity:
                sets[i] += [x]
                sums[i] += c
                break
        else:
            misfit(x)

    return sets
'''

def ignore(_):
    pass

def apply_RTA_first_fit(ts, bins, capacity=1.0, weight=id, misfit=ignore, empty_bin=list):
    sets = [empty_bin() for _ in xrange(0, bins)]
    sums = [0.0 for _ in xrange(0, bins)]
    bin_packing_error = False
    fitted = False

    partition = [[], [], [], []] # TODO: create this array of lists dinamically

    for x in ts: # x is the current task to be assigned to a bin
        fitted = False
        for i in xrange(0, bins): #iterates over all bins
            partition[i].append(x) #add the current task to a temporary array of tasks, one for each bin "i"
            tmp_ts = SegmentedTaskSystem(partition[i]) #create a temporary SegmentedTaskSystem using the tasks assigned to a bin "i"
            if rta.is_schedulable(1, tmp_ts) == True:
                print "RTA is schedulable bin = " + str(i) + " task = " + str(x.deadline)
                sets[i] += [x] #RTA is true, then assign "x" to the current set (bin)
                fitted = True
                break
            else:
                partition[i].remove(x) #removes the current task from the bin "i" if RTA fails
            tmp_ts = [] #resets the temporary SegmentedTaskSystem
        if fitted == False: #if the partition fails, sets fitted to False
            misfit(x)

    if bin_packing_error == False: #if there was no error during the partition
        for i in sets:
            print(i)
    else:
        print "FFD RTA was not able to partition the TS"

    print "****done\n\n"

    return sets
    

def RM_bin_packing_test():
    tasks = []
    tasks.append(SegmentedTask(5000, 18000, 98000))
    tasks.append(SegmentedTask(2000, 15000, 39000))
    tasks.append(SegmentedTask(2000, 19000, 82000))
    tasks.append(SegmentedTask(500, 5000, 18000))
    tasks.append(SegmentedTask(2000, 11000, 29000))
    tasks.append(SegmentedTask(1000, 10000, 68000))
    tasks.append(SegmentedTask(1000, 17000, 72000))
    tasks.append(SegmentedTask(2000, 22000, 65000))

    #order by increasing order of periods
    sorted_tasks = sorted(tasks, key=lambda x: x.period, reverse=False)
    #printed sorted tasks
    #print(sorted_tasks)

    ts = SegmentedTaskSystem(sorted_tasks)
    #print(ts)

    #apply first fit using capacity of 0.69 and tasks utilizations
    #apply_RM_fit(ts)
    apply_RTA_first_fit(ts, 4, 0.69, utilization, misfit)

def RTA_test():
    print "test"
    ts = tasks.TaskSystem([
        tasks.SporadicTask(1,  4),
        tasks.SporadicTask(1,  5),
        tasks.SporadicTask(3,  9),
        tasks.SporadicTask(3, 18),
    ])

    print "RTA is schedulable = " + str(rta.is_schedulable(1, ts)) + "\n"

    print "Response Time T0 " + str(ts[0].response_time) + " expected 1\n"
    print "Response Time T1 " + str(ts[1].response_time) + " expected 2\n"
    print "Response Time T2 " + str(ts[2].response_time) + " expected 7\n"
    print "Response Time T3 " + str(ts[3].response_time) + " expected 18\n"

def main():
    #ts = generate_taskset_files('uni-medium', 'uni-moderate', 4, 1)
    #print(ts)
    #task_set_partitioning(ts)
    #bin_packing_test()
    RM_bin_packing_test()
    #RTA_test()

if __name__ == "__main__":
    main()