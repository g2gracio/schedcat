from capgs_utils import *

def compare_run_time_results_with_original_values(capgs_ts_file, results_file):
	original_ts = read_capgs_ts_from_file(capgs_ts_file)
	file = open(results_file, "r")
	for line in file:
		numbers = map(int, line.split())
		if numbers[1] > original_ts.task_from_id(numbers[0]).cost:
			print "Task[%d] perdeu deadline, WCET=%d, medido=%d" % (numbers[0], original_ts.task_from_id(numbers[0]).cost, numbers[1])
		else:
			print "Task[%d] nao perdeu deadline" % (numbers[0])

compare_run_time_results_with_original_values("capgs_test_0", "results_capg_ts_0.txt")
