from __future__ import division
from math import ceil
from schedcat.util.quantor import forall


def fp_demand(taskset, time):
    # Account for higher-priority interference from the same processor
    if taskset is None:
        return 0
    else:
        return sum([t.cost * int(ceil(time/t.period)) for t in taskset])


def fp_demand_mem(taskset, time):
    # Account for higher-priority interference from the other processors 
    mem_demand = 0
    if taskset is not None:
        for t in taskset:
            # tbd: it should be period when tdma
            rt = t.response_time if hasattr(t, 'response_time') else t.period
            mem_demand = mem_demand + t.mem_cost * int(ceil((time+rt-t.cost)/t.period))
    return mem_demand


def num_jobs(taskset, time, B):
    # count the task job releases within interval time
    n = sum([int(ceil(time/t.period)) for t in taskset])
    if B > 0:
        return n+1
    else:
        return n


def max_interference(taskset, taskset_ext):
    if taskset is None:
        return 0
    max_mem_phase = taskset.max_mem_cost()
    time   = max_mem_phase
    demand = max_mem_phase + fp_demand_mem(taskset_ext, time)
    while demand > time:
        time   = demand
        demand = max_mem_phase + fp_demand_mem(taskset_ext, time)

    return demand - max_mem_phase


def rta_schedulable_synchr(taskset, task, taskset_ext=None):
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task) ]
    lower_prio =        [ ]
    

    # check memory utilization
    if taskset_ext is None:
        u_ext = 0
    else:
        u_ext = sum([t.mem_utilization() for t in taskset_ext])
    u_mem = sum([t.mem_utilization() for t in higher_equal_prio])
    if u_ext + u_mem >= 1:
        # utilization exceeded
        task.response_time = False
        return False

    # compute the worst external interference on memory
    M = max_interference(taskset,taskset_ext)

    # check utilization (memory and computation requirements)
    u_int = sum([t.utilization() for t in higher_equal_prio])


    u = u_int + min(u_ext,sum([M/t.period for t in higher_equal_prio]))
    if u >= 1: 
        # utilization exceeded
        task.response_time = False
        return False


    # compute low priority blocking
    B = max([t.cost for t in lower_prio] or [0])

    # compute busy period length
    demand = B + task.cost; time = 0
    while demand > time:
        time = demand
        int_demand = B + fp_demand(higher_equal_prio, time)
        ext_demand = min(M*num_jobs(higher_equal_prio, time, 1), fp_demand_mem(taskset_ext, time))
        demand     = int_demand + ext_demand
    busy_period = demand


    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        # compute the start of the memory phase
        mem_start  = max(B+sum([t.cost for t in higher_prio]),(k-1)*task.period)
        int_demand = fp_demand(higher_prio,mem_start) + B + (k-1)*task.cost
        ext_demand = min(M*(num_jobs(higher_equal_prio,mem_start,1)-1),fp_demand_mem(taskset_ext,mem_start))
        print  "ext_demand " + str(ext_demand)
        demand     = int_demand + ext_demand
 
        while demand > mem_start:
            mem_start  = demand
            int_demand = B + fp_demand(higher_prio, mem_start) + (k-1)*task.cost
            ext_demand = min(M*(num_jobs(higher_equal_prio,mem_start,1)-1),fp_demand_mem(taskset_ext, mem_start))
            print  "ext_demand " + str(ext_demand)
            demand     = int_demand + ext_demand
 
        # compute the end of the memory phase (memory phase cannot be preempted by the tasks running on the same processor)
        mem_end    = mem_start + task.mem_cost
        ext_demand = min(M*(num_jobs(higher_equal_prio,mem_start,1)-1)+fp_demand_mem(taskset_ext,mem_end-mem_start),\
                         fp_demand_mem(taskset_ext,mem_end))
        demand     = int_demand + ext_demand

        while demand > mem_end:
            mem_end    = demand
            ext_demand = min(M*(num_jobs(higher_equal_prio,mem_start,1)-1)+fp_demand_mem(taskset_ext,mem_end-mem_start),\
                             fp_demand_mem(taskset_ext,mem_end))
            demand     = int_demand + ext_demand

        response_times.append(mem_end + task.exec_cost - (k-1)*task.period)
    

    task.response_time = max(response_times)
    if task.response_time > task.deadline:
        return False
    else:
        return True



def rta_schedulable(taskset, task, taskset_ext=None):
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task) ]
    

    # check memory utilization
    if taskset_ext is None:
        u_ext = 0
    else:
        u_ext = sum([t.mem_utilization() for t in taskset_ext])
    u_mem = sum([t.mem_utilization() for t in higher_equal_prio])
    if u_ext + u_mem >= 1:
        # utilization exceeded
        task.response_time = False
        return False

    # compute the worst external interference on memory
    M = max_interference(taskset,taskset_ext)

    # check utilization (memory and computation requirements)
    u_int = sum([t.utilization() for t in higher_equal_prio])


    u = u_int + min(u_ext,sum([M/t.period for t in higher_equal_prio]))
    if u >= 1: 
        # utilization exceeded
        task.response_time = False
        return False


    # compute low priority blocking
    B = max([t.cost for t in lower_prio] or [0])

    # compute busy period length
    demand = B + task.cost; time = 0
    while demand > time:
        time = demand
        int_demand = B + fp_demand(higher_equal_prio, time)
        ext_demand = min(M*num_jobs(higher_equal_prio, time, B), fp_demand_mem(taskset_ext, time))
        demand     = int_demand + ext_demand
    busy_period = demand


    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        # compute the start of the memory phase
        mem_start  = max(B+sum([t.cost for t in higher_prio]),(k-1)*task.period)
        int_demand = fp_demand(higher_prio,mem_start) + B + (k-1)*task.cost
        ext_demand = min(M*(num_jobs(higher_equal_prio,mem_start,B)-1),fp_demand_mem(taskset_ext,mem_start))
        demand     = int_demand + ext_demand
 
        while demand > mem_start:
            mem_start  = demand
            int_demand = B + fp_demand(higher_prio, mem_start) + (k-1)*task.cost
            ext_demand = min(M*(num_jobs(higher_equal_prio,mem_start,B)-1),fp_demand_mem(taskset_ext, mem_start))
            demand     = int_demand + ext_demand
 
        # compute the end of the memory phase (memory phase cannot be preempted by the tasks running on the same processor)
        mem_end    = mem_start + task.mem_cost
        ext_demand = min(M*(num_jobs(higher_equal_prio,mem_start,B)-1)+fp_demand_mem(taskset_ext,mem_end-mem_start),\
                         fp_demand_mem(taskset_ext,mem_end))
        demand     = int_demand + ext_demand

        while demand > mem_end:
            mem_end    = demand
            ext_demand = min(M*(num_jobs(higher_equal_prio,mem_start,B)-1)+fp_demand_mem(taskset_ext,mem_end-mem_start),\
                             fp_demand_mem(taskset_ext,mem_end))
            demand     = int_demand + ext_demand

        response_times.append(mem_end + task.exec_cost - (k-1)*task.period)
    

    task.response_time = max(response_times)
    if task.response_time > task.deadline:
        return False
    else:
        return True


def rta_tdma_schedulable(taskset, task, N=1):
    # basic TDMA, slot length=1, round length=N, no reclaiming
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task) ]


    # compute low priority blocking
    B = max([t.mem_cost*N + t.exec_cost for t in lower_prio] + [N-1])

    # check utilization
    u = sum([(t.exec_cost + t.mem_cost*N) / t.period for t in higher_equal_prio])
    if u >= 1: 
        # utilization exceeded 
        task.response_time = False
        return False

    # compute busy period length
    demand = B + task.mem_cost*N + t.exec_cost; time = 0
    while demand > time:
        time       = demand
        cmp_demand = B + fp_demand(higher_equal_prio, time)
        mem_delay  = fp_demand_mem(higher_equal_prio, time) * (N-1)
        demand     = cmp_demand + mem_delay
    busy_period = demand

    
    # find all response times over busy period
    response_times = []
    for k in range(1,int(ceil(busy_period/task.period))+1):
        task_start = max(sum([t.cost for t in higher_prio]),(k-1)*task.period)
        cmp_demand = B + fp_demand(higher_prio, task_start) + (k-1)*task.cost
        mem_delay  = fp_demand_mem(higher_prio, task_start) * (N-1) + (k-1) * task.mem_cost * (N-1)
        demand     = cmp_demand + mem_delay

        while demand > task_start:
            task_start = demand
            cmp_demand = B + fp_demand(higher_prio, task_start)  + (k-1)*task.cost
            mem_delay  = fp_demand_mem(higher_prio, task_start) * (N-1) + (k-1) * task.mem_cost * (N-1)
            demand     = cmp_demand + mem_delay
 
        response_times.append(task_start + task.mem_cost * N + task.exec_cost - (k-1)*task.period)


    task.response_time = max(response_times)


    if task.response_time > task.deadline:
        return False
    else:
        return True


def rta_rr_schedulable(taskset, task, tasksets_ext=None):
    # basic Round Robin, quantum size=1, taskset_ext should be an array demand per core
    # contention-based scheduler
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task) ]


    # core number
    N = len(tasksets_ext) + 1

    # compute low priority memory and computation blocking
    B_mem = max([t.mem_cost  for t in lower_prio] or [0]) 
    B     = max([t.cost      for t in lower_prio] + [N-1]) 

    # check utilization
    u_cmp =     sum([t.utilization()     for t in higher_equal_prio])
    u_int_mem = sum([t.mem_utilization() for t in higher_equal_prio])
    u_ext_mem = 0
    for taskset_ext in tasksets_ext:
        u_ext_mem = u_ext_mem + min(u_int_mem,sum([t.mem_utilization() for t in taskset_ext]))

    if u_cmp + u_ext_mem >= 1: 
        # utilization exceeded
        task.response_time = False        
        return False


    # compute busy period length
    demand = B + task.cost; time = 0
    while demand > time:
        time           = demand
        int_cmp_demand = B + fp_demand(higher_equal_prio, time)
        int_mem_demand = B_mem + fp_demand_mem(higher_equal_prio, time)
        ext_mem_demand = 0
        for taskset_ext in tasksets_ext:
            ext_mem_demand = ext_mem_demand + min(fp_demand_mem(taskset_ext, time), int_mem_demand)
        demand         = int_cmp_demand + ext_mem_demand 
    busy_period = demand

    # find all response times over busy period
    response_times = []
    
    for k in range(1,int(ceil(busy_period/task.period))+1):
        
        mem_start      = max(sum([t.cost for t in higher_prio]),(k-1)*task.period)
        int_cmp_demand = B + fp_demand(higher_prio, mem_start) + (k-1)*task.cost
        int_mem_demand = B_mem + fp_demand_mem(higher_prio, mem_start) + (k-1)*task.mem_cost
        ext_mem_demand = 0
        for taskset_ext in tasksets_ext:
            ext_mem_demand = ext_mem_demand + min(fp_demand_mem(taskset_ext, mem_start), int_mem_demand)
        demand         = int_cmp_demand + ext_mem_demand
        while demand > mem_start:
            mem_start      = demand
            int_cmp_demand = B + fp_demand(higher_prio, mem_start) + (k-1)*task.cost
            int_mem_demand = B_mem + fp_demand_mem(higher_prio, mem_start) + (k-1)*task.mem_cost
            ext_mem_demand = 0
            for taskset_ext in tasksets_ext:
                ext_mem_demand = ext_mem_demand + min(fp_demand_mem(taskset_ext, mem_start), int_mem_demand)
            demand         = int_cmp_demand + ext_mem_demand

        mem_end = mem_start + task.mem_cost
        int_cmp_demand = int_cmp_demand + task.mem_cost
        int_mem_demand = int_mem_demand + task.mem_cost
        ext_mem_demand = 0
        for core in tasksets_ext:
            ext_mem_demand = ext_mem_demand + min(fp_demand_mem(taskset_ext, mem_end), int_mem_demand)
        demand         = int_cmp_demand + ext_mem_demand
        while demand > mem_end:
            mem_end = demand
            if mem_end >= mem_start + task.mem_cost * N:
                mem_end = mem_start + task.mem_cost * N
                break
            ext_mem_demand = 0 
            for taskset_ext in tasksets_ext:
                ext_mem_demand = ext_mem_demand + min(fp_demand_mem(taskset_ext, mem_end), int_mem_demand)        
            demand         = int_cmp_demand + ext_mem_demand

        response_times.append(mem_end + task.exec_cost - (k-1)*task.period)


    task.response_time = max(response_times)

    if task.response_time > task.deadline:
        return False
    else:
        return True




def rta_fully_preemptive_schedulable(taskset, task, taskset_ext=None):
    higher_prio =       [ t for t in taskset.with_higher_priority_than(task) ]
    higher_equal_prio = [ t for t in taskset.with_higher_priority_than(task) ] + [ task ]
    lower_prio =        [ t for t in taskset.with_lower_priority_than(task) ]
    

    # check memory utilization
    if taskset_ext is None:
        u_ext = 0
    else:
        u_ext = sum([t.mem_utilization() for t in taskset_ext])
    u_mem = sum([t.mem_utilization() for t in higher_equal_prio])
    if u_ext + u_mem >= 1:
        # utilization exceeded
        task.response_time = False
        return False

    # compute the worst external interference on memory
    M = max_interference(taskset, taskset_ext)

    # check utilization (memory and computation requirements)
    u_int = sum([t.utilization() for t in higher_equal_prio])
    u = u_int + min(u_ext,M*u_mem)
    if u >= 1: 
        # utilization exceeded
        task.response_time = False
        return False


    task.response_time = task.cost
    int_demand         = fp_demand(higher_equal_prio, task.response_time)
    ext_demand         = min(M*(num_jobs(higher_equal_prio,task.response_time,0)),\
                             fp_demand_mem(taskset_ext,task.response_time))
    demand             = int_demand + ext_demand

    while demand > task.response_time:
        task.response_time = demand
        if task.response_time > task.deadline:
            return False
        int_demand         = fp_demand(higher_equal_prio, task.response_time)
        ext_demand         = min(M*(num_jobs(higher_equal_prio, task.response_time,0)),\
                                 fp_demand_mem(taskset_ext,task.response_time))
        demand             = int_demand + ext_demand


    return True


