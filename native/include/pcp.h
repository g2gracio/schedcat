#ifndef _PCP_H_
#define _PCP_H_

#include "sharedres_types.h"

class PCP_Analysis 
{
public:
	PCP_Analysis() { }

	void test(const ResourceSharingInfo& info);
};

#endif
