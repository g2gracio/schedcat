#include "sharedres.h"
#include "blocking.h"

#include "stl-helper.h"
#include "math-helper.h"

# include "pcp.h"

#include <iostream>

void PCP_Analysis::test(const ResourceSharingInfo & info)
{
	const TaskInfos & tasks = info.get_tasks();
	for(unsigned int i = 0; i < tasks.size(); i++) {
		std::cout << "ID[" << i << "] = " << tasks[i].get_id() << " P=" << tasks[i].get_period() << " C=" << tasks[i].get_cost() << std::endl;
		const Requests & requests = tasks[i].get_requests();		
		if(requests.size() > 0) {
			for(unsigned int j = 0; j < requests.size(); j++) { 
				std::cout << " **** Task[" << i << "] Resource=" << requests[j].get_resource_id() << " CS length=" <<
						requests[j].get_request_length() << std::endl;
			}
		}	
	}
}
