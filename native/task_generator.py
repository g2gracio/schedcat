from schedcat.model.serialize import write
from schedcat.util.time import ms2us
import schedcat.model.resources as resources
import os
import random
from schedcat.generator.tasksets import mkgen
import schedcat.generator.tasks as gen

NAMED_PERIODS = {
# Named period distributions used in several UNC papers, in milliseconds.
    'homogeneous'     	: gen.log_uniform_int(10,  100),
    'heterogeneous'  	: gen.log_uniform_int(1, 1000),
}

NAMED_UTILIZATIONS = {
# Named utilization distributions used in several UNC papers, in milliseconds.
    'light'     : gen.uniform(0.05, 0.1),
    'medium'    : gen.uniform(0.1  , 0.25),
}

# Critical section lenght in microseconds
CSLENGTH = { 'short'  : lambda: random.randint(1,   25),
             'medium' : lambda: random.randint(25,  100),
             'long'   : lambda: random.randint(100, 500), 
}

def generate_taskset_files(util_name, period_name, cap, number):
    generator = mkgen(NAMED_UTILIZATIONS[util_name],
                      NAMED_PERIODS[period_name])
    generated_sets = []
    for i in range(number):
        taskset = generator(max_util=cap)
        filename = "{0}_{1}_{2}_{3}".format(util_name,
                                            period_name, cap, i)
        write(taskset, filename)
        generated_sets.append(filename)
    return generated_sets

def generate_lock_taskset_files(util_name, period_name, cap,
                                cslength, nres, pacc, number):
    generator = mkgen(NAMED_UTILIZATIONS[util_name],
                      NAMED_PERIODS[period_name])
    generated_sets = []
    for i in range(number):
        taskset = generator(max_util=cap, time_conversion=ms2us)
        resources.initialize_resource_model(taskset)
        for task in taskset:
            for res_id in range(nres):
                a = random.random()
                print "i = %d, res_id = %d, a = %.2f, pacc = %.2f" % (i, res_id, a, pacc)
                if a < pacc:
                    #nreqs = random.randint(1, 5)
                    length = CSLENGTH[cslength]
                    #for j in range(nreqs): #somente uma requisacao por ativacao
                    task.resmodel[res_id].add_request(length())
        filename = "{0}_{1}_{2}_{3}_{4}_{5}_{6}".format(
                util_name, period_name, cap, cslength, nres, pacc,
                i)
        write(taskset, filename)
        generated_sets.append(filename)
    return generated_sets

ts = generate_lock_taskset_files("light", "homogeneous", 0.5, "short", 4, 0.05, 10)


