#!/usr/bin/env python
from __future__ import division
from schedcat.model.tasks import SporadicTask
from schedcat.model.tasks import TaskSystem
import schedcat.generator.tasks as gen
import schedcat.mapping.binpack as bp
from generator_emstada import gen_taskset
from collections import defaultdict
from copy import deepcopy
from datetime import datetime
import sys
import os
import optparse


from schedcat.util.time import ms2us
from schedcat.generator.tasksets import mkgen, \
                                        NAMED_UTILIZATIONS, \
                                        NAMED_PERIODS

import schedcat.sched.prem.rta as rta

class SegmentedTask(SporadicTask):
    
    def __init__(self, mem_cost, exec_cost, period, deadline=None, id=None):
        if deadline is None:
            deadline=period
        self.period     = period
        self.exec_cost  = exec_cost
        self.mem_cost   = mem_cost
        self.cost       = exec_cost + mem_cost
        self.deadline   = deadline
        self.id         = id

    def __repr__(self):
        idstr = ", id=%s" % self.id if self.id is not None else ""
        dstr  = ", deadline=%s" % self.deadline if self.deadline != self.period else ""
        ustr = "%.2f" % self.utilization()
        return "Segmented_Task(%s, %s, %s%s%s, %s)" % (self.mem_cost, self.exec_cost, self.period, dstr, idstr, ustr)

    def mem_utilization(self):
        return self.mem_cost / self.period



class SegmentedTaskSystem(TaskSystem):
    
    def max_exec_cost(self):
        return max([t.exec_cost for t in self])

    def max_mem_cost(self):
        return max([t.mem_cost for t in self])

    def mem_utilization(self):
        u = [t.mem_utilization() for t in self]


NAMED_STALLS = {
# Named period distributions used in several UNC papers, in milliseconds.
    'uni-extreme'  : gen.uniform(0.40, 0.65),
    'uni-high'     : gen.uniform(0.20,  0.40),
    'uni-medium'   : gen.uniform(0.05, 0.20),
    'uni-low'      : gen.uniform(0, 0.05)
}


def First_Fit_FP(sorting_factor, N, input_taskset):
# First Fit with the schedulability analysis
    
    taskset = deepcopy(input_taskset)

    if sorting_factor   == 'T':
        taskset = sorted(taskset, key=lambda t: t.period, reverse=False)
    elif sorting_factor == 'U':
        taskset = sorted(taskset, key=lambda t: t.utilization(), reverse=False)
    elif sorting_factor == 'None':
        pass


    partition = []; hp_proc_tasks = []; selected = set()
    
    for core in range(0,N):
        partition.append(SegmentedTaskSystem())
        for task_index, task in enumerate(taskset,0):
            if task_index in selected:
                continue
            partition[core] = SegmentedTaskSystem([ t for t in partition[core]] + [task])
            partition[core].assign_ids_by_period()
            for t in partition[core]:
                if not rta.rta_schedulable(partition[core],t,hp_proc_tasks):
                    partition[core].remove(task)
                    break
            else:
                selected.add(task_index)
        hp_proc_tasks = hp_proc_tasks + partition[core]

    return len(selected) == len(taskset)


def Next_Fit_FP(sorting_factor, N, input_taskset):
# Next Fit with the schedulability analysis
    
    taskset = deepcopy(input_taskset)

    if sorting_factor   == 'T':
        taskset = sorted(taskset, key=lambda t: t.period, reverse=False)
    elif sorting_factor == 'U':
        taskset = sorted(taskset, key=lambda t: t.utilization(), reverse=False)
    elif sorting_factor == 'None':
        pass


    partition = []; hp_proc_tasks = []; core = 0
    partition.append(SegmentedTaskSystem())


    for task in taskset:
        partition[core] = SegmentedTaskSystem([t for t in partition[core]] + [task])
        partition[core].assign_ids_by_period()
        for t in partition[core]:
            if not rta.rta_schedulable(partition[core],t,hp_proc_tasks):
                if core == N-1:
                    return False
                partition[core].remove(task)
                hp_proc_tasks = hp_proc_tasks + partition[core] 
                core = core + 1
                partition.append(SegmentedTaskSystem([task]))
                if not rta.rta_schedulable(partition[core],task,hp_proc_tasks):
                    return False
                else:
                    break
    return True


def First_Fit_TDMA(sorting_factor, N, input_taskset):

    taskset = deepcopy(input_taskset)

    if sorting_factor   == 'T':
        taskset = sorted(taskset, key=lambda t: t.period, reverse=False)
    elif sorting_factor == 'U':
        taskset = sorted(taskset, key=lambda t: t.utilization(), reverse=False)
    elif sorting_factor == 'None':
        pass


    core = 0; partition = []

    for core in range(0,N):
        partition.append([])


    for task in taskset:
        for core in range(0,N):
            partition[core] = SegmentedTaskSystem([ t for t in partition[core]] + [task])
            partition[core].assign_ids_by_period()
            for t in partition[core]:
                if not rta.rta_tdma_schedulable(partition[core],t,N):
                    if core == N-1:
                        return False
                    partition[core].remove(t)
                    break
            else:
                break

    return True
 
    
def Next_Fit_TDMA(sorting_factor, N, input_taskset):
# Next Fit with the schedulability analysis
    
    taskset = deepcopy(input_taskset)

    if sorting_factor   == 'T':
        taskset = sorted(taskset, key=lambda t: t.period, reverse=False)
    elif sorting_factor == 'U':
        taskset = sorted(taskset, key=lambda t: t.utilization(), reverse=False)
    elif sorting_factor == 'None':
        pass


    partition = []; hp_proc_tasks = []; core = 0
    partition.append(SegmentedTaskSystem())


    for task in taskset:
        partition[core] = SegmentedTaskSystem([t for t in partition[core]] + [task])
        partition[core].assign_ids_by_period()
        for t in partition[core]:
            if not rta.rta_tdma_schedulable(partition[core],t,N):
                if core == N-1:
                    return False
                partition[core].remove(task)
                core = core + 1
                partition.append(SegmentedTaskSystem([task]))
                if not rta.rta_tdma_schedulable(partition[core],task,N):
                    return False
                else:
                    break
    return True


def first_fit_with_resized_bins(prem_taskset, N=1):

    error_margin = 0.05
    
    U=0; s=0
    for t in prem_taskset:
        U = U + t.utilization()
        s = s + t.mem_cost/t.cost
    s /= len(prem_taskset)


    # adjust the bins size
    u = [0] * N
    u[0] = min(U*s / (1 - (1-s)**N), 1.0)
    for core in range(1,N):
        u[core] = min(u[core-1] * (1-s), 1.0)


    partition = [ [] ]
    core      = 0
    u_current = 0


    # first fit with different sized bins
    for t in prem_taskset:
        partition[core].append(t)
        u_current = u_current + t.utilization() 
        if  u_current >= u[core] + error_margin and core < N-1:
            partition[core].remove(t)
            core    = core + 1
            partition.append([])
            partition[core].append(t)
            u_current = t.utilization()

    return partition


def simple_heuristic(heuristic_name, sorting_factor, sched_test_type, N, input_taskset):
    
    taskset = deepcopy(input_taskset)

    if heuristic_name == 'EFF' or heuristic_name == 'ENF':
        error_margin = 0.05
        U = sum([ task.utilization() for task in taskset ])


    if   sorting_factor == 'TI':
        taskset = sorted(taskset, key=lambda t: t.period,        reverse=False)
    elif sorting_factor == 'UI':
        taskset = sorted(taskset, key=lambda t: t.utilization(), reverse=False)
    elif sorting_factor == 'TD':
        taskset = sorted(taskset, key=lambda t: t.period,        reverse=True)
    elif sorting_factor == 'UD':
        taskset = sorted(taskset, key=lambda t: t.utilization(), reverse=True)
    elif sorting_factor == 'None':
        pass

   
    if heuristic_name   == 'FF':
        sets = bp.first_fit(taskset, N, 1.0, lambda t:  t.utilization())
    elif heuristic_name == 'WF':
        sets = bp.worst_fit(taskset, N, 1.0, lambda t:  t.utilization())
    elif heuristic_name == 'NF':
        sets = bp.next_fit(taskset, N, 1.0, lambda t:  t.utilization())
    elif heuristic_name == 'EFF':
        sets = bp.first_fit(taskset, N, U/N + error_margin, lambda t:  t.utilization())
    elif heuristic_name == 'ENF':
        sets = bp.next_fit(taskset, N, U/N + error_margin, lambda t:  t.utilization())

    partition = []
    for core in range(0,N):
         partition.append(SegmentedTaskSystem(list(sets[core])))
         partition[core].assign_ids_by_period()

    if sched_test_type == 'TDMA':
        for core in range(0,N):
            for task in partition[core]:
                if not rta.rta_tdma_schedulable(partition[core],task,N):
                    return False
        return True
    elif sched_test_type == 'FP':
        hp_proc_tasks = []
        for core in range(0,N):
            for task in partition[core]:
                if not rta.rta_schedulable(partition[core],task,hp_proc_tasks):
                    return False
            hp_proc_tasks = hp_proc_tasks + partition[core]
        return True
    elif sched_test_type == 'RR':
        for core in range(0,N):
            other_proc_tasksets = []
            for other_core in range(0,N):
                if other_core == core: 
                    continue
                other_proc_tasksets.append(partition[other_core]) 
            for task in partition[core]:
                if not rta.rta_rr_schedulable(partition[core],task,other_proc_tasksets):
                    return False
        return True

    return None 


def compound_heuristics(N=4, tasks_per_core=4, runs=10, period_min=10, period_max=100):

    # test paramters
    utilization_step = 0.1
    heuristics       = ['FF','NF']
    sorting_factors  = ['T']
    sched_tests      = ['FP', 'TDMA']
    #runs             = 10


    results = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int))))
    test_utilizations = [ util*utilization_step for util in  range(1,int(N/utilization_step)) ]

    for r in range(0,runs):
        for util in test_utilizations:
            prem_taskset = taskset_generation(util, period_min, period_max, N, 'uni-medium', tasks_per_core)
            for sorting_factor in sorting_factors:
                results[util]['FF'][sorting_factor]['FP'] += First_Fit_FP(sorting_factor, N, prem_taskset)
                results[util]['NF'][sorting_factor]['FP'] += Next_Fit_FP(sorting_factor, N, prem_taskset)
                results[util]['FF'][sorting_factor]['TDMA'] += First_Fit_TDMA(sorting_factor, N, prem_taskset)
                results[util]['NF'][sorting_factor]['TDMA'] += Next_Fit_TDMA(sorting_factor, N, prem_taskset)



    f = create_log_file('compound_test',tasks_per_core,period_min,period_max,N,runs)
    f.write("U\tFF-T-FP\tFF-T-TD\tNF-T-FP\tNF-T-TD\n")
    for util in test_utilizations:
        f.write ("%.2f\t%s\t%s\t%s\t%s\n" % \
                (util,\
                results[util]['FF']['T']['FP'],\
                results[util]['FF']['T']['TDMA'],\
                results[util]['NF']['T']['FP'],\
                results[util]['NF']['T']['TDMA']))
    f.close()


def simple_heuristics(N=4, tasks_per_core=4, runs=10, period_min=10, period_max=100):

    # test paramters
    utilization_step = 0.1
    sorting_factors  = ['UI', 'UD', 'TI', 'TD',  'None']
    heuristics       = ['FF', 'WF', 'NF', 'EFF', 'ENF' ]
    sched_tests      = ['FP', 'TDMA', 'RR']

    results = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int))))
    test_utilizations = [ util*utilization_step for util in  range(1,int(N/utilization_step)) ]

    for r in range(0,runs):
        for util in test_utilizations:
            prem_taskset = taskset_generation(util, period_min, period_max, N, 'uni-medium', tasks_per_core, 4/N)
            for heuristic_type in heuristics:
                for sorting_factor in sorting_factors:
                    for sched_test in sched_tests:
                        results[util][heuristic_type][sorting_factor][sched_test] += simple_heuristic(heuristic_type,sorting_factor,sched_test,N,prem_taskset)

    for heuristic_type in heuristics:
        logfile_name = 'simple_heuristic_' + str(heuristic_type)
        f = create_log_file(logfile_name,tasks_per_core,period_min,period_max,N,runs)
        f.write("U\tno-Co\tno-FP\tno-RR\tUI-Co\tUI-FP\tUI-RR\tUD-Co\tUD-FP\tUD-RR\tTI-Co\tTI-FP\tTI-RR\tTD-Co\tTD-FP\tTD-RR\n")
        for util in test_utilizations:
            f.write ("%.2f\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % \
                (util,\
                results[util][heuristic_type]['None']['TDMA'],\
                results[util][heuristic_type]['None']['FP'],\
                results[util][heuristic_type]['None']['RR'],\
                results[util][heuristic_type]['UI']['TDMA'],\
                results[util][heuristic_type]['UI']['FP'],\
                results[util][heuristic_type]['UI']['RR'],\
                results[util][heuristic_type]['UD']['TDMA'],\
                results[util][heuristic_type]['UD']['FP'],\
                results[util][heuristic_type]['UD']['RR'],\
                results[util][heuristic_type]['TI']['TDMA'],\
                results[util][heuristic_type]['TI']['FP'],\
                results[util][heuristic_type]['TI']['RR'],\
                results[util][heuristic_type]['TD']['TDMA'],\
                results[util][heuristic_type]['TD']['FP'],\
                results[util][heuristic_type]['TD']['RR']\
                ))
        f.close()


def mem_test(N=4, tasks_per_core=8, runs=10, period_min=10, period_max=100, utilization_step=0.1):


    test_utilizations = [ step*utilization_step for step in range(1,int(1/utilization_step)) ]
    num_tasks         = tasks_per_core * N


    tdma_schedulable  = defaultdict(lambda: defaultdict(int))
    fp_schedulable    = defaultdict(lambda: defaultdict(int))
    nomem_schedulable = defaultdict(lambda: defaultdict(int))

    for r in range(0,runs):
        for mem_stall in ['uni-extreme', 'uni-high', 'uni-medium', 'uni-low']:
            for util in test_utilizations:
                partition = []
                for core in range(0,N):
                    partition.append(SegmentedTaskSystem(taskset_generation(util, period_min, period_max, 1, mem_stall, tasks_per_core, 4/N)))
                    partition[core].assign_ids_by_period()

                hp_proc_tasks = []
                for core in range(0,N):
                    for task in partition[core]:
                        task.fp_schedulable = rta.rta_schedulable(partition[core],task,hp_proc_tasks)
                    for task in partition[core]:
                        hp_proc_tasks.append(task)

                # instead of deepcopy just clear response times
                for core in range(0,N):
                    for task in partition[core]:
                        task.response_time    = task.period 

                for core in range(0,N):
                    for task in partition[core]:
                        task.nomem_schedulable = rta.rta_schedulable(partition[core],task,[])
                    for task in partition[core]:
                        hp_proc_tasks.append(task)

                # instead of deepcopy just clear response times
                for core in range(0,N):
                    for task in partition[core]:
                        task.response_time    = task.period 

                for core in range(0,N):
                    for task in partition[core]:
                        task.tdma_schedulable = rta.rta_tdma_schedulable(partition[core],task,N)

                tdma_schedulable[mem_stall][util]  += sum([ sum([task.tdma_schedulable  for task in partition[core]])  for core in range(0,N)])
                fp_schedulable[mem_stall][util]    += sum([ sum([task.fp_schedulable    for task in partition[core]])  for core in range(0,N)])
                nomem_schedulable[mem_stall][util] += sum([ sum([task.nomem_schedulable for task in partition[core]])  for core in range(0,N)])


    for mem_stall in ['uni-extreme', 'uni-high', 'uni-medium', 'uni-low']:
        for util in test_utilizations:
            tdma_schedulable[mem_stall][util]  /= runs*num_tasks
            fp_schedulable[mem_stall][util]    /= runs*num_tasks
            nomem_schedulable[mem_stall][util] /= runs*num_tasks
     
    f = create_log_file('mem_test', tasks_per_core, period_min, period_max, N, runs)
    f.write("U\tNoMem\NoMem\tNoMem\tNoMem\tFP\tFP\tFP\tFP\tCo\tCo\tCo\tCo\t from low to extreme\n")
    for util in test_utilizations:
        f.write ("%.2f\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % \
                (util,\
                nomem_schedulable['uni-low'][util],\
                nomem_schedulable['uni-medium'][util],\
                nomem_schedulable['uni-high'][util],\
                nomem_schedulable['uni-extreme'][util],\
                fp_schedulable['uni-low'][util],\
                fp_schedulable['uni-medium'][util],\
                fp_schedulable['uni-high'][util],\
                fp_schedulable['uni-extreme'][util],\
                tdma_schedulable['uni-low'][util],\
                tdma_schedulable['uni-medium'][util],\
                tdma_schedulable['uni-high'][util],\
                tdma_schedulable['uni-extreme'][util] \
                ))
    f.close()

def gero_case():
    
    avg_1 = SegmentedTask(96774, 119387, 3333000)
    sha_1 = SegmentedTask(96774, 211451, 10000000)
    
    avg_2 = SegmentedTask(96774, 119387, 3333000)
    sha_2 = SegmentedTask(96774, 211451, 10000000)

    avg_3 = SegmentedTask(96774, 119387, 3333000)
    sha_3 = SegmentedTask(96774, 211451, 10000000)


    core1 = SegmentedTaskSystem([avg_1, sha_1])
    core1.assign_ids_by_period()
    core2 = SegmentedTaskSystem([avg_2, sha_2])
    core2.assign_ids_by_period()
    core3 = SegmentedTaskSystem([avg_2, sha_2])
    core3.assign_ids_by_period()


    rta.rta_schedulable(core1,avg_1)
    rta.rta_schedulable(core1,sha_1)
    
    rta.rta_schedulable(core2,avg_2,core1)
    rta.rta_schedulable(core2,sha_2,core1)

    hp_proc_tasks = []

    hp_proc_tasks.append(avg_1)
    hp_proc_tasks.append(avg_2)
    hp_proc_tasks.append(sha_1)
    hp_proc_tasks.append(sha_2)

    rta.rta_schedulable(core3,avg_3,hp_proc_tasks)
    rta.rta_schedulable(core3,sha_3,hp_proc_tasks)

    print "core, avg, sha"

    print 1, avg_1.response_time, sha_1.response_time
    print 2, avg_2.response_time, sha_2.response_time
    print 3, avg_3.response_time, sha_3.response_time


def gero_case_synchrone():
    
    avg_1 = SegmentedTask(96774, 119387, 3333000)
    sha_1 = SegmentedTask(96774, 211451, 10000000)
    
    avg_2 = SegmentedTask(96774, 119387, 3333000)
    sha_2 = SegmentedTask(96774, 211451, 10000000)

    avg_3 = SegmentedTask(96774, 119387, 3333000)
    sha_3 = SegmentedTask(96774, 211451, 10000000)


    core1 = SegmentedTaskSystem([avg_1, sha_1])
    core1.assign_ids_by_period()
    core2 = SegmentedTaskSystem([avg_2, sha_2])
    core2.assign_ids_by_period()
    core3 = SegmentedTaskSystem([avg_2, sha_2])
    core3.assign_ids_by_period()


    rta.rta_schedulable_synchr(core1,avg_1)
    rta.rta_schedulable_synchr(core1,sha_1)

    print "core, avg, sha"
    print 1, avg_1.response_time, sha_1.response_time

#    rta.rta_schedulable(core1,avg_1)
#    rta.rta_schedulable(core1,sha_1)
    
    rta.rta_schedulable_synchr(core2,avg_2,core1)
    rta.rta_schedulable_synchr(core2,sha_2,core1)

    print core1

    print 2, avg_2.response_time, sha_2.response_time

#    rta.rta_schedulable(core2,avg_2,core1)
#    rta.rta_schedulable(core2,sha_2,core1)

    hp_proc_tasks = []

    hp_proc_tasks.append(avg_1)
    hp_proc_tasks.append(avg_2)
    hp_proc_tasks.append(sha_1)
    hp_proc_tasks.append(sha_2)

    print hp_proc_tasks

    rta.rta_schedulable_synchr(core3,avg_3,hp_proc_tasks)
    rta.rta_schedulable_synchr(core3,sha_3,hp_proc_tasks)

    print 3, avg_3.response_time, sha_3.response_time


 






def basic_rta_test(N=4, util=0.6, task_per_core=8, runs=10, period_min=10, period_max=100, h_overhead=6):

    results = []
    for core in range (0,N):
        results.append([])

    for r in range(0,runs):
        partition = []
        for core in range(0,N):
            partition.append(SegmentedTaskSystem(taskset_generation(util, period_min, period_max, 1, 'uni-medium', task_per_core, 4/N,0)))
            partition[core].assign_ids_by_period()


        for core in range(0,N):
            for task in partition[core]:
                task.tdma_schedulable = rta.rta_tdma_schedulable(partition[core],task,N)

        # clear response times
        for core in range(0,N):
            for task in partition[core]:
                task.tdma_response_time = task.response_time
                task.response_time      = task.period


        for core in range(0,N):
            for task in partition[core]:
                task.rr_schedulable = False
                task.rr_response_time = 1


        for core in range(0,N):
            for task in partition[core]:
                task.mem_cost = task.mem_cost + h_overhead

        hp_proc_tasks = []
        for core in range(0,N):
            for task in partition[core]:
                task.fp_schedulable = rta.rta_schedulable(partition[core],task,hp_proc_tasks)
            for task in partition[core]:
                hp_proc_tasks.append(task)

        # instead of deepcopy just clear response times
        for core in range(0,N):
            for task in partition[core]:
                task.fp_response_time = task.response_time
                task.response_time    = task.period 




#        for core in range(0,N):
#            other_proc_tasksets = []
#            for other_core in range(0,N):
#                if other_core == core: 
#                    continue
#                other_proc_tasksets.append(partition[other_core]) 
#            for task in partition[core]:
#                task.rr_schedulable = rta.rta_rr_schedulable(partition[core],task,other_proc_tasksets)
#            for task in partition[core]:
#                task.rr_response_time = task.response_time


        tdma_schedulable = [0] * N 
        fp_schedulable   = [0] * N 
        rr_schedulable   = [0] * N
        fp_rt_ratio      = [0] * N
        fp_sched_ratio   = [0] * N 
        rr_rt_ratio      = [0] * N
        rr_sched_ratio   = [0] * N 

        for core in range(0,N):
            tdma_schedulable     = sum([task.tdma_schedulable for task in partition[core]])
            fp_schedulable       = sum([task.fp_schedulable   for task in partition[core]])
            rr_schedulable       = sum([task.rr_schedulable   for task in partition[core]])
            num_tasks            = len(partition[core])
            fp_sched_ratio[core] = ((fp_schedulable - tdma_schedulable) / num_tasks) * 100
            rr_sched_ratio[core] = ((rr_schedulable - tdma_schedulable) / num_tasks) * 100
            sum_fp_rt_difference = 0
            for task in partition[core]:
                if task.tdma_response_time is False and task.fp_response_time is False:
                    pass
                elif task.tdma_response_time is False:
                    sum_fp_rt_difference += 1.0
                elif  task.fp_response_time is False:
                    sum_fp_rt_difference -= 1.0
                else:
                    sum_fp_rt_difference += (task.tdma_response_time - task.fp_response_time) / task.tdma_response_time
            fp_rt_ratio[core] = (sum_fp_rt_difference / num_tasks) * 100

            sum_rr_rt_difference = 0
            for task in partition[core]:
                if task.tdma_response_time is False and task.rr_response_time is False:
                    pass
                elif task.tdma_response_time is False:
                    sum_rr_rt_difference += 1.0
                elif  task.rr_response_time is False:
                    sum_rr_rt_difference -= 1.0
                else:
                    sum_rr_rt_difference += (task.tdma_response_time - task.rr_response_time) / task.tdma_response_time
            rr_rt_ratio[core] = (sum_rr_rt_difference / num_tasks) * 100



            results[core].append((fp_rt_ratio[core],fp_sched_ratio[core],rr_rt_ratio[core],rr_sched_ratio[core]))


    f = create_log_file('rt_and_sched_test',task_per_core,period_min,period_max,N,runs)
    f.write("core\tfp-rt\tfp-sch\trr-rt\trr-sch\n")
    for core in range(0,N):
        f.write ("%s\t%.3f\t%.3f\t%.3f\t%.3f\n" % \
                (core,\
                sum([ fp_rt_ratio for (fp_rt_ratio,fp_sched_ratio,rr_rt_ratio,rr_sched_ratio)    in results[core] ])/runs,\
                sum([ fp_sched_ratio for (fp_rt_ratio,fp_sched_ratio,rr_rt_ratio,rr_sched_ratio) in results[core] ])/runs,\
                sum([ rr_rt_ratio for (fp_rt_ratio,fp_sched_ratio,rr_rt_ratio,rr_sched_ratio)    in results[core] ])/runs,\
                sum([ rr_sched_ratio for (fp_rt_ratio,fp_sched_ratio,rr_rt_ratio,rr_sched_ratio) in results[core] ])/runs\
                ))
    f.close()


def taskset_generation(utilization, period_min=10, period_max=100, N=4, stall_distribution='uni-medium',task_per_core=4, mem_scale=1, hc_overhead=0):
    # period min and max in ms

    # stall ratio is generated randomly
    stall_ratio = NAMED_STALLS[stall_distribution]

    taskset          = gen_taskset((period_min,period_max), 'logunif', task_per_core*N, utilization)
    premized_taskset = []
    for task in taskset:
        mem_cost = int(task.cost*stall_ratio()) * mem_scale + hc_overhead
        cpu_cost = task.cost - mem_cost
        period   = task.period
        premized_task = SegmentedTask(mem_cost, cpu_cost, period)
        premized_taskset.append(premized_task)
    return premized_taskset


def create_log_file(test_name, tasks_per_core, period_min, period_max, N, runs):
    now = datetime.now()
    if not os.path.exists('dir_results'):
        os.makedirs('dir_results')
    dirname  = "dir_results/" + str(test_name)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    filename = str(N) + "_cores_" + str(tasks_per_core) \
                      + "_task_per_core_with_periods_" + str(period_min) + "_" +str(period_max) + "_runs_" + str(runs) + "_" \
                      +  now.strftime("%d_%m_%H_%M_%S")
    filename = dirname + "/" + filename + ".log"
    return open(filename,"w")


def main():
    #don't add help option as we will handle it ourselves
    parser = optparse.OptionParser()
    parser.add_option("-u", "--taskset-utilisation",
                       type="float", dest="util",
                      default="0.5")
    parser.add_option("-n", "--tasks-per-core",
                      type="int", dest="tasks_per_core",
                      default="4")
    parser.add_option("-N", "--core-number",
                      type="int", dest="core_number",
                      default="4")
    parser.add_option("-t", "--test-type",
                      type="string", dest="test_type",
                      help="basic_rta, heuristic, compound, memory")
    parser.add_option("-r", "--runs",
                      type="int", dest="runs",
                      default="10")
    parser.add_option("-p", "--period-min",
                      type="int", dest="period_min",
                      default="10")
    parser.add_option("-P", "--period-max",
                      type="int", dest="period_max",
                      default="100")
    parser.add_option("-s", "--step",
                      type="float", dest="utilization_step",
                      default="0.1")


    (options, args) = parser.parse_args()

    if options.test_type == "basic_rta":
        print "running Basic RTA test"
        basic_rta_test(options.core_number, options.util, options.tasks_per_core, options.runs, options.period_min, options.period_max, 6)
    elif options.test_type == "heuristic":
        print "running Simple Heuristics test"
        simple_heuristics(options.core_number, options.tasks_per_core, options.runs, options.period_min, options.period_max)
    elif options.test_type == "compound":
        print "running compound Heuristics test"
        compound_heuristics(options.core_number, options.tasks_per_core, options.runs, options.period_min, options.period_max)
    elif options.test_type == "memory":
        print "running memory test"
        mem_test(options.core_number, options.tasks_per_core, options.runs, options.period_min, options.period_max, options.utilization_step)
    else:
        parser.print_help()

if __name__ == "__main__":
    gero_case_synchrone()
    #gero_case()
    sys.exit(0)
    sys.exit(main())
