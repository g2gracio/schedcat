import os
import random
import schedcat.generator.tasks as gen
import schedcat.mapping.binpack as bp
from schedcat.mapping.rollback import Bin

from schedcat.model.serialize import write
from schedcat.generator.tasksets import mkgen #, \
                                    #    NAMED_UTILIZATIONS, \
                                    #    NAMED_PERIODS
from schedcat.util.time import ms2us
import schedcat.model.resources as resources

from ecrts2019_utils import *

import pandas as pd
import pylab as pl
import argparse

NAMED_PERIODS = {
# Named period distributions used in several UNC papers, in milliseconds.
    'uni-short'     : gen.uniform_int( 3,  33),
    'uni-moderate'  : gen.uniform_int(10, 100),
    'uni-long'      : gen.uniform_int(50, 250),

    'log-uni-short'     : gen.log_uniform_int( 3,  33),
    'log-uni-moderate'  : gen.log_uniform_int(10, 100),
    'log-uni-long'      : gen.log_uniform_int(50, 250),
}

NAMED_UTILIZATIONS = {
# Named utilization distributions used in several UNC papers, in milliseconds.
    'uni-light'     : gen.uniform(0.01, 0.2), #(0.001, 0.1)
    'uni-light-2'     : gen.uniform(0.01, 0.1),
    'uni-medium'    : gen.uniform(0.1  , 0.4), 
    'uni-heavy'     : gen.uniform(0.5  , 0.9),

    'exp-light'     : gen.exponential(0, 1, 0.10),'bimo-light'    : gen.multimodal([(gen.uniform(0.001, 0.5), 8),
                                      (gen.uniform(0.5  , 0.9), 1)]),
    'bimo-medium'   : gen.multimodal([(gen.uniform(0.001, 0.5), 6),
                                      (gen.uniform(0.5  , 0.9), 3)]),
    'bimo-heavy'    : gen.multimodal([(gen.uniform(0.001, 0.5), 4),
                                      (gen.uniform(0.5  , 0.9), 5)]),
    'exp-medium'    : gen.exponential(0, 1, 0.25),
    'exp-heavy'     : gen.exponential(0, 1, 0.50),
}

def generate_taskset_files(util_name, period_name, cap, number):
    generator = mkgen(NAMED_UTILIZATIONS[util_name],
                      NAMED_PERIODS[period_name])
    generated_sets = []
    for i in range(number):
        taskset = generator(max_util=cap, time_conversion=ms2us)
        filename = "{0}_{1}_{2}_{3}".format(util_name,
                                            period_name, cap, i)
        #write(taskset, filename)
        #generated_sets.append(filename)
        generated_sets.append(taskset)
    return generated_sets

#cost_factor = [0.5, 1.0, 5.0, 10.0, 50.0 ]
#util_name = ["uni-light", "uni-medium", "uni-heavy", "bimo-light", "bimo-medium", "bimo-heavy"]

fixed_cost = 2 #in ms - it is automatically converted to us in "g.sd_tasks_fixed_cost()"
util_name = ["uni-light"]
dma_name = [0.6, 0.8, 1.0, 1.2] #time to do DMA as percentage of task cost
jitter = 2 # multiplied by the period

n_cores = 3
factor_M = n_cores
number_of_tasksets = 100
utilization_cap = 1.0

#The next MUST be in us!!!!
# Values used by Rodolfo
#tdma_round_lenght = 200
#tdma_slot_size =  tdma_round_lenght / float(n_cores)
#tdma_slot_size_wo_overhead = tdma_slot_size

#Values used for the paper
tdma_round_lenght = 240
tdma_slot_size =  tdma_round_lenght / float(n_cores)
tdma_slot_size_wo_overhead = (tdma_slot_size) - (tdma_slot_size * 0.05)

#This is for the fixed size. This should be total time to load or reload a task.
#Setting the round_lenght to 1000 is the same as a DMA of 1.0
tdma_round_length_fixed = 1000
tdma_slot_size_fixed = tdma_round_length_fixed / float(n_cores)

util_range = pl.frange(0.1, utilization_cap, 0.1)


#def generate_sd_taskset_files(number_of_tasksets, cap, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, has_streaming_task):
def generate_sd_taskset_files(number_of_tasksets, has_streaming_task):
    generated_sets = []

    ''' 
    tdma_slot_size is the size of the TDMA in the core under analysis
    tdma_slot_size_wo_overhead is the size of the TDMA in the core under analysis without overhead
    tdma_round_lenght is the lenght of the complete TDMA round
    factor_M - under contention, each memory request takes M times to complete
    '''

    #global cost_factor, util_name, dma_name, jitter
    global fixed_cost, util_name, dma_name, jitter,util_range

    for util in util_name:
        #for period in period_name:
        #for cost in cost_factor:
        for dma in dma_name:
            for ts_utilization_bound in util_range:
                for n_task_sets in xrange(number_of_tasksets):
                    tasks = []
                    #hrt_tasks = g.tasks(None, ts_utilization_bound, True, ms2us)
                    if ts_utilization_bound <= 0.1:
                        # this is necessary for the utilization cap of 0.1, otherwise the first task may 
                        # have utilization higher than 0.1 and the task set will be empty
                        g = gen.TaskGenerator(None, NAMED_UTILIZATIONS['uni-light-2']) 
                    else:
                        g = gen.TaskGenerator(None, NAMED_UTILIZATIONS[util])
                        
                    #hrt_tasks = g.sd_tasks_cost_factor(cost, tdma_slot_size, None, ts_utilization_bound, True, ms2us)
                    hrt_tasks = g.sd_tasks_fixed_cost(fixed_cost, None, ts_utilization_bound, False, ms2us)
                    
                    streaming_task = 0
                    max  = -1.0
                    generated_tasks = []

                    for t in hrt_tasks:
                        generated_tasks.append(t)
                        if t.utilization() > max:
                            max = t.utilization()
                            streaming_task = t

                    #print "hrt_tasks", len(generated_tasks)
                    task_id = 0
                    for task in generated_tasks:
                        #print "Task =", task

                        arrival_curve_jitter = 0.0
                        input_load_time = 0.0
                        output_unload_time = 0.0
                        load_time = 0.0
                        unload_time = 0.0  

                        if has_streaming_task == True:
                            
                            if task == streaming_task:
                                #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
                                #generate DMA parameters here
                                #arrival_curve_jitter = task.period * jitter
                                arrival_curve_jitter = task.period * jitter
                                task.deadline = 4 * task.period
                                                
                        input_load_time = (task.cost * float(dma)) / 2 * tdma_slot_size / tdma_round_lenght
                        output_unload_time = (task.cost * float(dma)) / 2 * tdma_slot_size / tdma_round_lenght
                        load_time = 0
                        unload_time = 0
                        
                        '''
                        input_load_time = (task.cost * float(dma)) / 4 
                        output_unload_time = (task.cost * float(dma)) / 4
                        load_time = (task.cost * float(dma)) / 4
                        unload_time = (task.cost * float(dma)) / 4
                        '''
                        
                        t = SD_Task(arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, True, task.cost, task.period, task.deadline, task.deadline, task_id, 0)
                        
                        tasks.append(t)

                        task_id = task_id + 1
                        
                    sd_ts = SD_TaskSystem(tasks)
                    #print "Generated ts:", sd_ts
                    #filename = "generated_ts_ecrts2019/{0}_cost-{1}_dma-{2}_cap-{3}_{4}_{5}".format("sd_ts", util, cost, dma, ts_utilization_bound, n_task_sets)
                    filename = "generated_ts_ecrts2019/{0}_util-{1}_fixed-cost-{2}_dma-{3}_cap-{4}_{5}".format("sd_ts", util, str(fixed_cost), dma, ts_utilization_bound, n_task_sets)
                    sd_write(sd_ts, filename)
                    generated_sets.append(filename)
                    #exit()
                    del sd_ts
                    del tasks

    return generated_sets

def utilization(task):
	#print "period = %d, wcet = %d, util = %.4f" % (task.period, task.cost, task.utilization())
	return task.utilization()

bin_packing_error = False

def misfit(task):
    global bin_packing_error
    bin_packing_error = True
    print "Not able to partition the task set - Task utilization %.4f" % (task.utilization())

def task_set_partitioning(ts):
    #for x in ts[0]:
    #    print("Printing task")
    #    print(x)

    bin_packing_error = False

    print "Task set utilization = %.2f" % ts[0].utilization()
    sets = bp.best_fit_decreasing(ts[0], 4, 1, utilization, misfit)

    if bin_packing_error == False:
        for i in sets:
            print(i)
    else:
        print "BDF was not able to partition the TS"

def test_SD_ts():
    tasks = []
    #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
    
    #task set is schedulable
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(800, 1, 1, 1, 1, True, 100, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))

    '''
    #task set is schedulable
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(1800, 1, 1, 1, 1, True, 100, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))
    '''

    '''
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(1800, 1, 1, 1, 1, True, 100, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 2, 1, 2, 1, True,  10, 1000, 3, 1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True,  10, 1000, 4,  1000, 3, 0))
    '''

    '''
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(1800, 1, 1, 1, 1, True, 10, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 2, 1, 2, 1, True,  10, 1000, 3, 1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True,  10, 1000, 4,  1000, 3, 0))
    '''

    '''
    # this tests Hfinal_i_k
    tasks.append(SD_Task(0, 0, 0, 7, 7, True, 50, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(2000, 0, 0, 10, 10, True, 50, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 0, 0, 5, 5, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 0, 0, 1, 1, True, 100, 1000, 4,  1000, 3, 0))
    '''

    '''
    # this tests Hcons_i_k
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 50, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(2000, 1, 1, 1, 1, True, 10, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 1, 2, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))
    '''

    '''
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 50, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(3000, 1, 1, 1, 1, True, 11, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 1, 2, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))
    '''

    sd_ts = SD_TaskSystem(tasks)

    '''
    schedulable = sd_ts.Saud_schedulability_test()

    if schedulable == True:
        print "Task set is schedulable by Saud's algorithm"
    else:
        print "Task set is NOT schedulable by Saud's algorithm"
        print "\n\n******************************\n\n"
    '''
    
    #tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length
    schedulable = sd_ts.SPM_Stream_Schedulability_test(1, 1, 3)

    if schedulable == True:
        print "Task set is schedulable by SPM Stream test"
    else:
        print "Task set is NOT schedulable by SPM Stream test"
    
    print "\n\n-------------------No Contention-----------\n\n"
    schedulable = sd_ts.Contention_schedulability_test(1, 1, 3, 3)

    if schedulable == True:
        print "Task set is schedulable by Contention test"
    else:
        print "Task set is NOT schedulable by Contention test"

    print "\n\n-------------------No Streaming-----------\n\n"
    schedulable = sd_ts.No_streaming_schedulability_test(1, 1, 3)

    if schedulable == True:
        print "Task set is schedulable by No Streaming test"
    else:
        print "Task set is NOT schedulable by No Streaming test"

    #for task in sd_ts:
    #    print(task)

#ts = generate_taskset_files('uni-medium', 'uni-moderate', 4, 1)
#print(ts)
#task_set_partitioning(ts)

#def apply_sched_tests_ecrts2019(number_of_tasksets, cap, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M):

def apply_sched_tests_ecrts2019(number_of_tasksets, factor_M):
    global fixed_cost, util_name, dma_name, jitter, tdma_slot_size, tdma_round_lenght, util_range, tdma_round_length_fixed, tdma_slot_size_fixed #cost_factor

    for util in util_name:
        #for period in period_name:
        #for cost in cost_factor:
        for dma in dma_name:
            #print "Schedulability tests for - Util:", util,"Cost:", cost, "Dma %:", dma
            print "Schedulability tests for - Util:", util,"Fixed Cost:", fixed_cost, "Dma %:", dma
            #data_frame = pd.DataFrame(columns=['Utili', 'SPM Stream', 'No Streaming', 'Contention-Based', 'Saud'])
            data_frame = pd.DataFrame(columns=['Utili', 'PREM', 'PREM-fixed slot', 'Contention'])
            data_frame.set_index('Utili')
            i = 0
            #for ts_utilization_bound in pl.frange(0.1, cap, 0.1):
            for ts_utilization_bound in util_range:
                prem = 0
                prem_fixed = 0
                contention = 0
                #saud = 0
                for n_task_sets in xrange(number_of_tasksets):
                    #input_filename = "generated_ts_ecrts2019/{0}_cost-{1}_dma-{2}_cap-{3}_{4}_{5}".format("sd_ts", util, cost, dma, ts_utilization_bound, n_task_sets)
                    input_filename = "generated_ts_ecrts2019/{0}_util-{1}_fixed-cost-{2}_dma-{3}_cap-{4}_{5}".format("sd_ts", util, str(fixed_cost), dma, ts_utilization_bound, n_task_sets)
                    sd_ts = read_sd_ts_from_file(input_filename)

                    prem = prem + int(sd_ts.No_streaming_schedulability_test_base(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght))
                    prem_fixed = prem_fixed + int(sd_ts.No_streaming_schedulability_test_base_fixed(tdma_slot_size_fixed, tdma_round_length_fixed))
                    contention = contention + int(sd_ts.Contention_no_streaming_schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M))
                    
                    #saud = saud + int(sd_ts.Saud_schedulability_test())

                prem = prem / float(number_of_tasksets)
                prem_fixed = prem_fixed / float(number_of_tasksets)
                contention = contention / float(number_of_tasksets)
                #saud = saud / float(number_of_tasksets)

                #data_frame.loc[i] = [ts_utilization_bound, spm_stream, no_streaming, contention, saud]
                data_frame.loc[i] = [ts_utilization_bound, prem, prem_fixed, contention]
                i = i + 1
            print data_frame
            #output_filename = "{0}_cost-{1}_dma-{2}.{3}".format(util, cost, dma, "txt")
            output_filename = "{0}_fixed-cost-{1}_dma-{2}.{3}".format(util, str(fixed_cost), dma, "txt")
            data_frame.to_csv("graphs/results/" + output_filename, sep=' ')
            del data_frame

def eembc_and_streaming_benchmarks():
    eembc_tasks = []
    streaming_tasks = []

    tdma_slot_size = 20
    tdma_slot_size_wo_overhead = tdma_slot_size - (tdma_slot_size * 0.01)
    tdma_round_lenght = tdma_slot_size * 3
    factor_M = 3 # 3 cores

    #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
    
    #IDs are defined by assing_ids call, later in the code
    eembc_tasks.append(SD_Task(0, 5, 5, 5, 5, True, 50, 700, 700, 2400, -1, 0))
    eembc_tasks.append(SD_Task(0, 1, 1, 1, 1, True, 10, 500, 500, 2300, -1, 0))
    eembc_tasks.append(SD_Task(0, 3, 3, 3, 3, True, 20, 400, 400, 1200, -1, 0))
    eembc_tasks.append(SD_Task(0, 4, 4, 4, 4, True, 40, 450, 450, 1300, -1, 0))

    streaming_tasks.append(SD_Task(10, 2, 2, 2, 2, True, 500, 3000, 3000, 3000, -1, 0))

    #generate unique integer numbers
    choices = list(range(len(eembc_tasks)))
    random.shuffle(choices)
    task_set = []
    for i in xrange(3):
        index = choices.pop()
        task_set.append(eembc_tasks[index])
    
    #choose a streaming task and append it to the task set
    streaming_index = random.randint(0, len(streaming_tasks) - 1)
    task_set.append(streaming_tasks[streaming_index])

    #creates a Synchrounous Dataflow task set
    sd_ts = SD_TaskSystem(task_set)
    sd_ts.assign_ids()

    print "Generated task set is:"
    print sd_ts

    #apply the schedulability tests
    spm_stream = int(sd_ts.SPM_Stream_Schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght))
    contention = int(sd_ts.Contention_schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M))
    no_streaming = int(sd_ts.No_streaming_schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght))

    print "SPM Stream =", spm_stream, " Contention =", contention, " No Streaming =", no_streaming

'''
A synthetic study where we used the vision benchmarks (I assume we have DRAM there). This is done by randomly 
generating tasks with the parameters of one of the benchmarks, as I suggested in my previous text. For this study, 
we look at how we compare against contention too. Set all jitter to 0. Basically, we just do 
utilization vs schedulability by testing a large number of task sets.
'''
def benchmark_evaluation():
    jitter = 0
    n_cores = 3
    factor_M = 1
    tdma_round_lenght = 100 * n_cores
    tdma_slot_size =  100
    tdma_slot_size_wo_overhead = tdma_slot_size - (tdma_slot_size * 0.04) # DMA overhead is 3.98 us
    
    BRAM_size = 1 * 1024 * 1024 #1 MB - big enough to fit the biggest detector (NFER - around 300KB) in one partition of 512 KB
    DMA_time_to_transfer_1_byte = 0.0009 #in us
    tdma_slot_size_fixed = BRAM_size / 2 * DMA_time_to_transfer_1_byte
    tdma_round_length_fixed = tdma_slot_size_fixed * 3 

    util_cap = pl.frange(0.1, 1.0, 0.1)
    total_number_of_task_sets = 100

    #original task parameters
    original_tasks_bram = []
    original_tasks_dram = []

    '''
    Tasks parameters (in us)

                WCET BRAM contention   WCET DRAM contention     DMA transfer time Data+Code
    Disparity   113678.45              119529.54                86.58
    mser        7978.03                9272.38                  390.85
    IDC         216278.96              217454.01                22.00
    FFT         5206.15                5206.42                  10.60
    '''

    #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
    original_tasks_bram.append(SD_Task(jitter, 86.58,   86.58, 0, 0, True, 113678.45,  0, 0, 0, -1, 0)) #Disparity
    original_tasks_bram.append(SD_Task(jitter, 390.85, 390.85, 0, 0, True, 7978.03,    0, 0, 0, -1, 0)) #mser
    original_tasks_bram.append(SD_Task(jitter, 22.00,   22.00, 0, 0, True, 216278.96,  0, 0, 0, -1, 0)) #IDC
    original_tasks_bram.append(SD_Task(jitter, 10.60,   10.60, 0, 0, True, 5206.15,    0, 0, 0, -1, 0)) #FFT

    original_tasks_dram.append(SD_Task(jitter, 86.58,   86.58, 0, 0, True, 119529.54,  0, 0, 0, -1, 0)) #Disparity
    original_tasks_dram.append(SD_Task(jitter, 390.85, 390.85, 0, 0, True, 9272.38,    0, 0, 0, -1, 0)) #mser
    original_tasks_dram.append(SD_Task(jitter, 22.00,   22.00, 0, 0, True, 217454.01,  0, 0, 0, -1, 0)) #IDC
    original_tasks_dram.append(SD_Task(jitter, 10.60,   10.60, 0, 0, True, 5206.42,    0, 0, 0, -1, 0)) #FFT


    i = 0
    data_frame = pd.DataFrame(columns=['Utili', 'PREM', 'PREM-fixed slot', 'Contention'])
    data_frame.set_index('Utili')
    for util in util_cap:
        prem = 0
        prem_fixed = 0
        contention = 0

        for ts in xrange(total_number_of_task_sets):
            #generate a task set and apply the sched tests
            #adjust period according to the utilization and based on the measured WCET

            tasks_prem = []
            tasks_contention = []

            number_of_tasks = random.randint(1, len(original_tasks_bram))

            '''
            For the schedulability not to go down the drain, on each core the execution of the 
            shortest task should be longer than the maximum memory time (which again is 
            (DMA load + DMA unload) * TDMA_ROUND / TDMA_SLOT)

            Maximum memory time is around 2400, shortest WCET is 5206
            '''
            
            for j in xrange(number_of_tasks):
                task_index = random.randint(0, len(original_tasks_bram) - 1)
                period_bram = original_tasks_bram[task_index].cost / (float(util) / float(number_of_tasks))
                period_dram = original_tasks_dram[task_index].cost / (float(util) / float(number_of_tasks))

                tasks_prem.append(SD_Task(jitter, original_tasks_bram[task_index].input_load_time, original_tasks_bram[task_index].output_unload_time, 0, 0, True, original_tasks_bram[task_index].cost, period_bram, period_bram + j, period_bram, j, 0))
                tasks_contention.append(SD_Task(jitter, original_tasks_dram[task_index].input_load_time, original_tasks_dram[task_index].output_unload_time, 0, 0, True, original_tasks_dram[task_index].cost, period_dram, period_dram + j, period_dram, j, 0))

            sd_ts_prem = SD_TaskSystem(tasks_prem)
            sd_ts_contention = SD_TaskSystem(tasks_contention)
            #print "Task Set for util =", util, "tasks =", number_of_tasks
            #print sd_ts

            tmp, response_time_prem = sd_ts_prem.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            prem = prem + int(tmp)
            
            tmp, response_time_prem_fixed = sd_ts_prem.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            prem_fixed = prem_fixed + int(tmp)
            
            tmp, response_time_contention = sd_ts_contention.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            contention = contention + int(tmp)

            del tasks_prem
            del tasks_contention

        prem = prem / float(total_number_of_task_sets)
        prem_fixed = prem_fixed / float(total_number_of_task_sets)
        contention = contention / float(total_number_of_task_sets)

        data_frame.loc[i] = [util, prem, prem_fixed, contention]

        i = i + 1

    print data_frame

'''
The case study, where we basically test only one task set (or some number of task sets, but basically we have 
less variability). In this case we might as well forget about contention, and focus on looking at: 
1. schedulability vs the fixed slot case, which probably will do worse if we put the tasks properly on the 
core 2. response time vs jitter
'''
def case_study_on_anomaly_detection_original():
    n_cores = 3
    factor_M = 5
    tdma_round_lenght = 100 * n_cores
    tdma_slot_size =  100
    tdma_slot_size_wo_overhead = tdma_slot_size - (tdma_slot_size * 0.04) # DMA overhead is 3.98 us - 4% of the total

    BRAM_size = 1 * 1024 * 1024 #1 MB - big enough to fit the biggest detector (NFER - around 300KB) in one partition of 512 KB
    DMA_time_to_transfer_1_byte = 0.0009 #in us
    tdma_slot_size_fixed = BRAM_size / 2 * DMA_time_to_transfer_1_byte
    tdma_round_length_fixed = tdma_slot_size_fixed * 3

    #print "tdma_slot_size_fixed =", tdma_slot_size_fixed, "tdma_round_length_fixed =", tdma_round_length_fixed

    '''
                BRAM (us)	Code Size (bytes) Data Size (bytes) DMA Data+Code (us)
    Spike			1784	696	    6400	6.23
    Clipping/Loss	1008	336	    12800	11.54
    Level Change    1160.4	796	    7680	7.44
    Frequency		4169.6	2520	1912    3.89
    NFER			3185.6	15400	309664	285.45
    Voter		    300 	8223	1280	8.34
    '''

    cpu_0 = []
    cpu_1 = []
    cpu_2 = []

    jitter = 0
    period = 20000 # in us equals to 20ms
    period_voter = 15000 # in us

    #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
    #original data without varying utilization
    cpu_0.append(SD_Task(jitter, 6.23,   6.23, 0, 0, True,   1784,  period, 1, period, 0, 0)) #Spike
    cpu_0.append(SD_Task(jitter, 3.89,   3.89, 0, 0,  True, 4169.6,  period, 2, period, 1, 0)) #Frequency
    cpu_0.append(SD_Task(jitter, 11.54, 11.54, 0, 0, True, 1008, period, 3, period, 2, 1)) #Clip
    cpu_0.append(SD_Task(jitter, 7.44, 7.44, 0, 0, True, 1160.4, period, 4, period, 3, 1)) #Level

    sd_ts = SD_TaskSystem(cpu_0)
    print "\n*******CPU 0 (Spike, Frequency, Clip, Level) Total Util =", sd_ts.utilization(),"***********\n"
    schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
    print "PREM: schedulable =",schedulable, "response_time =", response_time

    schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
    print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time

    schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
    print "Contention: schedulable =",schedulable, "response_time =", response_time

    #Clip, Level, and Voter run on the same core
    cpu_1.append(SD_Task(jitter, 11.54, 11.54, 0, 0, True, 1008, period, 2, period, 0, 1)) #Clip
    cpu_1.append(SD_Task(jitter, 7.44, 7.44, 0, 0, True, 1160.4, period, 3, period, 1, 1)) #Level
    cpu_1.append(SD_Task(0, 8.34, 8.34, 0, 0, True, 300, period_voter, 1, period_voter, 2, 1)) #Voter
    cpu_1.append(SD_Task(jitter, 6.23,   6.23, 0, 0, True,   1784,  period, 4, period, 3, 0)) #Spike
    cpu_1.append(SD_Task(jitter, 7.44, 7.44, 0, 0, True, 1160.4, period, 5, period, 4, 1)) #Level
    cpu_1.append(SD_Task(jitter, 3.89,   3.89, 0, 0,  True, 4169.6,  period, 6, period, 5, 2)) #Frequency

    sd_ts = SD_TaskSystem(cpu_1)
    print "\n*******CPU 1 (Clip, Level, Voter, Spike, Level, Frequency) Total Util =", sd_ts.utilization(),"***********\n"

    schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
    print "PREM: schedulable =",schedulable, "response_time =", response_time

    schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
    print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time

    schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
    print "Contention: schedulable =",schedulable, "response_time =", response_time

    #Only Nfer run on the core

    cpu_2.append(SD_Task(jitter, 285.45, 285.45, 0, 0, True, 3185.6, period, 1, period, 0, 2)) #Nfer
    cpu_2.append(SD_Task(jitter, 6.23,   6.23, 0, 0, True,   1784,  period, 2, period, 1, 2)) #Spike
    cpu_2.append(SD_Task(jitter, 3.89,   3.89, 0, 0,  True, 4169.6,  period, 3, period, 2, 2)) #Frequency

    sd_ts = SD_TaskSystem(cpu_2)
    print "sd_ts\n", sd_ts

    print "\n*******CPU 2 (NFER, Spike, Frequency) Total Util =", sd_ts.utilization(),"***********\n"

    schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
    print "PREM: schedulable =",schedulable, "response_time =", response_time

    schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
    print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time

    schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
    print "Contention: schedulable =",schedulable, "response_time =", response_time


def find_max_response_time(response_time):
    max = -1
    for i in response_time:
        if response_time[i] > max:
            max = response_time[i]
    return max

'''
The case study, where we basically test only one task set (or some number of task sets, but basically we have 
less variability). In this case we might as well forget about contention, and focus on looking at: 
1. schedulability vs the fixed slot case, which probably will do worse if we put the tasks properly on the 
core 2. response time vs jitter
'''
def case_study_on_anomaly_detection():
    n_cores = 3
    factor_M = 1.47
    tdma_slot_size = 100
    tdma_round_lenght = tdma_slot_size * n_cores
    dma_programming_overhead = 3.98
    #tdma_slot_size_wo_overhead = tdma_slot_size - (tdma_slot_size * 0.04) # DMA overhead is 3.98 us - 4% of the total

    tdma_slot_size_wo_overhead = tdma_slot_size - dma_programming_overhead # DMA overhead is 3.98 us - 0.69% of the total
    
    BRAM_size = 1 * 1024 * 1024 #1 MB - big enough to fit the biggest detector (NFER - around 300KB) in one partition of 512 KB
    #DMA_time_to_transfer_1_byte = 0.0009 #in us
    DMA_time_to_transfer_1_byte = 0.001096516 #in us
    tdma_slot_size_fixed = BRAM_size / 2 * DMA_time_to_transfer_1_byte
    tdma_round_length_fixed = tdma_slot_size_fixed * 3

    #print "tdma_slot_size_fixed =", tdma_slot_size_fixed, "tdma_round_length_fixed =", tdma_round_length_fixed

    #Time to transfer data/code of NFER is 285.45, which is the longest task
    #tdma_slot_size_fixed_nfer = 285.45

    #Time to transfer data/code of NFER is 339.551, which is the longest task
    tdma_slot_size_fixed_nfer = 356.44
    
    tdma_round_length_fixed_nfer = tdma_slot_size_fixed_nfer * 3

    '''
    old DMA transfer times
                BRAM (us)	Code Size (bytes) Data Size (bytes) DMA Data+Code (us)
    Spike			1784	696	    6400	6.23
    Clipping/Loss	1008	336	    12800	11.54
    Level Change    1160.4	796	    7680	7.44
    Frequency		4169.6	2520	1912    3.89
    NFER			3185.6	15400	309664	285.45
    Voter		    300 	8223	1280	8.34

    spike_time = 6.23
    clip_time = 11.54
    level_time = 7.44
    frequency_time = 3.89
    nfer_time = 339.551
    voter_time = 8.34

    '''

    '''
    these are current DMA transfer values
                BRAM (us)	Code Size (bytes) Data Size (bytes) DMA Data+Code (us)
    Spike			1784	696	    6400	10.40
    Clipping/Loss	1008	336	    12800	16.80
    Level Change    1160.4	796	    7680	12.43
    Frequency		4169.6	2520	1912    7.87
    NFER			3185.6	15400	309664	356.44
    Voter		    300 	8223	1280	13.37
    '''

    spike_time = 10.40
    clip_time = 16.80
    level_time = 12.43
    frequency_time = 7.87
    nfer_time = 356.44
    voter_time = 13.37

    jitter = 0
    period = 20000 # in us equals to 20ms
    period_voter = 15000 # in us

    data_frame = pd.DataFrame(columns=['Channel', 'PREM', 'PREM-fixed slot', 'PREM-fixed slot nfer', 'Contention', 
                                    'PREM-CPU0', 'PREM-fixed slot-CPU0', 'PREM-fixed slot-CPU0 nfer', 'Contention-CPU0', 
                                    'PREM-CPU1', 'PREM-fixed slot-CPU1', 'PREM-fixed slot-CPU1 nfer', 'Contention-CPU1', 
                                    'PREM-CPU2', 'PREM-fixed slot-CPU2', 'PREM-fixed slot-CPU2 nfer', 'Contention-CPU2'])
    data_frame.set_index('Channel')

    #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
    period_factor = 4
    i = 0
    for channels in xrange(1, 6): #from 1 to 5 channels, varies the total utilization
        cpu_0 = []
        cpu_1 = []
        cpu_2 = []

        if(channels == 1):
            #CPU0 = 0.25888, CPU1 = 0.16722, CPU2 = 0.15928, Total = 0.58538
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 1, period * period_factor, 0, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 2, period * period_factor, 1, 0)) #Clip

            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 1)) #Voter
            cpu_1.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 2, period * period_factor, 1, 1)) #Spike
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 3, period * period_factor, 2, 1)) #Level

            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 1, period * period_factor, 0, 2)) #Nfer

            sd_ts = SD_TaskSystem(cpu_0)

            #print "\n*******CPU 0 (Spike, Frequency, Clip, Level) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed0_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention0 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_1)
            #print "\n*******CPU 1 (Clip, Level, Voter, Spike, Level, Frequency) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed1_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention1 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_2)
            #print "\n*******CPU 2 (NFER, Spike, Frequency) Total Util =", sd_ts.utilization(),"***********\n"

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed2_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention2 = find_max_response_time(response_time)

            max_resp_prem = max(max_resp_time_prem0, max_resp_time_prem1, max_resp_time_prem2)
            max_resp_time_prem_fixed = max(max_resp_time_prem_fixed0, max_resp_time_prem_fixed1, max_resp_time_prem_fixed2)
            max_resp_time_prem_fixed_nfer = max(max_resp_time_prem_fixed0_nfer, max_resp_time_prem_fixed1_nfer, max_resp_time_prem_fixed2_nfer)
            max_resp_time_contention = max(max_resp_time_contention0, max_resp_time_contention1, max_resp_time_contention2)

            #print "Max PREM =",max_resp_prem, "Max PREM-FIXED =",max_resp_time_prem_fixed, "Max Contention =",max_resp_time_contention
            data_frame.loc[i] = [channels, max_resp_prem, max_resp_time_prem_fixed, max_resp_time_prem_fixed_nfer, max_resp_time_contention,
                                max_resp_time_prem0,max_resp_time_prem_fixed0,max_resp_time_prem_fixed0_nfer,max_resp_time_contention0,
                                max_resp_time_prem1,max_resp_time_prem_fixed1,max_resp_time_prem_fixed1_nfer,max_resp_time_contention1,
                                max_resp_time_prem2,max_resp_time_prem_fixed2,max_resp_time_prem_fixed2_nfer,max_resp_time_contention2]

        if(channels == 2):
            '''
            CPU 0	0.31768	Frequency, Spike, Voter
            CPU 1	0.44532	Voter, Frequency, Clip, Clip, Level, Level
            CPU 2	0.40776	NFER,NFER, Spike
            Total	1.17076	
            '''
            cpu_0.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 0)) #Voter
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 2, period * period_factor, 1, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 3, period * period_factor, 2, 0)) #Spike
            

            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 1)) #Voter
            cpu_1.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 2, period * period_factor, 1, 1)) #Frequency
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 3, period * period_factor, 2, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 4, period * period_factor, 3, 1)) #Clip
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 5, period * period_factor, 4, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 6, period * period_factor, 5, 1)) #Level

            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 1, period * period_factor, 0, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 2, period * period_factor, 1, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 3, period * period_factor, 2, 2)) #Spike

            sd_ts = SD_TaskSystem(cpu_0)

            #print "\n*******CPU 0 (Spike, Frequency, Clip, Level) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed0_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention0 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_1)
            #print "\n*******CPU 1 (Clip, Level, Voter, Spike, Level, Frequency) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed1_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention1 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_2)
            #print "\n*******CPU 2 (NFER, Spike, Frequency) Total Util =", sd_ts.utilization(),"***********\n"

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed2_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention2 = find_max_response_time(response_time)

            max_resp_prem = max(max_resp_time_prem0, max_resp_time_prem1, max_resp_time_prem2)
            max_resp_time_prem_fixed = max(max_resp_time_prem_fixed0, max_resp_time_prem_fixed1, max_resp_time_prem_fixed2)
            max_resp_time_prem_fixed_nfer = max(max_resp_time_prem_fixed0_nfer, max_resp_time_prem_fixed1_nfer, max_resp_time_prem_fixed2_nfer)
            max_resp_time_contention = max(max_resp_time_contention0, max_resp_time_contention1, max_resp_time_contention2)

            #print "Max PREM =",max_resp_prem, "Max PREM-FIXED =",max_resp_time_prem_fixed, "Max Contention =",max_resp_time_contention
            data_frame.loc[i] = [channels, max_resp_prem, max_resp_time_prem_fixed, max_resp_time_prem_fixed_nfer, max_resp_time_contention,
                                max_resp_time_prem0,max_resp_time_prem_fixed0,max_resp_time_prem_fixed0_nfer,max_resp_time_contention0,
                                max_resp_time_prem1,max_resp_time_prem_fixed1,max_resp_time_prem_fixed1_nfer,max_resp_time_contention1,
                                max_resp_time_prem2,max_resp_time_prem_fixed2,max_resp_time_prem_fixed2_nfer,max_resp_time_contention2]
        if(channels == 3):
            '''
            CPU 0	0.52616	Voter, Frequency, Frequency, Spike
            CPU 1	0.57374	Voter, Voter, Frequency, Clip, Clip, Clip, Level, Level, Level
            CPU 2	0.65624	NFER,NFER, NFER, Spike, Spike
            Total	1.75614	
            '''
            cpu_0.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 0)) #Voter
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 2, period * period_factor, 1, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 3, period * period_factor, 2, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 4, period * period_factor, 3, 0)) #Spike
            

            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 1)) #Voter
            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 2, period_voter * period_factor, 1, 1)) #Voter
            cpu_1.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 3, period * period_factor, 2, 1)) #Frequency
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 4, period * period_factor, 3, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 5, period * period_factor, 4, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 6, period * period_factor, 5, 1)) #Clip
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 7, period * period_factor, 6, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 8, period * period_factor, 7, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 9, period * period_factor, 8, 1)) #Level

            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 1, period * period_factor, 0, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 2, period * period_factor, 1, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 3, period * period_factor, 2, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 4, period, 3, 2)) #Spike
            cpu_2.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 5, period * period_factor, 4, 2)) #Spike

            sd_ts = SD_TaskSystem(cpu_0)

            #print "\n*******CPU 0 (Spike, Frequency, Clip, Level) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed0_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention0 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_1)
            #print "\n*******CPU 1 (Clip, Level, Voter, Spike, Level, Frequency) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed1_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention1 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_2)
            #print "\n*******CPU 2 (NFER, Spike, Frequency) Total Util =", sd_ts.utilization(),"***********\n"

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed2_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention2 = find_max_response_time(response_time)

            max_resp_prem = max(max_resp_time_prem0, max_resp_time_prem1, max_resp_time_prem2)
            max_resp_time_prem_fixed = max(max_resp_time_prem_fixed0, max_resp_time_prem_fixed1, max_resp_time_prem_fixed2)
            max_resp_time_prem_fixed_nfer = max(max_resp_time_prem_fixed0_nfer, max_resp_time_prem_fixed1_nfer, max_resp_time_prem_fixed2_nfer)
            max_resp_time_contention = max(max_resp_time_contention0, max_resp_time_contention1, max_resp_time_contention2)

            #print "Max PREM =",max_resp_prem, "Max PREM-FIXED =",max_resp_time_prem_fixed, "Max Contention =",max_resp_time_contention
            data_frame.loc[i] = [channels, max_resp_prem, max_resp_time_prem_fixed, max_resp_time_prem_fixed_nfer, max_resp_time_contention,
                                max_resp_time_prem0,max_resp_time_prem_fixed0,max_resp_time_prem_fixed0_nfer,max_resp_time_contention0,
                                max_resp_time_prem1,max_resp_time_prem_fixed1,max_resp_time_prem_fixed1_nfer,max_resp_time_contention1,
                                max_resp_time_prem2,max_resp_time_prem_fixed2,max_resp_time_prem_fixed2_nfer,max_resp_time_contention2]

        if(channels == 4):
            '''
            Four Channels		
            CPU 0	0.75464	NFER, Frequency, Frequency, Spike, Spike
            CPU 1	0.72216	Voter, Voter, Voter, Voter, Frequency, Clip, Clip, Clip, Clip, Level, Level, Level, Level
            CPU 2	0.86472	NFER,NFER, NFER, Frequency, Spike, Spike
            Total	2.34152	
            '''
            cpu_0.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 1, period * period_factor, 0, 0)) #Nfer
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 2, period * period_factor, 1, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 3, period * period_factor, 2, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 4, period * period_factor, 3, 0)) #Spike
            cpu_0.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 5, period * period_factor, 4, 0)) #Spike

            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 1)) #Voter
            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 2, period_voter * period_factor, 1, 1)) #Voter
            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 3, period_voter * period_factor, 2, 1)) #Voter
            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 4, period_voter * period_factor, 3, 1)) #Voter
            cpu_1.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 5, period * period_factor, 4, 1)) #Frequency
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 6, period * period_factor, 5, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 7, period * period_factor, 6, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 8, period * period_factor, 7, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 9, period * period_factor, 8, 1)) #Clip
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 10, period * period_factor, 9, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 11, period * period_factor, 10, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 12, period * period_factor, 11, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 13, period * period_factor, 12, 1)) #Level

            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 1, period * period_factor, 0, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 2, period * period_factor, 1, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 3, period * period_factor, 2, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 4, period * period_factor, 3, 2)) #Frequency
            cpu_2.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 5, period * period_factor, 4, 2)) #Spike
            cpu_2.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 6, period * period_factor, 5, 2)) #Spike

            sd_ts = SD_TaskSystem(cpu_0)

            #print "\n*******CPU 0 (Spike, Frequency, Clip, Level) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed0_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention0 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_1)
            #print "\n*******CPU 1 (Clip, Level, Voter, Spike, Level, Frequency) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed1_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention1 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_2)
            #print "\n*******CPU 2 (NFER, Spike, Frequency) Total Util =", sd_ts.utilization(),"***********\n"

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed2_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention2 = find_max_response_time(response_time)

            max_resp_prem = max(max_resp_time_prem0, max_resp_time_prem1, max_resp_time_prem2)
            max_resp_time_prem_fixed = max(max_resp_time_prem_fixed0, max_resp_time_prem_fixed1, max_resp_time_prem_fixed2)
            max_resp_time_prem_fixed_nfer = max(max_resp_time_prem_fixed0_nfer, max_resp_time_prem_fixed1_nfer, max_resp_time_prem_fixed2_nfer)
            max_resp_time_contention = max(max_resp_time_contention0, max_resp_time_contention1, max_resp_time_contention2)

            #print max_resp_time_prem0, max_resp_time_prem1, max_resp_time_prem2
            #print "Max PREM =",max_resp_prem, "Max PREM-FIXED =",max_resp_time_prem_fixed, "Max Contention =",max_resp_time_contention
            data_frame.loc[i] = [channels, max_resp_prem, max_resp_time_prem_fixed, max_resp_time_prem_fixed_nfer, max_resp_time_contention,
                                max_resp_time_prem0,max_resp_time_prem_fixed0,max_resp_time_prem_fixed0_nfer,max_resp_time_contention0,
                                max_resp_time_prem1,max_resp_time_prem_fixed1,max_resp_time_prem_fixed1_nfer,max_resp_time_contention1,
                                max_resp_time_prem2,max_resp_time_prem_fixed2,max_resp_time_prem_fixed2_nfer,max_resp_time_contention2]

        if(channels == 5):
            '''
            Five Channels		
            CPU 0	0.98312	Voter, NFER, Frequency x 3, Spike x 2
            CPU 1	0.98898	Voter x 3, Frequency, Clip x 5, Level x 5, Spike x 2
            CPU 2	0.9548	Voter, NFER x 4, Frequency, Spike
            Total	2.9269
            '''
            cpu_0.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 0)) #Voter
            cpu_0.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 2, period * period_factor, 1, 0)) #Nfer
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 3, period * period_factor, 2, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 4, period * period_factor, 3, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 5, period * period_factor, 4, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 6, period * period_factor, 5, 0)) #Spike
            cpu_0.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 7, period * period_factor, 6, 0)) #Spike

            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 1)) #Voter
            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 2, period_voter * period_factor, 1, 1)) #Voter
            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 3, period_voter * period_factor, 2, 1)) #Voter
            cpu_1.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 4, period * period_factor, 3, 1)) #Frequency
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 5, period * period_factor, 4, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 6, period * period_factor, 5, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 7, period * period_factor, 6, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 8, period * period_factor, 7, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 9, period * period_factor, 8, 1)) #Clip
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 10, period * period_factor, 9, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 11, period * period_factor, 10, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 12, period * period_factor, 11, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 13, period * period_factor, 12, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 14, period * period_factor, 13, 1)) #Level
            cpu_1.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 15, period * period_factor, 14, 1)) #Spike
            cpu_1.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 16, period * period_factor, 15, 1)) #Spike

            cpu_2.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 2)) #Voter
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 2, period * period_factor, 1, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 3, period * period_factor, 2, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 4, period * period_factor, 3, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 5, period * period_factor, 4, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 6, period * period_factor, 5, 2)) #Frequency
            cpu_2.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 7, period * period_factor, 6, 2)) #Spike

            sd_ts = SD_TaskSystem(cpu_0)

            #print "\n*******CPU 0 (Spike, Frequency, Clip, Level) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed0_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention0 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_1)
            #print "\n*******CPU 1 (Clip, Level, Voter, Spike, Level, Frequency) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed1_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention1 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_2)
            #print "\n*******CPU 2 (NFER, Spike, Frequency) Total Util =", sd_ts.utilization(),"***********\n"

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed2_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention2 = find_max_response_time(response_time)

            max_resp_prem = max(max_resp_time_prem0, max_resp_time_prem1, max_resp_time_prem2)
            max_resp_time_prem_fixed = max(max_resp_time_prem_fixed0, max_resp_time_prem_fixed1, max_resp_time_prem_fixed2)
            max_resp_time_prem_fixed_nfer = max(max_resp_time_prem_fixed0_nfer, max_resp_time_prem_fixed1_nfer, max_resp_time_prem_fixed2_nfer)
            max_resp_time_contention = max(max_resp_time_contention0, max_resp_time_contention1, max_resp_time_contention2)

            #print "Max PREM =",max_resp_prem, "Max PREM-FIXED =",max_resp_time_prem_fixed, "Max Contention =",max_resp_time_contention
            data_frame.loc[i] = [channels, max_resp_prem, max_resp_time_prem_fixed, max_resp_time_prem_fixed_nfer, max_resp_time_contention,
                                max_resp_time_prem0,max_resp_time_prem_fixed0,max_resp_time_prem_fixed0_nfer,max_resp_time_contention0,
                                max_resp_time_prem1,max_resp_time_prem_fixed1,max_resp_time_prem_fixed1_nfer,max_resp_time_contention1,
                                max_resp_time_prem2,max_resp_time_prem_fixed2,max_resp_time_prem_fixed2_nfer,max_resp_time_contention2]

        if(channels == 6):
            '''
            Five Channels		
            CPU 0	0.98312	Voter, NFER, Frequency x 3, Spike x 2
            CPU 1	0.98898	Voter x 3, Frequency, Clip x 5, Level x 5, Spike x 2
            CPU 2	0.9548	Voter, NFER x 4, Frequency, Spike
            Total	2.9269
            '''
            cpu_0.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 0)) #Voter
            cpu_0.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 2, period * period_factor, 1, 0)) #Nfer
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 3, period * period_factor, 2, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 4, period * period_factor, 3, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 5, period * period_factor, 4, 0)) #Frequency
            cpu_0.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 6, period * period_factor, 5, 0)) #Spike
            cpu_0.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 7, period * period_factor, 6, 0)) #Spike

            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 1)) #Voter
            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 2, period_voter * period_factor, 1, 1)) #Voter
            cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 3, period_voter * period_factor, 2, 1)) #Voter
            cpu_1.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 4, period * period_factor, 3, 1)) #Frequency
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 5, period * period_factor, 4, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 6, period * period_factor, 5, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 7, period * period_factor, 6, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 8, period * period_factor, 7, 1)) #Clip
            cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, 9, period * period_factor, 8, 1)) #Clip
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 10, period * period_factor, 9, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 11, period * period_factor, 10, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 12, period * period_factor, 11, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 13, period * period_factor, 12, 1)) #Level
            cpu_1.append(SD_Task(jitter, level_time, level_time, 0, 0, True, 1160.4, period, 14, period * period_factor, 13, 1)) #Level
            cpu_1.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 15, period * period_factor, 14, 1)) #Spike
            cpu_1.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,  period, 16, period * period_factor, 15, 1)) #Spike

            cpu_2.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period_voter, 1, period_voter * period_factor, 0, 2)) #Voter
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 2, period * period_factor, 1, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 3, period * period_factor, 2, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 4, period * period_factor, 3, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, 5, period * period_factor, 4, 2)) #Nfer
            cpu_2.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 6, period * period_factor, 5, 2)) #Frequency
            cpu_2.append(SD_Task(jitter, frequency_time,   frequency_time, 0, 0,  True, 4169.6,  period, 7, period * period_factor, 6, 2)) #Frequency
            cpu_2.append(SD_Task(jitter, spike_time,   spike_time, 0, 0, True,   1784,   period, 8, period * period_factor, 7, 2)) #Spike
            #CPU 2	1.16328
            #Total	3.13538

            sd_ts = SD_TaskSystem(cpu_0)

            #print "\n*******CPU 0 (Spike, Frequency, Clip, Level) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed0 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed0_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention0 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_1)
            #print "\n*******CPU 1 (Clip, Level, Voter, Spike, Level, Frequency) Total Util =", sd_ts.utilization(),"***********\n"
            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed1 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed1_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention1 = find_max_response_time(response_time)

            sd_ts = SD_TaskSystem(cpu_2)
            print "Task set util = ", sd_ts.utilization()
            print sd_ts
            #print "\n*******CPU 2 (NFER, Spike, Frequency) Total Util =", sd_ts.utilization(),"***********\n"

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
            #print "PREM: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
            #print "PREM-Fixed: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_prem_fixed2 = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)
            max_resp_time_prem_fixed2_nfer = find_max_response_time(response_time)

            schedulable, response_time = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
            #print "Contention: schedulable =",schedulable, "response_time =", response_time
            max_resp_time_contention2 = find_max_response_time(response_time)

            max_resp_prem = max(max_resp_time_prem0, max_resp_time_prem1, max_resp_time_prem2)
            max_resp_time_prem_fixed = max(max_resp_time_prem_fixed0, max_resp_time_prem_fixed1, max_resp_time_prem_fixed2)
            max_resp_time_prem_fixed_nfer = max(max_resp_time_prem_fixed0_nfer, max_resp_time_prem_fixed1_nfer, max_resp_time_prem_fixed2_nfer)
            max_resp_time_contention = max(max_resp_time_contention0, max_resp_time_contention1, max_resp_time_contention2)

            #print "Max PREM =",max_resp_prem, "Max PREM-FIXED =",max_resp_time_prem_fixed, "Max Contention =",max_resp_time_contention
            data_frame.loc[i] = [channels, max_resp_prem, max_resp_time_prem_fixed, max_resp_time_prem_fixed_nfer, max_resp_time_contention,
                                max_resp_time_prem0,max_resp_time_prem_fixed0,max_resp_time_prem_fixed0_nfer,max_resp_time_contention0,
                                max_resp_time_prem1,max_resp_time_prem_fixed1,max_resp_time_prem_fixed1_nfer,max_resp_time_contention1,
                                max_resp_time_prem2,max_resp_time_prem_fixed2,max_resp_time_prem_fixed2_nfer,max_resp_time_contention2]

        i = i + 1

        del cpu_0, cpu_1, cpu_2

    print data_frame
    data_frame.to_csv("graphs/results/case_study_channels.csv", sep=' ')

def case_study_on_anomaly_detection_util():

    jitter = 0

    n_cores = 3
    factor_M = 2.26
    tdma_round_lenght = 100 * n_cores
    tdma_slot_size =  100
    dma_programming_overhead = 3.98
    #tdma_slot_size_wo_overhead = tdma_slot_size - (tdma_slot_size * 0.04) # DMA overhead is 3.98 us - 4% of the total

    tdma_slot_size_wo_overhead = tdma_slot_size - dma_programming_overhead # DMA overhead is 3.98 us - 0.69% of the total

    BRAM_size = 1 * 1024 * 1024 #1 MB - big enough to fit the biggest detector (NFER - around 300KB) in one partition of 512 KB
    #DMA_time_to_transfer_1_byte = 0.000878143 #in us
    DMA_time_to_transfer_1_byte = 0.001096516 #in us
    tdma_slot_size_fixed = BRAM_size / 2 * DMA_time_to_transfer_1_byte
    tdma_round_length_fixed = tdma_slot_size_fixed * 3

    #Time to transfer data/code of NFER is 285.45, which is the longest task
    #tdma_slot_size_fixed_nfer = 285.45

    #Time to transfer data/code of NFER is 339.551, which is the longest task
    tdma_slot_size_fixed_nfer = 339.551
    tdma_round_length_fixed_nfer = tdma_slot_size_fixed_nfer * 3

    #print "tdma_slot_size_fixed =", tdma_slot_size_fixed, "tdma_round_length_fixed =", tdma_round_length_fixed

    util_cap = pl.frange(0.1, 1.0, 0.1)

    '''
    these are the old DMA transfer values
                BRAM (us)	Code Size (bytes) Data Size (bytes) DMA Data+Code (us)
    Spike			1784	696	    6400	6.23
    Clipping/Loss	1008	336	    12800	11.54
    Level Change    1160.4	796	    7680	7.44
    Frequency		4169.6	2520	1912    3.89
    NFER			3185.6	15400	309664	285.45
    Voter		    300 	8223	1280	8.34
    '''

    '''
    these are current DMA transfer values
                BRAM (us)	Code Size (bytes) Data Size (bytes) DMA Data+Code (us)
    Spike			1784	696	    6400	6.23
    Clipping/Loss	1008	336	    12800	11.54
    Level Change    1160.4	796	    7680	7.44
    Frequency		4169.6	2520	1912    3.89
    NFER			3185.6	15400	309664	339.551
    Voter		    300 	8223	1280	8.34
    '''

    spike_time = 6.23
    clip_time = 11.54
    level_time = 7.44
    frequency_time = 3.89
    nfer_time = 339.551
    voter_time = 8.34

    data_frame = pd.DataFrame(columns=['Utili', 'PREM-CPU0', 'PREM-fixed-CPU0', 'PREM-fixed-nfer-CPU0','Contention-CPU0', 'PREM-CPU1', 'PREM-fixed-CPU1', 'PREM-fixed-nfer-CPU1','Contention-CPU1', 'PREM-CPU2', 'PREM-fixed-CPU2', 'PREM-fixed-nfer-CPU2', 'Contention-CPU2'])
    data_frame.set_index('Utili')

    i = 0
    for util in util_cap:

        cpu_0 = []
        cpu_1 = []
        cpu_2 = []

        #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
        #IDs are defined by assing_ids call, later in the code
        #Spike and Frequency run on the same core
        period = 1784 / (float(util) / 2.0) #divided by 2 because there are two tasks
        cpu_0.append(SD_Task(jitter, spike_time, spike_time, 0, 0, True,   1784,  period, period, period, 0, 0)) #Spike
        period = 8339.2 / (float(util) / 2.0)
        cpu_0.append(SD_Task(jitter, frequency_time, frequency_time, 0, 0, True, 4169.6,  period, period, period, 1, 0)) #Frequency

        #print "\n*******CPU 0 (Spike, Frequency) ***********\n"

        sd_ts = SD_TaskSystem(cpu_0)
        prem_cpu0, response_time_prem_cpu0 = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
        #print "PREM: schedulable =",prem_cpu0, "response_time =", response_time_prem_cpu0

        prem_fixed_cpu0, response_time_fixed_cpu0 = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
        #print "PREM-Fixed: schedulable =",prem_fixed_cpu0, "response_time =", response_time_fixed_cpu0

        prem_fixed_cpu0_nfer, response_time_fixed_cpu0_nfer = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)

        contention_cpu0, response_time_contention_cpu0 = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
        #print "Contention: schedulable =",contention_cpu0, "response_time =", response_time_contention_cpu0

        #Clip, Level, and Voter run on the same core
        period = 1008 / (float(util) / 3.0)
        cpu_1.append(SD_Task(jitter, clip_time, clip_time, 0, 0, True, 1008, period, period, period, 0, 1)) #Clip
        period = 1160.4 / (float(util) / 3.0)
        cpu_1.append(SD_Task(jitter, level_time,  level_time, 0, 0,  True, 1160.4, period, period, period, 1, 1)) #Level
        period = 300 / (float(util) / 3.0)
        cpu_1.append(SD_Task(0, voter_time, voter_time, 0, 0, True, 300, period, period, period, 2, 1)) #Voter

        #print "\n*******CPU 1 (Clip, Level, Voter) ***********\n"

        sd_ts = SD_TaskSystem(cpu_1)
        prem_cpu1, response_time_prem_cpu1 = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
        #print "PREM: schedulable =",prem_cpu1, "response_time =", response_time_prem_cpu1

        prem_fixed_cpu1, response_time_fixed_cpu1 = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
        #print "PREM-Fixed: schedulable =",prem_fixed_cpu1, "response_time =", response_time_fixed_cpu1

        prem_fixed_cpu1_nfer, response_time_fixed_cpu1_nfer = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)

        contention_cpu1, response_time_contention_cpu1 = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
        #print "Contention: schedulable =",contention_cpu1, "response_time =", response_time_contention_cpu1
        
        #Only Nfer run on the core
        period = 3185.6 / float(util)
        cpu_2.append(SD_Task(jitter, nfer_time, nfer_time, 0, 0, True, 3185.6, period, period, period, 0, 2)) #Nfer

        #print "\n*******CPU 2 (NFER) ***********\n"

        sd_ts = SD_TaskSystem(cpu_2)
        prem_cpu2, response_time_prem_cpu2 = sd_ts.No_streaming_schedulability_test_base_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght)
        #print "PREM: schedulable =",prem_cpu2, "response_time =", response_time_pre_cpu2

        prem_fixed_cpu2, response_time_fixed_cpu2 = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed, tdma_round_length_fixed)
        #print "PREM-Fixed: schedulable =",prem_fixed_cpu2, "response_time =", response_time_fixed_cpu2

        prem_fixed_cpu2_nfer, response_time_fixed_cpu2_nfer = sd_ts.No_streaming_schedulability_test_base_fixed_response_time(tdma_slot_size_fixed_nfer, tdma_round_length_fixed_nfer)

        contention_cpu2, response_time_contention_cpu2 = sd_ts.Contention_no_streaming_schedulability_test_response_time(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
        #print "Contention: schedulable =",contention_cpu2, "response_time =", response_time_contention_cpu2

        data_frame.loc[i] = [util, int(prem_cpu0), int(prem_fixed_cpu0), int(prem_fixed_cpu0_nfer), int(contention_cpu0), 
        int(prem_cpu1), int(prem_fixed_cpu1), int(prem_fixed_cpu1_nfer), int(contention_cpu1), 
        int(prem_cpu2), int(prem_fixed_cpu2), int(prem_fixed_cpu2_nfer), int(contention_cpu2)]

        i = i + 1
        del cpu_0
        del cpu_1
        del cpu_2
    
    print data_frame
    data_frame.to_csv("graphs/results/case_study_util.csv", sep=' ')

parser = argparse.ArgumentParser()

parser.add_argument("-g", "--generate", help="Generate task sets", action="store_true")
parser.add_argument("-s", "--schedulability_tests", help="Apply the schedulability tests", action="store_true")
parser.add_argument("-hs", "--has_streaming", help="Generate task sets with streaming tasks", action="store_true")
parser.add_argument("-r", "--read_file", help="Read a file that contains a task set and print the task set. Pass the entire path to the file. Ex: -r generated_ts_ecrts2019/sd_ts_util-uni-light_fixed-cost-2_dma-0.1_cap-1.0_1", type=str)
parser.add_argument("-e", "--eembc_benchmarks", help="Apply the schedulability tests using the EEMBC benchmarks", action="store_true")
parser.add_argument("-c", "--case_study", help="Apply the schedulability tests on the ECRTS 2019 case study", action="store_true")
parser.add_argument("-cu", "--case_study_util", help="Apply the schedulability tests on the ECRTS 2019 case study varying utilization", action="store_true")
parser.add_argument("-b", "--sdvbs_benchmarks", help="Apply the schedulability tests on the ECRTS 2019 benchmarks (SD VBS/EEMBC)", action="store_true")


args = parser.parse_args()

def main():
    ''' 
    tdma_slot_size is the size of the TDMA in the core under analysis
    tdma_slot_size_wo_overhead is the size of the TDMA in the core under analysis without overhead
    tdma_round_lenght is the lenght of the complete TDMA round
    n_cores is the number of cores available in the platform
    factor_M - under contention, each memory request takes M times to complete
    '''
    # not in use
    #tdma_slot_size = 5
    #tdma_slot_size_wo_overhead = 4
    #tdma_round_lenght = tdma_slot_size * n_cores

    #test_SD_ts()
    
    global n_cores, factor_M, number_of_tasksets

    if args.read_file != None:
        ts = read_sd_ts_from_file(args.read_file)
        ts.print_ts()
        exit()

    if args.generate:
        #ts = generate_sd_taskset_files(number_of_tasksets, utilization_cap, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, args.has_streaming)
        if (os.path.isdir("./generated_ts_ecrts2019") == False):
            print "Directory generated_ts_ecrts2019 does not exist! Creating.."
            os.makedirs("./generated_ts_ecrts2019")
        else:
            print "Directory generated_ts_ecrts2019 exists, task sets will be overwritten!"
        ts = generate_sd_taskset_files(number_of_tasksets, args.has_streaming)
        print "Generated",len(ts),"task sets"

    if args.schedulability_tests:
        #apply_sched_tests_ecrts2019(number_of_tasksets, utilization_cap, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
        apply_sched_tests_ecrts2019(number_of_tasksets, factor_M)

    if args.eembc_benchmarks:
        eembc_and_streaming_benchmarks()

    if args.case_study:
        case_study_on_anomaly_detection()

    if args.case_study_util:
        case_study_on_anomaly_detection_util()

    if args.sdvbs_benchmarks:
        benchmark_evaluation()

    if args.generate == False and args.schedulability_tests == False and args.read_file == None and args.eembc_benchmarks == None and args.case_study == None and args.sdvbs_benchmarks == None:
        print "None parameter has been passed. Read the help (-h)."

if __name__ == "__main__":
    main()
