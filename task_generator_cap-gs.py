import os
import random
from schedcat.generator.tasksets import mkgen
import schedcat.generator.tasks as gen
import schedcat.mapping.binpack as bp
from schedcat.mapping.rollback import Bin
from capgs_utils import *

NUMBER_OF_PROCESSORS = 8

PERIODS = {
# Periods used by HRT and SRT tasks, values in ms
    'hrt'     	: gen.uniform_choice([25, 50, 75]),
    'srt'  		: gen.uniform_choice([100, 150, 200, 500]),
}

UTILIZATIONS = {
# Utilizations used by HRT and SRT tasks, values in ms
    'hrt'       : gen.uniform(0.2 , 0.35),
    'srt'    	: gen.uniform(0.1 , 0.30),
}

UTILIZATION_BOUNDS = {
# Defines the minimum and maximum bounds for HRT tasks
	'hrt_min'	: 2.1,
	'hrt_max'	: 2.5,
# Defines the minimum and maximum bounds for SRT task groups
	'srt_grp_min'	: 0.6,
	'srt_grp_max'	: 0.7,
}

BE_TASKS = {
# Defines the distribution for generating the processor in which a BE task is assigned to
	'be' 		: gen.uniform_int(0, NUMBER_OF_PROCESSORS-1),
}

# number_of_tasksets = total number of task sets that will be generated
# with_be_tasks = a boolean informing whether best-effort (BE) tasks should be generated or not
# util_cap = the utilization cap for the bin packing heuristic. For instance, if the scheduler is
# RM, the cap will be 0.69
def generate_tasksets_capgs(number_of_tasksets, with_BE_tasks, util_cap):	
	generated_sets = []
	i = 0
	while i < number_of_tasksets:
		print "********Generating task set %d" % i		
		tasks = []
		g = gen.TaskGenerator(PERIODS['hrt'], UTILIZATIONS['hrt'])
		hrt_tasks = g.tasks(None, UTILIZATION_BOUNDS['hrt_max'], False, ms2us)		
		util = 0.0		
		for task in hrt_tasks:
			#group, task_type, exec_cost, period, deadline, id, cpu
			tasks.append(CAPGS_Task(-1, 0, task.cost, task.period, None, None, -1))
			util += task.utilization()
			#print "Task period %d wcet %d utilization %.4f" % (task.period, task.cost, task.utilization())
		#print "HRT utilization = %.2f" % (util)

		groups = int(((0.69 * NUMBER_OF_PROCESSORS) - util) / 0.69)
		#print "Number of groups = %d" % (groups)

		for j in range(groups):
			#print "Generating tasks for the SRT group %d" % (j) 
			g = gen.TaskGenerator(PERIODS['srt'], UTILIZATIONS['srt'])
			srt_tasks = g.tasks(None, UTILIZATION_BOUNDS['srt_grp_max'], True, ms2us)		
			util = 0.0		
			for task in srt_tasks:
				t = CAPGS_Task(j+1, 1, task.cost, task.period, None, None, -1)
				tasks.append(t)
				util += task.utilization()
				#print "Task period %d wcet %d utilization %.4f cpu %d" % (task.period, task.cost, task.utilization(), t.partition)

			#print "SRT utilization = %.2f" % (util)
			if(with_BE_tasks == True):
				tasks.append(CAPGS_Task(j+1, 2, 0, 1, 0, None, BE_TASKS['be']()))

		capgs_ts = CAPGS_TaskSystem(tasks)
		capgs_ts.assign_ids()
		#print "CAPGS TS utilization = %.2f" % (capgs_ts.utilization())
		#print "****Printing original TS.."
		#capgs_ts.print_ts()

		bfd_ts = capgs_ts.copy()
		
		capgs_set = cap_gs_task_partitioning(capgs_ts, util_cap, NUMBER_OF_PROCESSORS)

		if capgs_set == 0:
			continue

		sets = bp.best_fit_decreasing(bfd_ts, NUMBER_OF_PROCESSORS, util_cap, utilization, misfit)
		
		c = 0
		for s in sets:
			for t in s:
				if t.task_type != 2:
					t.partition = c
				#print t
			c = c + 1

		filename = "{0}_{1}".format("capgs_test", i)
		capgs_write(capgs_ts, filename)
		generated_sets.append(filename)
		filename = "{0}_{1}".format("bfd_test", i)
		capgs_write(bfd_ts, filename)
		generated_sets.append(filename)
		i = i + 1

		del capgs_set
		del bfd_ts
		del tasks

	return generated_sets
	
def test_bin_pack():
	t = [0.3258, 0.3112, 0.2834, 0.2864, 0.2416, 0.2486, 0.2292, 0.2025, 0.2573, 0.1774, 0.2113, 0.2994, 0.1417, 0.2769, 0.2570, 0.2345, 0.1795, 0.2782, 0.2496, 0.2915]

	tasks = []

	#group, task_type, exec_cost, period, deadline=None, id=None
	#HRT tasks
	tasks.append(CAPGS_Task(0, 0, 16292, 50000, None, 1))
	tasks.append(CAPGS_Task(0, 0, 23341, 75000, None, 2))
	tasks.append(CAPGS_Task(0, 0, 14172, 50000, None, 3))
	tasks.append(CAPGS_Task(0, 0, 7160,  25000, None, 4))
	tasks.append(CAPGS_Task(0, 0, 12082, 50000, None, 5))
	tasks.append(CAPGS_Task(0, 0, 6214,  25000, None, 6))
	tasks.append(CAPGS_Task(0, 0, 17187, 75000, None, 7))
	tasks.append(CAPGS_Task(0, 0, 5062,  25000, None, 8))
	tasks.append(CAPGS_Task(0, 0, 19295, 75000, None, 9))

	#SRT tasks group 1
	tasks.append(CAPGS_Task(1, 1, 17736, 100000, None, 10))
	tasks.append(CAPGS_Task(1, 1, 21132, 100000, None, 11))
	tasks.append(CAPGS_Task(1, 1, 44905, 150000, None, 12))
	tasks.append(CAPGS_Task(1, 2, 0, 0, 0, 21, BE_TASKS['be']()))

	#SRT tasks group 2
	tasks.append(CAPGS_Task(2, 1, 14168, 100000, None, 13))
	tasks.append(CAPGS_Task(2, 1, 27687, 100000, None, 14))
	tasks.append(CAPGS_Task(2, 1, 51407, 200000, None, 15))
	tasks.append(CAPGS_Task(2, 2, 0, 0, 0, 22, BE_TASKS['be']()))

	#SRT tasks group 3
	tasks.append(CAPGS_Task(3, 1, 35171, 150000, None, 16))
	tasks.append(CAPGS_Task(3, 1, 17955, 100000, None, 17))
	tasks.append(CAPGS_Task(3, 1, 27816, 100000, None, 18))
	tasks.append(CAPGS_Task(3, 2, 0, 0, 0, 23, BE_TASKS['be']()))

	#SRT tasks group 4
	tasks.append(CAPGS_Task(4, 1, 124788, 500000, None, 19))
	tasks.append(CAPGS_Task(4, 1, 29148, 100000, None, 20))
	tasks.append(CAPGS_Task(4, 2, 0, 0, 0, 24, BE_TASKS['be']()))

	capgs_ts = CAPGS_TaskSystem(tasks)
	
	capgs_set = cap_gs_task_partitioning(capgs_ts, 0.69, NUMBER_OF_PROCESSORS)
	if capgs_set != 0:
		print capgs_set
		filename = "{0}".format("capgs_test")
		capgs_write(capgs_ts, filename)

ts = generate_tasksets_capgs(10, True, 0.69)
print ts
#test_bin_pack()

