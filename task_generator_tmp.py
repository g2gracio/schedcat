import os
import random
import schedcat.generator.tasks as gen
import schedcat.mapping.binpack as bp
from schedcat.mapping.rollback import Bin

from schedcat.model.serialize import write
from schedcat.generator.tasksets import mkgen #, \
                                    #    NAMED_UTILIZATIONS, \
                                    #    NAMED_PERIODS
from schedcat.util.time import ms2us
import schedcat.model.resources as resources

from rtas2019_utils import *

import pandas as pd
import pylab as pl
import argparse

NAMED_PERIODS = {
# Named period distributions used in several UNC papers, in milliseconds.
    'uni-short'     : gen.uniform_int( 3,  33),
    'uni-moderate'  : gen.uniform_int(10, 100),
    'uni-long'      : gen.uniform_int(50, 250),

    'log-uni-short'     : gen.log_uniform_int( 3,  33),
    'log-uni-moderate'  : gen.log_uniform_int(10, 100),
    'log-uni-long'      : gen.log_uniform_int(50, 250),
}

NAMED_UTILIZATIONS = {
# Named utilization distributions used in several UNC papers, in milliseconds.
    'uni-light'     : gen.uniform(0.01, 0.2), #(0.001, 0.1)
    'uni-light-2'     : gen.uniform(0.01, 0.1),
    'uni-medium'    : gen.uniform(0.1  , 0.4), 
    'uni-heavy'     : gen.uniform(0.5  , 0.9),

    'exp-light'     : gen.exponential(0, 1, 0.10),'bimo-light'    : gen.multimodal([(gen.uniform(0.001, 0.5), 8),
                                      (gen.uniform(0.5  , 0.9), 1)]),
    'bimo-medium'   : gen.multimodal([(gen.uniform(0.001, 0.5), 6),
                                      (gen.uniform(0.5  , 0.9), 3)]),
    'bimo-heavy'    : gen.multimodal([(gen.uniform(0.001, 0.5), 4),
                                      (gen.uniform(0.5  , 0.9), 5)]),
    'exp-medium'    : gen.exponential(0, 1, 0.25),
    'exp-heavy'     : gen.exponential(0, 1, 0.50),
}

def generate_taskset_files(util_name, period_name, cap, number):
    generator = mkgen(NAMED_UTILIZATIONS[util_name],
                      NAMED_PERIODS[period_name])
    generated_sets = []
    for i in range(number):
        taskset = generator(max_util=cap, time_conversion=ms2us)
        filename = "{0}_{1}_{2}_{3}".format(util_name,
                                            period_name, cap, i)
        #write(taskset, filename)
        #generated_sets.append(filename)
        generated_sets.append(taskset)
    return generated_sets

#cost_factor = [0.5, 1.0, 5.0, 10.0, 50.0 ]
#util_name = ["uni-light", "uni-medium", "uni-heavy", "bimo-light", "bimo-medium", "bimo-heavy"]

fixed_cost = 2 #in ms - it is automatically converted to us in "g.sd_tasks_fixed_cost()"
util_name = ["uni-light"]
dma_name = [0.1, 0.2, 0.5, 1.0, 1.5] #percentage of the task's cost - take the percentage divide by 3 and divide by 4
jitter = 0.1 # percentage of the task's deadline

#def generate_sd_taskset_files(number_of_tasksets, cap, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, has_streaming_task):
def generate_sd_taskset_files(number_of_tasksets, cap, has_streaming_task):
    generated_sets = []

    ''' 
    tdma_slot_size is the size of the TDMA in the core under analysis
    tdma_slot_size_wo_overhead is the size of the TDMA in the core under analysis without overhead
    tdma_round_lenght is the lenght of the complete TDMA round
    factor_M - under contention, each memory request takes M times to complete
    '''

    #global cost_factor, util_name, dma_name, jitter
    global fixed_cost, util_name, dma_name, jitter

    for util in util_name:
        #for period in period_name:
        #for cost in cost_factor:
        for dma in dma_name:
            for ts_utilization_bound in pl.frange(0.1, cap, 0.1):
                for n_task_sets in xrange(number_of_tasksets):
                    tasks = []
                    #hrt_tasks = g.tasks(None, ts_utilization_bound, True, ms2us)
                    if ts_utilization_bound <= 0.1:
                        # this is necessary for the utilization cap of 0.1, otherwise the first task may 
                        # have utilization higher than 0.1 and the task set will be empty
                        g = gen.TaskGenerator(None, NAMED_UTILIZATIONS['uni-light-2']) 
                    else:
                        g = gen.TaskGenerator(None, NAMED_UTILIZATIONS[util])
                        
                    #hrt_tasks = g.sd_tasks_cost_factor(cost, tdma_slot_size, None, ts_utilization_bound, True, ms2us)
                    hrt_tasks = g.sd_tasks_fixed_cost(fixed_cost, None, ts_utilization_bound, False, ms2us)
                    
                    streaming_task = 0
                    max  = -1.0
                    generated_tasks = []

                    for t in hrt_tasks:
                        generated_tasks.append(t)
                        if t.utilization() > max:
                            max = t.utilization()
                            streaming_task = t

                    #print "hrt_tasks", len(generated_tasks)
                    task_id = 0
                    for task in generated_tasks:
                        #print "Task =", task

                        arrival_curve_jitter = 0.0
                        input_load_time = 0.0
                        output_unload_time = 0.0
                        load_time = 0.0
                        unload_time = 0.0  

                        if has_streaming_task == True:
                            
                            if task == streaming_task:
                                #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
                                #generate DMA parameters here
                                arrival_curve_jitter = task.deadline * jitter
                                task.deadline = task.period * 2

                        slot_time = (task.cost * float(dma)) / 3 / 4
                        #print "slot_time =", slot_time,"us" 
                                                
                        input_load_time = slot_time
                        output_unload_time = slot_time
                        load_time = slot_time
                        unload_time = slot_time
                        
                        '''
                        input_load_time = (task.cost * float(dma)) / 4 
                        output_unload_time = (task.cost * float(dma)) / 4
                        load_time = (task.cost * float(dma)) / 4
                        unload_time = (task.cost * float(dma)) / 4
                        '''
                        
                        t = SD_Task(arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, True, task.cost, task.period, task.deadline, task.deadline, task_id, 0)
                        
                        tasks.append(t)

                        task_id = task_id + 1
                        
                    sd_ts = SD_TaskSystem(tasks)
                    #print "Generated ts:", sd_ts
                    #filename = "generated_ts_rtas2019/{0}_cost-{1}_dma-{2}_cap-{3}_{4}_{5}".format("sd_ts", util, cost, dma, ts_utilization_bound, n_task_sets)
                    filename = "generated_ts_rtas2019/{0}_util-{1}_fixed-cost-{2}_dma-{3}_cap-{4}_{5}".format("sd_ts", util, str(fixed_cost), dma, ts_utilization_bound, n_task_sets)
                    sd_write(sd_ts, filename)
                    generated_sets.append(filename)
                    #exit()
                    del sd_ts
                    del tasks

    return generated_sets

def utilization(task):
	#print "period = %d, wcet = %d, util = %.4f" % (task.period, task.cost, task.utilization())
	return task.utilization()

bin_packing_error = False

def misfit(task):
    global bin_packing_error
    bin_packing_error = True
    print "Not able to partition the task set - Task utilization %.4f" % (task.utilization())

def task_set_partitioning(ts):
    #for x in ts[0]:
    #    print("Printing task")
    #    print(x)

    bin_packing_error = False

    print "Task set utilization = %.2f" % ts[0].utilization()
    sets = bp.best_fit_decreasing(ts[0], 4, 1, utilization, misfit)

    if bin_packing_error == False:
        for i in sets:
            print(i)
    else:
        print "BDF was not able to partition the TS"

def test_SD_ts():
    tasks = []
    #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
    
    #task set is schedulable
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(800, 1, 1, 1, 1, True, 100, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))

    '''
    #task set is schedulable
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(1800, 1, 1, 1, 1, True, 100, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))
    '''

    '''
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(1800, 1, 1, 1, 1, True, 100, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 2, 1, 2, 1, True,  10, 1000, 3, 1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True,  10, 1000, 4,  1000, 3, 0))
    '''

    '''
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(1800, 1, 1, 1, 1, True, 10, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 2, 1, 2, 1, True,  10, 1000, 3, 1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True,  10, 1000, 4,  1000, 3, 0))
    '''

    '''
    # this tests Hfinal_i_k
    tasks.append(SD_Task(0, 0, 0, 7, 7, True, 50, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(2000, 0, 0, 10, 10, True, 50, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 0, 0, 5, 5, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 0, 0, 1, 1, True, 100, 1000, 4,  1000, 3, 0))
    '''

    '''
    # this tests Hcons_i_k
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 50, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(2000, 1, 1, 1, 1, True, 10, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 1, 2, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))
    '''

    '''
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 50, 1000, 1,  1000, 0, 0))
    tasks.append(SD_Task(3000, 1, 1, 1, 1, True, 11, 1000, 2,  1000, 1, 0))
    tasks.append(SD_Task(0, 1, 2, 1, 1, True, 100, 1000, 3,1000, 2, 0))
    tasks.append(SD_Task(0, 1, 1, 1, 1, True, 100, 1000, 4,  1000, 3, 0))
    '''

    sd_ts = SD_TaskSystem(tasks)

    '''
    schedulable = sd_ts.Saud_schedulability_test()

    if schedulable == True:
        print "Task set is schedulable by Saud's algorithm"
    else:
        print "Task set is NOT schedulable by Saud's algorithm"
        print "\n\n******************************\n\n"
    '''
    
    #tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length
    schedulable = sd_ts.SPM_Stream_Schedulability_test(1, 1, 3)

    if schedulable == True:
        print "Task set is schedulable by SPM Stream test"
    else:
        print "Task set is NOT schedulable by SPM Stream test"
    
    print "\n\n-------------------No Contention-----------\n\n"
    schedulable = sd_ts.Contention_schedulability_test(1, 1, 3, 3)

    if schedulable == True:
        print "Task set is schedulable by Contention test"
    else:
        print "Task set is NOT schedulable by Contention test"

    print "\n\n-------------------No Streaming-----------\n\n"
    schedulable = sd_ts.No_streaming_schedulability_test(1, 1, 3)

    if schedulable == True:
        print "Task set is schedulable by No Streaming test"
    else:
        print "Task set is NOT schedulable by No Streaming test"

    #for task in sd_ts:
    #    print(task)

#ts = generate_taskset_files('uni-medium', 'uni-moderate', 4, 1)
#print(ts)
#task_set_partitioning(ts)

#def apply_sched_tests_rtas2019(number_of_tasksets, cap, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M):

def apply_sched_tests_rtas2019(number_of_tasksets, cap, factor_M):
    global fixed_cost, util_name, dma_name, jitter #cost_factor

    for util in util_name:
        #for period in period_name:
        #for cost in cost_factor:
        for dma in dma_name:
            #print "Schedulability tests for - Util:", util,"Cost:", cost, "Dma %:", dma
            print "Schedulability tests for - Util:", util,"Fixed Cost:", fixed_cost, "Dma %:", dma
            #data_frame = pd.DataFrame(columns=['Utili', 'SPM Stream', 'No Streaming', 'Contention-Based', 'Saud'])
            data_frame = pd.DataFrame(columns=['Utili', 'SPM Stream', 'No Streaming', 'Contention-Based'])
            data_frame.set_index('Utili')
            i = 0
            for ts_utilization_bound in pl.frange(0.1, cap, 0.1):
                spm_stream = 0
                contention = 0
                no_streaming = 0
                #saud = 0
                for n_task_sets in xrange(number_of_tasksets):
                    #input_filename = "generated_ts_rtas2019/{0}_cost-{1}_dma-{2}_cap-{3}_{4}_{5}".format("sd_ts", util, cost, dma, ts_utilization_bound, n_task_sets)
                    input_filename = "generated_ts_rtas2019/{0}_util-{1}_fixed-cost-{2}_dma-{3}_cap-{4}_{5}".format("sd_ts", util, str(fixed_cost), dma, ts_utilization_bound, n_task_sets)
                    sd_ts = read_sd_ts_from_file(input_filename)

                    #calculates the TDMA slot times according to task's fixed cost
                    t = sd_ts.task_from_id(0)
                    tdma_slot_size = (t.cost * float(dma)) / 4
                    tdma_slot_size_wo_overhead = tdma_slot_size - (tdma_slot_size * 0.01)
                    tdma_round_lenght = tdma_slot_size * 3

                    spm_stream = spm_stream + int(sd_ts.SPM_Stream_Schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght))
                    contention = contention + int(sd_ts.Contention_schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M))
                    no_streaming = no_streaming + int(sd_ts.No_streaming_schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght))
                    #saud = saud + int(sd_ts.Saud_schedulability_test())

                spm_stream = spm_stream / float(number_of_tasksets)
                contention = contention / float(number_of_tasksets)
                no_streaming = no_streaming / float(number_of_tasksets)
                #saud = saud / float(number_of_tasksets)

                #data_frame.loc[i] = [ts_utilization_bound, spm_stream, no_streaming, contention, saud]
                data_frame.loc[i] = [ts_utilization_bound, spm_stream, no_streaming, contention]
                i = i + 1
            print data_frame
            #output_filename = "{0}_cost-{1}_dma-{2}.{3}".format(util, cost, dma, "txt")
            output_filename = "{0}_fixed-cost-{1}_dma-{2}.{3}".format(util, str(fixed_cost), dma, "txt")
            data_frame.to_csv("graphs/results/" + output_filename, sep=' ')
            del data_frame

def eembc_and_streaming_benchmarks():
    eembc_tasks = []
    streaming_tasks = []

    tdma_slot_size = 20
    tdma_slot_size_wo_overhead = tdma_slot_size - (tdma_slot_size * 0.01)
    tdma_round_lenght = tdma_slot_size * 3
    factor_M = 3 # 3 cores

    #arrival_curve_jitter, input_load_time, output_unload_time, load_time, unload_time, is_sd_task, exec_cost, period, priority, deadline, id, cpu
    
    #IDs are defined by assing_ids call, later in the code
    eembc_tasks.append(SD_Task(0, 5, 5, 5, 5, True, 50, 700, 700, 2400, -1, 0))
    eembc_tasks.append(SD_Task(0, 1, 1, 1, 1, True, 10, 500, 500, 2300, -1, 0))
    eembc_tasks.append(SD_Task(0, 3, 3, 3, 3, True, 20, 400, 400, 1200, -1, 0))
    eembc_tasks.append(SD_Task(0, 4, 4, 4, 4, True, 40, 450, 450, 1300, -1, 0))

    streaming_tasks.append(SD_Task(10, 2, 2, 2, 2, True, 500, 3000, 3000, 3000, -1, 0))

    #generate unique integer numbers
    choices = list(range(len(eembc_tasks)))
    random.shuffle(choices)
    task_set = []
    for i in xrange(3):
        index = choices.pop()
        task_set.append(eembc_tasks[index])
    
    #choose a streaming task and append it to the task set
    streaming_index = random.randint(0, len(streaming_tasks) - 1)
    task_set.append(streaming_tasks[streaming_index])

    #creates a Synchrounous Dataflow task set
    sd_ts = SD_TaskSystem(task_set)
    sd_ts.assign_ids()

    print "Generated task set is:"
    print sd_ts

    #apply the schedulability tests
    spm_stream = int(sd_ts.SPM_Stream_Schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght))
    contention = int(sd_ts.Contention_schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M))
    no_streaming = int(sd_ts.No_streaming_schedulability_test(tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght))

    print "SPM Stream =", spm_stream, " Contention =", contention, " No Streaming =", no_streaming

parser = argparse.ArgumentParser()

parser.add_argument("-g", "--generate", help="Generate task sets", action="store_true")
parser.add_argument("-s", "--schedulability_tests", help="Apply the schedulability tests", action="store_true")
parser.add_argument("-hs", "--has_streaming", help="Generate task sets with streaming tasks", action="store_true")
parser.add_argument("-r", "--read_file", help="Read a file that contains a task set and print the task set. Pass the entire path to the file. Ex: -r generated_ts_rtas2019/sd_ts_util-uni-light_fixed-cost-2_dma-0.1_cap-1.0_1", type=str)
parser.add_argument("-e", "--eembc_benchmarks", help="Apply the schedulability tests using the EEMBC benchmarks", action="store_true")
args = parser.parse_args()

def main():
    ''' 
    tdma_slot_size is the size of the TDMA in the core under analysis
    tdma_slot_size_wo_overhead is the size of the TDMA in the core under analysis without overhead
    tdma_round_lenght is the lenght of the complete TDMA round
    n_cores is the number of cores available in the platform
    factor_M - under contention, each memory request takes M times to complete
    '''
    # not in use
    #tdma_slot_size = 5
    #tdma_slot_size_wo_overhead = 4
    #tdma_round_lenght = tdma_slot_size * n_cores
    
    n_cores = 3
    factor_M = n_cores
    number_of_tasksets = 100
    utilization_cap = 1.0

    #test_SD_ts()

    if args.read_file != None:
        ts = read_sd_ts_from_file(args.read_file)
        ts.print_ts()
        exit()

    if args.generate:
        #ts = generate_sd_taskset_files(number_of_tasksets, utilization_cap, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, args.has_streaming)
        if (os.path.isdir("./generated_ts_rtas2019") == False):
            print "Directory generated_ts_rtas2019 does not exist! Creating.."
            os.makedirs("./generated_ts_rtas2019")
        else:
            print "Directory generated_ts_rtas2019 exists, task sets will be overwritten!"
        ts = generate_sd_taskset_files(number_of_tasksets, utilization_cap, args.has_streaming)
        print "Generated",len(ts),"task sets"

    if args.schedulability_tests:
        #apply_sched_tests_rtas2019(number_of_tasksets, utilization_cap, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_lenght, factor_M)
        apply_sched_tests_rtas2019(number_of_tasksets, utilization_cap, factor_M)

    if args.eembc_benchmarks:
        eembc_and_streaming_benchmarks()

    if args.generate == False and args.schedulability_tests == False and args.read_file == None and args.eembc_benchmarks == None:
        print "None parameter has been passed. Read the help (-h)."

if __name__ == "__main__":
    main()
