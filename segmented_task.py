import xml.etree.ElementTree as ET
from schedcat.model.serialize import write_xml, task, set_attribute, subtag_for_attribute, maybe_int, parse_resmodel, get_attribute, write
from schedcat.util.time import ms2us
import schedcat.model.resources as resources
from schedcat.model.tasks import TaskSystem
from schedcat.model.tasks import SporadicTask
from schedcat.mapping.binpack import ignore
import copy
from fractions import Fraction
from schedcat.util.math    import lcm

from math import ceil
from math import floor

def segmented_task(t):
    tag = ET.Element('segmented_task')
    set_attribute(tag, 'id', t)
    set_attribute(tag, 'period', t)
    set_attribute(tag, 'priority', t)
    set_attribute(tag, 'cost', t)
    set_attribute(tag, 'deadline', t)
    set_attribute(tag, 'cpu', t)
    set_attribute(tag, 'load_time', t)
    set_attribute(tag, 'unload_time', t)
    set_attribute(tag, 'dma_stream_time', t)
    set_attribute(tag, 'NT', t)
    set_attribute(tag, 'is_segment_task', t)

    rmodel = subtag_for_attribute(tag, t, 'resmodel', 'resources')
    if not rmodel is None:
        for res_id in t.resmodel:
            res_requirement(t.resmodel[res_id], rmodel)

    tag.task = t
    task.xml = tag
    return tag

def parse_segmented_task(node):
    #load_time, unload_time, dma_stream_time, NT, is_segment_task, exec_cost, period, priority, deadline, id, cpu

    load_time 		  = maybe_int(node.get('load_time'))
    unload_time 		  = maybe_int(node.get('unload_time'))
    dma_stream_time 		  = maybe_int(node.get('dma_stream_time'))
    NT 		  = maybe_int(node.get('NT'))
    is_segment_task = maybe_int(node.get('is_segment_task'))
    cost      = maybe_int(node.get('cost'))
    period    = maybe_int(node.get('period'))
    priority    = maybe_int(node.get('priority'))
    deadline = maybe_int(node.get('deadline'))
    id 		  = maybe_int(node.get('id')) 
    cpu = maybe_int(node.get('cpu')) 

    #load_time, unload_time, dma_stream_time, NT, is_segment_task, exec_cost, period, priority, deadline, id, cpu
    t = Segmented_Task(load_time, unload_time, dma_stream_time, NT, is_segment_task, cost, period, priority, deadline, id, cpu)

    resmodel = parse_resmodel(node)
    if not resmodel is None:
        t.resmodel = resmodel

    t.xml = node
    node.task = t
    return t

def parse_segmented_taskset(node):
	tasks = [parse_segmented_task(n) for n in node.findall('segmented_task')]
	return Segmented_TaskSystem(tasks)

def read_sd_ts_from_file(file):
	tree = ET.ElementTree()
	tree.parse(file)
	root = tree.getroot()
	if root.tag == 'segmented_taskset':
		ts = parse_sd_taskset(root)
		ts.xml = tree.getroot()
		return ts
	elif root.tag == 'segmented_task':
		t = parse_sd_task(root)
		return t
	else:
		return None	

def segmentd_write(ts, fname):
	tag = ET.Element('segmented_taskset')
	prop = ET.SubElement(tag, 'properties')
	prop.set('utilization', str(ts.utilization()))
	prop.set('utilization_q', str(ts.utilization_q()))    
	prop.set('density_q', str(ts.density_q()))
	prop.set('density', str(ts.density()))
	prop.set('count', str(len(ts)))

	hp = ts.hyperperiod()
	if hp:
		prop.set('hyperperiod', str(hp))

	for t in ts:
		tag.append(segmented_task(t))
	
	write_xml(tag, fname)

'''
Segmented Task
Parameters are:
load_time: DMA time to load 
unload_time: DMA time to unload
dma_stream_time: DMA time to stream intot he next segment
NT: number of terminal segments
is_segment_task: bool (true means that the task is a segmented task)
exec_cost: WCET
period
priority
'''
class Segmented_Task(SporadicTask):
    def __init__(self, load_time, unload_time, dma_stream_time, is_segment_task,
                exec_cost, period, priority, deadline=None, id=None, cpu=-1):
        SporadicTask.__init__(self, exec_cost, period, deadline, id)
        self.load_time = float(load_time)
        self.unload_time = float(unload_time)
        self.dma_stream_time = float(dma_stream_time)
        self.is_segment_task = is_segment_task
        self.NT = 0  #TODO: This should be set by the segmentation algorithm
        self.priority = priority
        self.cpu = cpu
        self.segments = []

        self.exec_final_i = 0
        self.exec_cons_i = 0

    def __repr__(self):
        idstr = "id=%s" % self.id if self.id is not None else ""
        dstr  = ", deadline=%s" % self.deadline
        typestr = ", %s" % ('Segmented_Task' if bool(self.is_segment_task) == True else 'SporadicTask')
        cpustr = ", cpu=%s" % self.cpu
        ldstr = ", load_time=%s" % self.load_time
        ulstr = ", unload_time=%s" % self.unload_time
        dmastr = ", dma_st=%s" % self.dma_stream_time
        NTstr = ", NT=%s" % self.NT
        return "Segmented_Task(%s, wcet=%s, period=%s, prio=%s, util=%.2f%s%s%s%s%s%s%s)" % \
        (idstr, self.cost, self.period, self.priority, self.utilization(), dstr, typestr, cpustr, ldstr, ulstr, dmastr, NTstr)

    def utilization(self):
        if self.id < 0:
            return 0.0
        return self.cost / float(self.period)

    def utilization_q(self):
        if self.id < 0:
            return 0.0
        return Fraction(self.cost, self.period)

    def density(self):
        if self.id < 0:
            return 0.0
        return self.cost / float(min(self.period, self.deadline))

    def density_q(self):
        if self.id < 0:
            return 0.0
        return Fraction(self.cost, min(self.period, self.deadline))

'''
Segment
Parameters are:
load_time: DMA time to load 
unload_time: DMA time to unload
dma_stream_time: DMA time to stream intot he next segment
NT: number of terminal segments
exec_cost: WCET of the segment
streaming_or_terminal: 1 streaming, 0 terminal
'''
class Segment():
    def __init__(self, task, load_time, unload_time, dma_stream_time, exec_cost, streaming_or_terminal):
        self.load_time = float(load_time)
        self.unload_time = float(unload_time)        
        self.dma_stream_time = float(dma_stream_time)
        self.exec_cost = exec_cost
        self.parent_task = task
        self.streaming_or_terminal = streaming_or_terminal
        self.interval = 0

    def __repr__(self):
        idstr = "task_id=%s" % self.parent_task.id if self.parent_task.id is not None else ""
        dstr  = ", deadline=%s" % self.parent_task.deadline
        ldstr = ", load_time=%s" % self.load_time
        ulstr = ", unload_time=%s" % self.unload_time
        dmastr = ", dma_st=%s" % self.dma_stream_time
        streamingstr = ", %s" % ('Streaming segment' if self.streaming_or_terminal == 1 else 'Terminal Segment')
        intervalstr = ", inverval=%s" % self.interval
        return "Segment(%s, wcet=%s, period=%s, prio=%s%s%s%s%s%s%s)" % \
        (idstr, self.exec_cost, self.parent_task.period, self.parent_task.priority, dstr, ldstr, ulstr, dmastr, intervalstr, streamingstr)

class Segmented_TaskSystem(TaskSystem):
    def __init__(self, tasks=[]):
        TaskSystem.__init__(self, tasks)
        self.dummy_task_id = 0

    def __str__(self):
        return "\n".join([str(t) for t in self])

    def __repr__(self):
        return "Segmented_TaskSystem([" + ", ".join([repr(t) for t in self]) + "])"

    def copy(self):
        ts = Segmented_TaskSystem((copy.deepcopy(t) for t in self))
        return ts

    #load_time, unload_time, dma_stream_time, NT, is_segment_task, exec_cost, period, priority, deadline, id, cpu
    def print_ts(self):
        for t in self:
            print "Task[%d], p=%d, cost=%d, prio=%d, util=%.2f, load_time=%.2f, unload_time=%.2f, NT=%d" % (t.id, t.period, t.cost, t.priority, t.utilization(), float(t.load_time), float(t.unload_time), t.NT)
        return

    def hyperperiod(self):
        periods = []
        for t in self:
            if t.is_sd_task == 0:
                periods.append(t.period)
        return lcm(*periods)

    def task_from_id(self, t_id):
        for t in self:
            if t.id == int(t_id):
                return t
        return None

    #returns all tasks with highest priority than t_id
    def highest_prio_tasks(self, t_id):
        hp = []

        for t in self:
            if t.id != t_id and t.priority < self[t_id].priority:
                hp.append(t)
        return hp

    #returns all tasks with lowest priority than t_id
    def lowest_prio_tasks(self, t_id):
        lp = []

        for t in self:
            if t.id != t_id and t.priority > self[t_id].priority:
                lp.append(t)
        return lp
    
    def segmentation_algorithm(self, task):
        #load_time, unload_time, dma_stream_time, NT, exec_cost, streaming_or_terminal
        task.NT = 1
        return [Segment(task, task.load_time / 2, task.unload_time / 2, task.dma_stream_time / 2, task.cost / 3, 1),
                Segment(task, task.load_time / 3, task.unload_time / 3, task.dma_stream_time / 3, task.cost / 3, 1),
                Segment(task, task.load_time / 3, task.unload_time / 3, task.dma_stream_time / 3, task.cost / 3, 0)]

    def longest_cost_in_all_segments(self, tasks):
        cost = -1
        for t in tasks:
            for s in t.segments:
                if s.exec_cost > cost:
                    cost = s.exec_cost
        return cost

    def longest_unload_in_all_segments(self, tasks):
        ul = -1
        for t in tasks:
            for s in t.segments:
                if s.unload_time > ul:
                    ul = s.unload_time
        return ul

    def longest_load_in_all_segments(self, tasks):
        ld = -1
        for t in tasks:
            for s in t.segments:
                if s.load_time > ld:
                    ld = s.load_time
        return ld

    def compute_b(self, task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):
        lp = self.lowest_prio_tasks(task.id)

        t_l1_cmax = 0.0
        t_l1_ul = 0.0

        t_l2_cmax = 0.0
        t_l2_ld = 0.0

        #l1 does include "task" - t_l1 is got from lower priority tasks
        #l2 does not include "task" - t_l1 is got from lower priority tasks

        if len(lp) >= 1: # there is only one lower priority task
            t_l1_cmax = self.longest_cost_in_all_segments(lp + [task])
            t_l1_ul = self.longest_unload_in_all_segments(lp + [task])
        else: # #there are no lower priority task
            t_l1_cmax = self.longest_cost_in_all_segments([task])
            t_l1_ul = self.longest_unload_in_all_segments([task])

        if len(lp) == 0:
            t_l2_cmax = 0.0
            t_l2_ld = 0.0
        else: # at least one higher priority task plus the task under analysis
            t_l2_cmax = self.longest_cost_in_all_segments(lp) 
            t_l2_ld = self.longest_load_in_all_segments(lp)

        #calculates the second part of the equation (7)
        ul = self.longest_unload_in_all_segments(self)  # highest unload of any task in the system

        hp = self.highest_prio_tasks(task.id)
        ld = self.longest_load_in_all_segments(hp + [task])

        part_1 = max(t_l1_cmax, tdma_slot_size + ceil((ul + t_l2_ld) / tdma_slot_size_wo_overhead) * tdma_round_length)

        #calculates the third part of the equation (7)
        part_2 = max(t_l2_cmax, tdma_slot_size + ceil((t_l1_ul + ld) / tdma_slot_size_wo_overhead) * tdma_round_length)
        
        #print "Task", "part_1 =", part_1, "part_2 =",part_2,task.id, "B =", (part_1 + part_2)
        return part_1 + part_2

    def find_max_dma_times_in_segments(self):
        max_ld = -1.0
        max_ul = -1.0

        for t in self:
            for s in t.segments:
                if s.load_time > max_ld:
                    max_ld = s.load_time

                if s.unload_time > max_ul:
                    max_ul = s.unload_time
        
        return (max_ld, max_ul)

    def compute_f(self, task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):
        #first_part_a = task.cost + tdma_slot_size + ceil(task.output_unload_time / float(tdma_slot_size_wo_overhead)) * tdma_round_length
        #TODO:  The previous test considered output_unload_time. do we still have it here or it is just unload_time?

        #take the cost and unload time from the last segment
        first_part_a = task.segments[len(task.segments)-1].exec_cost + tdma_slot_size + ceil(task.segments[len(task.segments)-1].unload_time / float(tdma_slot_size_wo_overhead)) * tdma_round_length
        
        (max_ld, max_ul) = self.find_max_dma_times_in_segments()
        
        first_part_b = ceil((max_ld + max_ul) / float(tdma_slot_size_wo_overhead)) * tdma_round_length
        
        second_part_a = tdma_slot_size + ceil((max_ld + max_ul) / float(tdma_slot_size_wo_overhead)) * tdma_round_length

        #second_part_b = ceil(task.output_unload_time / float(tdma_slot_size_wo_overhead)) * float(tdma_round_length)
        #TODO: The previous test considered output_unload_time. do we still have it here or it is just unload_time?
        second_part_b = ceil(task.segments[len(task.segments)-1].unload_time / float(tdma_slot_size_wo_overhead)) * float(tdma_round_length)
        
        first_part = first_part_a + first_part_b
        
        second_part = second_part_a + second_part_b
        
        #print "Task", task.id, "F =", max(first_part, second_part)
        
        return max(first_part, second_part)

    #task is the current task under analysis, I_j is a set of tuple (task with higher priority, number of interferring jobs)
    def compute_s_i_streaming(self, task, I_j):
        s_i_streaming = []

        #print "task",task.id,"compute_s_i_streaming()"

        for entry in I_j:
            #Initialize s_i_streaming by putting in it Ij copies of each streaming segment Tj,k for each higher priority task Tj
            number_of_inteferring_jobs = entry[1]
            hp_task = entry[0]
            for seg in hp_task.segments:
                if seg.streaming_or_terminal == 1: #streaming segment
                    for i in xrange(number_of_inteferring_jobs):
                        #print "adding seg", seg.interval, "from task", hp_task.id
                        s_i_streaming.append([hp_task, seg, seg.interval])
            #and one copy of each streaming segment Ti,k of the task under analysis.
            for seg in task.segments:
                if seg.streaming_or_terminal == 1: #streaming segment
                    #print "adding seg", seg.interval, "from task", task.id
                    s_i_streaming.append([task, seg, seg.interval])

        return s_i_streaming

    def order_segments_by_lenght(self, task_j, s_i_streaming, streaming_segment):
        segs = [] #segs contains all segments of type "streaming_segment" of task_j order by increasing interval

        #print "task_j",task_j.id,"order_segments_by_lenght()"

        for s in s_i_streaming:
            if task_j == s[0] and s[1].streaming_or_terminal == streaming_segment:
                #print "Adding segment"
                segs.append(s[1])
        
        segs.sort(key=lambda x: x.interval)
        return segs

    def remove_segments_from_s_i_streaming(self, task, s_i_streaming, I_j, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):

        #print "-----------Task under analysis =", task.id,"-----------\n"
        #print "original s_i_streaming"
        #print s_i_streaming,"\n\n"

        for j in I_j:
            number_of_preemptions = (j[1] * j[0].NT)
            #print "Task j =", t[0].id, "preemptions =",number_of_preemptions
            segments_order_by_lenght = self.order_segments_by_lenght(j[0], s_i_streaming, 1)
            hp_than_j = self.highest_prio_tasks(j[0].id)
            
            max_number = 0
            
            #TODO: is compute_b correct here? Need to change compute_b to use segments info
            B_j = self.compute_b(j[0], tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)

            #BIG TODO HERE: is H_j correct?
            H_j = segments_order_by_lenght[len(segments_order_by_lenght) - 1].exec_cost + B_j    
            
            for k in hp_than_j:
                I_j = ceil((B_j + H_j) / float(k.period))
                max_number = max_number + (I_j * k.NT)
            
            if number_of_preemptions > max_number:
                number_of_preemptions = max_number
            
            for i in xrange(number_of_preemptions):
                #remove from s_i_streaming the first number_of_preemptions segments that are ordered in segments_order_by_lenght
                seg_to_be_removed = segments_order_by_lenght[i]
                triple = [j[0], seg_to_be_removed, seg_to_be_removed.interval]
                #print "Removing", triple, "from task ",j[0].id,"in the current task under analysis", task.id 
                s_i_streaming.remove(triple)

            #print segments_order_by_lenght

        #print "\n\nafter removing s_i_streaming"
        #print s_i_streaming,"\n"
        return s_i_streaming

    def get_terminal_segments(self, task):
        segs = []
        for seg in task.segments:
            if seg.streaming_or_terminal == 0:
                segs.append(seg)
        return segs

    def longest_cost_in_segments(self, tasks, seg_type):
        longest_cost = -1
        seg = 0
        for t in tasks:
            for s in t.segments:
                #seg_type == 0 is a terminal segment, 1 == is a streaming one
                if s.streaming_or_terminal == seg_type:
                    if s.exec_cost > longest_cost:
                        longest_cost = s.exec_cost
                        seg = s
        return seg

    def longest_unload_time_in_segments(self, tasks, seg_type):
        longest_unload = -1
        seg = 0
        for t in tasks:
            for s in t.segments:
                #seg_type == 0 is a terminal segment, 1 == is a streaming one
                if s.streaming_or_terminal == seg_type:
                    if s.unload_time > longest_unload:
                        longest_unload = s.unload_time
                        seg = s
        return seg

    def longest_load_time_in_segments(self, tasks, seg_type):
        longest_load = -1
        seg = 0
        for t in tasks:
            for s in t.segments:
                #seg_type == 0 is a terminal segment, 1 == is a streaming one
                if s.streaming_or_terminal == seg_type:
                    if s.load_time > longest_load:
                        longest_load = s.load_time
                        seg = s
        return seg

    def create_dummy_segment(self, parent_task):
        #task, load_time, unload_time, dma_stream_time, exec_cost, streaming_or_terminal
        return [Segment(parent_task, 0, 0, 0, 0, 0)]

    def create_dummy_task(self):
        self.dummy_task_id = self.dummy_task_id - 1
        #load_time, unload_time, dma_stream_time, is_segment_task, exec_cost, period, priority, deadline, id, cpu
        task = Segmented_Task(0, 0, 0, 0, True, 0, 0, 0, self.dummy_task_id, 0)
        task.segments = self.create_dummy_segment(task)
        return task

    def compute_h_i_terminal(self, task, I_j, alpha, beta, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):
        E_i = []
        UL_i = []
        LD_i = []

        #print "-------Task under Analysis =", task.id,"---------"

        lp = self.lowest_prio_tasks(task.id)
        seg_with_longest_cost = []
        seg_with_longest_unload = []
        seg_with_longest_load = []

        if lp == []:
            task_tmp = self.create_dummy_task()
            lp.append(task_tmp)

        seg_with_longest_cost = self.longest_cost_in_segments(lp, 0) #0 means terminal segment
        if seg_with_longest_cost == []:
            seg_with_longest_cost = self.create_dummy_segment(task_tmp)
        
        seg_with_longest_unload = self.longest_unload_time_in_segments(lp, 0)
        if seg_with_longest_unload == []:
            seg_with_longest_unload = self.create_dummy_segment(task_tmp)
        
        seg_with_longest_load = self.longest_load_time_in_segments(lp, 0)
        if seg_with_longest_load == []:
            seg_with_longest_load = self.create_dummy_segment(task_tmp)

        # SUM j < i: Ij x Tj.NT are terminal intervals of higher priority tasks. Hence, put in Ei the ctime of all such intervals, 
        # put in ULi and LDi the load / unload times of all such intervals.
        for j in I_j:
            terminal_segs = self.get_terminal_segments(j[0])
            for i in xrange(j[0].NT):
                E_i.append(terminal_segs[i].exec_cost)
                UL_i.append(terminal_segs[i].unload_time)
                LD_i.append(terminal_segs[i].load_time)

        # SUM j <i: Ij x Tj.NT are preemptions caused by higher priority tasks. Add ctimes to Ei following the same rules as
        # S_i^streaming for removing from Si (i.e., add the ctimes of segments with the smallest length). 
        # Add load / unloads to ULi , LDi by picking streaming segments with largest load / unload time, under the
        # condition that the number of segments picked from Tj cannot be greater than SUM k < j: Ik x Tk.NT

        for j in I_j:
            number_of_preemptions = (j[1] * j[0].NT)
            
            terminal_segs = self.get_terminal_segments(j[0])
            terminal_segs.sort(key=lambda x: x.exec_cost)

            hp_than_j = self.highest_prio_tasks(j[0].id)
            
            max_number = 0
            
            #TODO: is compute_b correct here? Need to change compute_b to use segments info
            B_j = self.compute_b(j[0], tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)
            #BIG TODO HERE: is H_j correct?
            H_j = terminal_segs[len(terminal_segs) - 1].exec_cost + B_j    
            
            for k in hp_than_j:
                I_j = ceil((B_j + H_j) / float(k.period))
                max_number = max_number + (I_j * k.NT)
            
            if number_of_preemptions > max_number:
                number_of_preemptions = max_number
            
            for i in xrange(number_of_preemptions):
                E_i.append(terminal_segs[i].exec_cost)
                UL_i.append(seg_with_longest_unload.unload_time)
                LD_i.append(seg_with_longest_load.load_time)

        # Ti.NT - 1 are terminal intervals of the task under analysis, except for the last one (which is in Fi). Hence, put in Ei and ULi the
        # ctime and unload of all terminal intervals of the task under analysis except the last; put in LDi the load time of all terminal intervals
        # of the task under analysis including the last one (because the load happens before Fi).
        terminal_segs = self.get_terminal_segments(task)
        if task.NT > 1:
            for i in xrange(task.NT - 1):
                E_i.append(terminal_segs[i].exec_cost)
                UL_i.append(terminal_segs[i].unload_time)
                LD_i.append(terminal_segs[i].load_time)

                # Ti.NT - 1 are the preemptions caused by the terminal intervals of the task under analysis. Hence, add to E i that number of segments
                # of lower priority tasks with maximum ctime, then add to ULi that number of segments of lower priority tasks with maximum unload
                # time, and same for LDi .
                E_i.append(seg_with_longest_cost.exec_cost)
                UL_i.append(seg_with_longest_unload.unload_time)
                LD_i.append(seg_with_longest_load.load_time)

        # include the last terminal segment to LDi set
        if(task.NT > 0):
            LD_i.append(terminal_segs[len(terminal_segs)-1].load_time)

        # there is one extra blocking task of lower priority in the first interval of Hi . For this one, add to Ei a longest interval of lower
        # priority tasks; and to ULi two longest unload of lower priority tasks (two unloads because you have to unload the task in the first
        # interval, but you also have to unload the task in Bi).
        E_i.append(seg_with_longest_cost.exec_cost)
        UL_i.append(seg_with_longest_unload.unload_time)
        UL_i.append(seg_with_longest_unload.unload_time)

        # Sort ULi and LDi in decreasing order.
        UL_i.sort(reverse=True)
        LD_i.sort(reverse=True)

        # Combine one load and one unload into one DMA phase of length alpha + beta x (ld + ul). Put the obtained DMA phases into a set Mi.
        M_i = []
        ul_len = int(len(UL_i))
        ld_len = int(len(LD_i))
        final_len = ul_len if ul_len < ld_len else ld_len
        for l in xrange(final_len): #TODO: it seems that UL and LD have different size here
            M_i.append(alpha + beta * (UL_i[l] + LD_i[l]))

        # Compute the union of Ei and Mi and sort.
        union = E_i + M_i
        union.sort(reverse=True)

        # H_i^terminal is the sum of the NTi largest values.
        sum = 0
        for i in xrange(task.NT):
            sum = sum + union[i]

        return sum

    def non_streaming_sched_test(self, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):
        return
        #for task in self:
        #    print "task("+str(task.id)+") =",task

    def streaming_sched_test(self, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length):

        alpha = tdma_round_length + tdma_slot_size
        beta = tdma_round_length / tdma_slot_size_wo_overhead

        #star by computing the segments for each task
        for task in self:
            task.segments = self.segmentation_algorithm(task)

            #compute the the length of the interval if we stream (max of DMA time for streaming, and ctime of segment)
            #t_i_k.l in the equation
            for seg in task.segments:
                #TODO: ask Rodolfo, Does the sum order matter (I mean, the sum of alpha + beta should be perform early)?
                seg.interval = float(max(seg.exec_cost, alpha + beta * seg.dma_stream_time))
                #print seg

        #now perform the schedulability test
        for task in self:
            #print task

            B_i = self.compute_b(task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)
            F_i = self.compute_f(task, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)
            R_i = task.cost + B_i
            H_i = R_i
        
            hp = self.highest_prio_tasks(task.id)

            end_loop = False            

            while end_loop == False:
                I_j = []
                for j in hp:                    
                    number_of_inteferring_jobs = int(ceil((B_i + H_i) / float(j.period))) #H_i is upated in the current loop, so next iteration will have another value
                    I_j.append([j, number_of_inteferring_jobs])

                s_i_streaming = self.compute_s_i_streaming(task, I_j)

                #print "------\nTask under analysis", task.id,"------"
                #print s_i_streaming

                #task, s_i_streaming, I_j
                s_i_streaming = self.remove_segments_from_s_i_streaming(task, s_i_streaming, I_j, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)

                H_i_streaming = 0

                for seg_interval_entry in s_i_streaming:
                    H_i_streaming = H_i_streaming + seg_interval_entry[2] #seg_interval_entry is formed as [task, seg, seg.interval]

                H_i_terminal = 0
                H_i_terminal = self.compute_h_i_terminal(task, I_j, alpha, beta, tdma_slot_size, tdma_slot_size_wo_overhead, tdma_round_length)
                
                #TODO: is this right?
                H_i = H_i_streaming + H_i_terminal

                New_R_i = B_i + H_i + F_i

                if New_R_i > task.deadline:
                    print "Task",task.id,"is not schedulable!"
                    return False

                if New_R_i == R_i: #convergence
                    print "Task",task.id,"is schedulable!"
                    end_loop = True

                R_i = New_R_i

                del I_j
                del s_i_streaming
                
        print "Task set is schedulable!"
        return True
